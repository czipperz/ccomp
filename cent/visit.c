#include "visit.h"

Cent_TopLevelVisitor Cent_TopLevelVisitor_new(void) {
    Cent_TopLevelVisitor default_visitor = {
        Cent_TopLevelVisitor_visit_variable_name,

        Cent_TopLevelVisitor_visit_base_type,
        Cent_TopLevelVisitor_visit_pointer_type,
        Cent_TopLevelVisitor_visit_type,

        Cent_TopLevelVisitor_visit_parameter,

        Cent_TopLevelVisitor_visit_integer_constant_expression,
        Cent_TopLevelVisitor_visit_string_constant_expression,
        Cent_TopLevelVisitor_visit_constant_expression,
        Cent_TopLevelVisitor_visit_variable_expression,
        Cent_TopLevelVisitor_visit_argument,
        Cent_TopLevelVisitor_visit_function_call_expression,
        Cent_TopLevelVisitor_visit_dereference_expression,
        Cent_TopLevelVisitor_visit_binary_operator,
        Cent_TopLevelVisitor_visit_binary_expression,
        Cent_TopLevelVisitor_visit_expression,
        Cent_TopLevelVisitor_visit_condition_expression,

        Cent_TopLevelVisitor_visit_function_declaration,
        Cent_TopLevelVisitor_visit_variable_declaration,
        Cent_TopLevelVisitor_visit_declaration,
        Cent_TopLevelVisitor_visit_block_declaration,
        Cent_TopLevelVisitor_visit_top_level_declaration,

        Cent_TopLevelVisitor_visit_expression_statement,
        Cent_TopLevelVisitor_visit_block_statement,
        Cent_TopLevelVisitor_visit_return_statement,
        Cent_TopLevelVisitor_visit_break_statement,
        Cent_TopLevelVisitor_visit_continue_statement,
        Cent_TopLevelVisitor_visit_while_statement,
        Cent_TopLevelVisitor_visit_if_statement,
        Cent_TopLevelVisitor_visit_empty_statement,
        Cent_TopLevelVisitor_visit_statement,

        Cent_TopLevelVisitor_visit_block,
        Cent_TopLevelVisitor_visit_top_level_block,
        Cent_TopLevelVisitor_visit_function_definition,
        Cent_TopLevelVisitor_visit_top_level,
    };
    return default_visitor;
}

void Cent_TopLevelVisitor_visit_variable_name(void* visitor, char* name) {}

void Cent_TopLevelVisitor_visit_base_type(void* visitor, Cent_BaseType* type) {}
void Cent_TopLevelVisitor_visit_pointer_type(void* visitor,
                                             Cent_PointerType* type) {
    Cent_TopLevelVisitor_accept_type(visitor, type->inner);
}
void Cent_TopLevelVisitor_visit_type(void* visitor, Cent_Type* type) {
    switch (type->type) {
    case Cent_Type_Base:
        return Cent_TopLevelVisitor_accept_base_type(visitor,
                                                     &type->value.base);
    case Cent_Type_Pointer:
        return Cent_TopLevelVisitor_accept_pointer_type(visitor,
                                                        &type->value.pointer);
    }
}

void Cent_TopLevelVisitor_visit_parameter(void* visitor,
                                          Cent_Parameter* parameter) {
    Cent_TopLevelVisitor_accept_type(visitor, &parameter->type);
    if (parameter->name) {
        Cent_TopLevelVisitor_accept_variable_name(visitor, parameter->name);
    }
}

void Cent_TopLevelVisitor_visit_integer_constant_expression(void* visitor,
                                                            uintmax_t value) {}
void Cent_TopLevelVisitor_visit_string_constant_expression(void* visitor,
                                                           char* value) {}
void Cent_TopLevelVisitor_visit_constant_expression(void* visitor,
                                                    Cent_Constant* constant) {
    switch (constant->type) {
    case Cent_Constant_Integer:
        return Cent_TopLevelVisitor_accept_integer_constant_expression(
            visitor, constant->value.integer);
    case Cent_Constant_String:
        return Cent_TopLevelVisitor_accept_string_constant_expression(
            visitor, constant->value.string);
    }
}
void Cent_TopLevelVisitor_visit_variable_expression(void* visitor, char* name) {
    Cent_TopLevelVisitor_accept_variable_name(visitor, name);
}
void Cent_TopLevelVisitor_visit_argument(void* visitor,
                                         Cent_Expression* expression) {
    Cent_TopLevelVisitor_accept_expression(visitor, expression);
}
void Cent_TopLevelVisitor_visit_function_call_expression(
    void* visitor, Cent_FunctionCallExpression* expression) {
    size_t i;
    Cent_TopLevelVisitor_accept_expression(visitor, expression->function);
    for (i = 0; i < expression->num_arguments; ++i) {
        Cent_TopLevelVisitor_accept_argument(visitor,
                                             &expression->arguments[i]);
    }
}
void Cent_TopLevelVisitor_visit_dereference_expression(
    void* visitor, Cent_DereferenceExpression* expression) {
    Cent_TopLevelVisitor_accept_expression(visitor, expression->inner);
}
void Cent_TopLevelVisitor_visit_binary_operator(void* visitor,
                                                Cent_Operator operator) {}
void Cent_TopLevelVisitor_visit_binary_expression(
    void* visitor, Cent_BinaryExpression* expression) {
    Cent_TopLevelVisitor_accept_expression(visitor, expression->left);
    Cent_TopLevelVisitor_accept_binary_operator(visitor, expression->operator);
    Cent_TopLevelVisitor_accept_expression(visitor, expression->right);
}
void Cent_TopLevelVisitor_visit_expression(void* visitor,
                                           Cent_Expression* expression) {
    switch (expression->type) {
    case Cent_Expression_Constant:
        return Cent_TopLevelVisitor_accept_constant_expression(
            visitor, &expression->value.constant);
    case Cent_Expression_Variable:
        return Cent_TopLevelVisitor_accept_variable_expression(
            visitor, expression->value.variable);
    case Cent_Expression_FunctionCall:
        return Cent_TopLevelVisitor_accept_function_call_expression(
            visitor, &expression->value.function_call);
    case Cent_Expression_Dereference:
        return Cent_TopLevelVisitor_accept_dereference_expression(
            visitor, &expression->value.dereference);
    case Cent_Expression_Binary:
        return Cent_TopLevelVisitor_accept_binary_expression(
            visitor, &expression->value.binary);
    }
}
void Cent_TopLevelVisitor_visit_condition_expression(
    void* visitor, Cent_Expression* expression) {
    Cent_TopLevelVisitor_accept_expression(visitor, expression);
}

void Cent_TopLevelVisitor_visit_function_declaration(
    void* visitor, Cent_FunctionDeclaration* function_declaration) {
    size_t i;
    Cent_TopLevelVisitor_accept_type(visitor,
                                     &function_declaration->return_type);
    Cent_TopLevelVisitor_accept_variable_name(visitor,
                                              function_declaration->name);
    for (i = 0; i < function_declaration->num_parameters; ++i) {
        Cent_TopLevelVisitor_accept_parameter(
            visitor, &function_declaration->parameters[i]);
    }
}
void Cent_TopLevelVisitor_visit_variable_declaration(
    void* visitor, Cent_VariableDeclaration* variable_declaration) {
    Cent_TopLevelVisitor_accept_type(visitor, &variable_declaration->type);
    Cent_TopLevelVisitor_accept_variable_name(visitor,
                                              variable_declaration->name);
    if (variable_declaration->value) {
        Cent_TopLevelVisitor_accept_expression(visitor,
                                               variable_declaration->value);
    }
}
void Cent_TopLevelVisitor_visit_declaration(void* visitor,
                                            Cent_Declaration* declaration) {
    switch (declaration->type) {
    case Cent_Declaration_Function:
        return Cent_TopLevelVisitor_accept_function_declaration(
            visitor, &declaration->value.function);
    case Cent_Declaration_Variable:
        return Cent_TopLevelVisitor_accept_variable_declaration(
            visitor, &declaration->value.variable);
    }
}
void Cent_TopLevelVisitor_visit_block_declaration(
    void* visitor, Cent_Declaration* declaration) {
    Cent_TopLevelVisitor_accept_declaration(visitor, declaration);
}
void Cent_TopLevelVisitor_visit_top_level_declaration(
    void* visitor, Cent_Declaration* declaration) {
    Cent_TopLevelVisitor_accept_declaration(visitor, declaration);
}

void Cent_TopLevelVisitor_visit_expression_statement(
    void* visitor, Cent_Expression* expression) {
    Cent_TopLevelVisitor_accept_expression(visitor, expression);
}
void Cent_TopLevelVisitor_visit_block_statement(void* visitor,
                                                Cent_Block* block) {
    Cent_TopLevelVisitor_accept_block(visitor, block);
}
void Cent_TopLevelVisitor_visit_return_statement(
    void* visitor, Cent_ReturnStatement* statement) {
    if (statement->is_valid) {
        Cent_TopLevelVisitor_accept_expression(visitor, &statement->expression);
    }
}
void Cent_TopLevelVisitor_visit_continue_statement(void* visitor) {}
void Cent_TopLevelVisitor_visit_break_statement(void* visitor) {}
void Cent_TopLevelVisitor_visit_while_statement(
    void* visitor, Cent_WhileStatement* statement) {
    Cent_TopLevelVisitor_accept_condition_expression(visitor,
                                                     &statement->condition);
    Cent_TopLevelVisitor_accept_statement(visitor, statement->body);
}
void Cent_TopLevelVisitor_visit_if_statement(void* visitor,
                                             Cent_IfStatement* statement) {
    Cent_TopLevelVisitor_accept_condition_expression(visitor,
                                                     &statement->condition);
    Cent_TopLevelVisitor_accept_statement(visitor, statement->trues);
    if (statement->falses) {
        Cent_TopLevelVisitor_accept_statement(visitor, statement->falses);
    }
}
void Cent_TopLevelVisitor_visit_empty_statement(void* visitor) {}
void Cent_TopLevelVisitor_visit_statement(void* visitor,
                                          Cent_Statement* statement) {
    switch (statement->type) {
    case Cent_Statement_Expression:
        return Cent_TopLevelVisitor_accept_expression_statement(
            visitor, &statement->value.expression);
    case Cent_Statement_Block:
        return Cent_TopLevelVisitor_accept_block_statement(
            visitor, statement->value.block);
    case Cent_Statement_Return:
        return Cent_TopLevelVisitor_accept_return_statement(
            visitor, &statement->value.returns);
    case Cent_Statement_Continue:
        return Cent_TopLevelVisitor_accept_continue_statement(visitor);
    case Cent_Statement_Break:
        return Cent_TopLevelVisitor_accept_break_statement(visitor);
    case Cent_Statement_While:
        return Cent_TopLevelVisitor_accept_while_statement(
            visitor, &statement->value.whiles);
    case Cent_Statement_If:
        return Cent_TopLevelVisitor_accept_if_statement(visitor,
                                                        &statement->value.ifs);
    case Cent_Statement_Empty:
        return Cent_TopLevelVisitor_accept_empty_statement(visitor);
    }
}

void Cent_TopLevelVisitor_visit_block(void* visitor, Cent_Block* block) {
    size_t i;
    for (i = 0; i < block->num_declarations; ++i) {
        Cent_TopLevelVisitor_accept_declaration(visitor,
                                                &block->declarations[i]);
    }
    for (i = 0; i < block->num_statements; ++i) {
        Cent_TopLevelVisitor_accept_statement(visitor, &block->statements[i]);
    }
}
void Cent_TopLevelVisitor_visit_top_level_block(void* visitor,
                                                Cent_Block* block) {
    Cent_TopLevelVisitor_accept_block(visitor, block);
}
void Cent_TopLevelVisitor_visit_function_definition(
    void* visitor, Cent_FunctionDefinition* function_definition) {
    Cent_TopLevelVisitor_accept_function_declaration(
        visitor, &function_definition->declaration);
    Cent_TopLevelVisitor_accept_top_level_block(visitor,
                                                &function_definition->body);
}
void Cent_TopLevelVisitor_visit_top_level(void* visitor,
                                          Cent_TopLevel* top_level) {
    switch (top_level->type) {
    case Cent_TopLevel_Declaration:
        return Cent_TopLevelVisitor_accept_declaration(
            visitor, &top_level->value.declaration);
    case Cent_TopLevel_FunctionDefinition:
        return Cent_TopLevelVisitor_accept_function_definition(
            visitor, &top_level->value.function_definition);
    }
}

#define make_accept(function, type)                                            \
    void Cent_TopLevelVisitor_accept_##function(Cent_TopLevelVisitor* visitor, \
                                                type x) {                      \
        return visitor->visit_##function(visitor, x);                          \
    }

#define make_accept_empty(function)                                            \
    void Cent_TopLevelVisitor_accept_##function(                               \
        Cent_TopLevelVisitor* visitor) {                                       \
        return visitor->visit_##function(visitor);                             \
    }

make_accept(variable_name, char*);

make_accept(base_type, Cent_BaseType*);
make_accept(pointer_type, Cent_PointerType*);
make_accept(type, Cent_Type*);

make_accept(parameter, Cent_Parameter*);

make_accept(integer_constant_expression, uintmax_t);
make_accept(string_constant_expression, char*);
make_accept(constant_expression, Cent_Constant*);
make_accept(variable_expression, char*);
make_accept(argument, Cent_Expression*);
make_accept(function_call_expression, Cent_FunctionCallExpression*);
make_accept(dereference_expression, Cent_DereferenceExpression*);
make_accept(binary_operator, Cent_Operator);
make_accept(binary_expression, Cent_BinaryExpression*);
make_accept(expression, Cent_Expression*);
make_accept(condition_expression, Cent_Expression*);

make_accept(function_declaration, Cent_FunctionDeclaration*);
make_accept(variable_declaration, Cent_VariableDeclaration*);
make_accept(declaration, Cent_Declaration*);
make_accept(block_declaration, Cent_Declaration*);
make_accept(top_level_declaration, Cent_Declaration*);

make_accept(expression_statement, Cent_Expression*);
make_accept(block_statement, Cent_Block*);
make_accept(return_statement, Cent_ReturnStatement*);
make_accept_empty(continue_statement);
make_accept_empty(break_statement);
make_accept(while_statement, Cent_WhileStatement*);
make_accept(if_statement, Cent_IfStatement*);
make_accept_empty(empty_statement);
make_accept(statement, Cent_Statement*);

make_accept(block, Cent_Block*);
make_accept(top_level_block, Cent_Block*);
make_accept(function_definition, Cent_FunctionDefinition*);
make_accept(top_level, Cent_TopLevel*);
