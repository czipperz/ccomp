#pragma once

#include "syntax.h"
#include <stdio.h>

void Cent_print_variable_name(FILE*, char*);

void Cent_print_base_type(FILE*, Cent_BaseType*);
void Cent_print_pointer_type(FILE*, Cent_PointerType*);
void Cent_print_type(FILE*, Cent_Type*);

void Cent_print_parameter(FILE*, Cent_Parameter*);

void Cent_print_integer_constant_expression(FILE*, uintmax_t);
void Cent_print_string_constant_expression(FILE*, char*);
void Cent_print_constant_expression(FILE*, Cent_Constant*);
void Cent_print_variable_expression(FILE*, char*);
void Cent_print_argument(FILE*, Cent_Expression*);
void Cent_print_function_call_expression(FILE*, Cent_FunctionCallExpression*);
void Cent_print_dereference_expression(FILE*, Cent_DereferenceExpression*);
void Cent_print_binary_operator(FILE*, Cent_Operator);
void Cent_print_binary_expression(FILE*, Cent_BinaryExpression*);
void Cent_print_expression(FILE*, Cent_Expression*);
void Cent_print_condition_expression(FILE*, Cent_Expression*);

void Cent_print_function_declaration(FILE*, Cent_FunctionDeclaration*);
void Cent_print_variable_declaration(FILE*, Cent_VariableDeclaration*);
void Cent_print_declaration(FILE*, Cent_Declaration*);
void Cent_print_block_declaration(FILE*, Cent_Declaration*);
void Cent_print_top_level_declaration(FILE*, Cent_Declaration*);

void Cent_print_expression_statement(FILE*, Cent_Expression*);
void Cent_print_block_statement(FILE*, Cent_Block*);
void Cent_print_return_statement(FILE*, Cent_ReturnStatement*);
void Cent_print_continue_statement(FILE*);
void Cent_print_break_statement(FILE*);
void Cent_print_while_statement(FILE*, Cent_WhileStatement*);
void Cent_print_if_statement(FILE*, Cent_IfStatement*);
void Cent_print_empty_statement(FILE*);
void Cent_print_statement(FILE*, Cent_Statement*);

void Cent_print_block(FILE*, Cent_Block*);
void Cent_print_top_level_block(FILE*, Cent_Block*);
void Cent_print_function_definition(FILE*, Cent_FunctionDefinition*);
void Cent_print_top_level(FILE*, Cent_TopLevel*);
