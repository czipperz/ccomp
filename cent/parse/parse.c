#include "parse.h"
#include "panic.h"
#include "print.h"

int Cent_Parser_parse_parameter(Cent_Parser* parser,
                                Cent_Parameter* parameter) {
    int res;
    Cent_Token name_token;
    res = Cent_Parser_parse_type(parser, &parameter->type);
    if (res == -1) {
        fprintf(stderr, "Error: While parsing parameters\n");
        return -1;
    } else if (res == 0) {
        fprintf(stderr, "Error: End of file while parsing parameters\n");
        return -1;
    }

    res = Cent_Tokenizer_next(&parser->tokenizer, &name_token);
    if (res == 0) {
        fprintf(stderr, "Error: Could not parse name of parameter of type ");
        Cent_print_type(stderr, &parameter->type);
        fprintf(stderr, "\n");
        Cent_Type_destroy(&parameter->type);
        return -1;
    }
    if (name_token.type == Cent_Token_Symbol) {
        parameter->name = name_token.value.symbol;
        return 1;
    } else if (name_token.type == Cent_Token_Comma
               || name_token.type == Cent_Token_CloseParen) {
        Cent_Tokenizer_push_front(&parser->tokenizer, &name_token);
        parameter->name = 0;
        return 1;
    }

    fprintf(stderr,
            "Error: While parsing parameters, expected a symbol but got a ");
    Cent_print_token(stderr, &name_token);
    fprintf(stderr, "\n");
    Cent_Type_destroy(&parameter->type);
    return -1;
}

int Cent_Parser_parse_code_block(Cent_Parser* parser, Cent_Block* block) {
    Deque declarations;
    Deque statements;
    Cent_Token block_token;
    int res = Cent_Tokenizer_next(&parser->tokenizer, &block_token);
    if (res == 0) {
        fprintf(stderr,
                "Error: End of file when expected open curly to start code "
                "block\n");
        return -1;
    }
    if (block_token.type != Cent_Token_OpenCurly) {
        fprintf(stderr,
                "Error: Expected open curly to start code block, found ");
        Cent_print_token(stderr, &block_token);
        fprintf(stderr, "\n");
        return -1;
    }

    declarations = Deque_new();
    statements = Deque_new();

    while (1) {
        Cent_Statement statement;
        Cent_Declaration declaration;

        res = Cent_Tokenizer_next(&parser->tokenizer, &block_token);
        if (res == 0) {
            fprintf(stderr,
                    "Error: End of file when expected close curly to end code "
                    "block\n");
            return -1;
        }
        if (block_token.type == Cent_Token_CloseCurly) {
            goto ret;
        }
        Cent_Tokenizer_push_front(&parser->tokenizer, &block_token);

        res = Cent_Parser_parse_statement_or_declaration(parser, &statement,
                                                         &declaration);
        if (res < 0) {
            size_t i;
            fprintf(stderr,
                    "Error: While parsing code block after:\nError: {\n");
            for (i = 0; i < declarations.length; ++i) {
                fprintf(stderr, "Error: ");
                Cent_print_declaration(
                    stderr,
                    Deque_at(&declarations, i, sizeof(Cent_Declaration)));
                fprintf(stderr, "\n");
            }
            Deque_destroy(&declarations, Cent_Declaration_destroy,
                          sizeof(Cent_Declaration));
            return -1;
        } else if (res == 0) {
            /* hit a close curly */
            goto ret;
        } else if (res == 1) {
            Deque_push_back(&statements, &statement, sizeof(Cent_Statement));
            break;
        } else if (res == 2) {
            Deque_push_back(&declarations, &declaration,
                            sizeof(Cent_Declaration));
        } else {
            unreachable();
        }
    }

    while (1) {
        Cent_Statement statement;

        res = Cent_Tokenizer_next(&parser->tokenizer, &block_token);
        if (res == 0) {
            fprintf(stderr,
                    "Error: End of file when expected close curly to end code "
                    "block\n");
            return -1;
        }
        if (block_token.type == Cent_Token_CloseCurly) {
            goto ret;
        }
        Cent_Tokenizer_push_front(&parser->tokenizer, &block_token);

        res = Cent_Parser_parse_statement(parser, &statement);
        if (res <= 0) {
            size_t i;
            fprintf(stderr,
                    "Error: While parsing code block after:\nError: {\n");
            for (i = 0; i < declarations.length; ++i) {
                fprintf(stderr, "Error: ");
                Cent_print_declaration(
                    stderr,
                    Deque_at(&declarations, i, sizeof(Cent_Declaration)));
                fprintf(stderr, "\n");
            }
            for (i = 0; i < statements.length; ++i) {
                fprintf(stderr, "Error: ");
                Cent_print_statement(
                    stderr, Deque_at(&statements, i, sizeof(Cent_Statement)));
                fprintf(stderr, "\n");
            }
            Deque_destroy(&declarations, Cent_Declaration_destroy,
                          sizeof(Cent_Declaration));
            Deque_destroy(&statements, Cent_Statement_destroy,
                          sizeof(Cent_Statement));
            return -1;
        }

        Deque_push_back(&statements, &statement, sizeof(Cent_Statement));
    }

ret:
    block->num_declarations = declarations.length;
    block->declarations = Deque_unwrap(&declarations);
    block->num_statements = statements.length;
    block->statements = Deque_unwrap(&statements);
    return 1;
}
