#include "panic.h"
#include "parse.h"
#include "print.h"
#include <string.h>

int Cent_Parser_parse_statement_or_declaration(Cent_Parser* parser,
                                               Cent_Statement* statement,
                                               Cent_Declaration* declaration) {
    Cent_Token* first_token;
    Cent_Token* second_token;
    Cent_Tokenizer_peek(&parser->tokenizer, 1);
    first_token = Cent_Tokenizer_peek(&parser->tokenizer, 0);
    if (!first_token) {
        fprintf(stderr,
                "Error: End of file when expected statement or declaration\n");
        return -1;
    }
    if (first_token->type == Cent_Token_Semicolon) {
        Cent_Tokenizer_pop_front(&parser->tokenizer);
        statement->type = Cent_Statement_Empty;
        return 1;
    }
    if (first_token->type == Cent_Token_CloseCurly) {
        Cent_Tokenizer_pop_front(&parser->tokenizer);
        return 0;
    }
    if (first_token->type != Cent_Token_Symbol
        || strcmp(first_token->value.symbol, "return") == 0
        || strcmp(first_token->value.symbol, "while") == 0) {
        return Cent_Parser_parse_statement(parser, statement);
    }

    second_token = Cent_Tokenizer_peek(&parser->tokenizer, 1);

    if (!second_token) {
        fprintf(stderr, "Error: End of file after ");
        Cent_print_token(stderr, first_token);
        fprintf(stderr, " while parsing statement or declaration\n");
        return -1;
    }

    if (second_token->type == Cent_Token_Star
        || second_token->type == Cent_Token_Symbol) {
        int res = Cent_Parser_parse_declaration(parser, declaration);
        if (res == 1) {
            res = 2;
        }
        return res;
    }
    return Cent_Parser_parse_statement(parser, statement);
}

static int Cent_Parser_parse_base_declaration(Cent_Parser* parser,
                                              Cent_Parameter* parameter) {
    int res;
    Cent_Token name_token;
    res = Cent_Parser_parse_type(parser, &parameter->type);
    if (res == -1) {
        fprintf(stderr, "Error: While parsing a declaration\n");
        return -1;
    } else if (res == 0) {
        fprintf(stderr, "Error: End of file while parsing declaration\n");
        return 0;
    }

    res = Cent_Tokenizer_next(&parser->tokenizer, &name_token);
    if (res == 0) {
        fprintf(stderr, "Error: Could not parse name of declaration of type ");
        Cent_print_type(stderr, &parameter->type);
        fprintf(stderr, "\n");
        Cent_Type_destroy(&parameter->type);
        return -1;
    }
    if (name_token.type == Cent_Token_Symbol) {
        parameter->name = name_token.value.symbol;
        return 1;
    }

    fprintf(stderr,
            "Error: While parsing parameters, expected a symbol but got a ");
    Cent_print_token(stderr, &name_token);
    fprintf(stderr, "\n");
    Cent_Type_destroy(&parameter->type);
    return -1;
}

int Cent_Parser_parse_declaration(Cent_Parser* parser,
                                  Cent_Declaration* declaration) {
    Cent_Parameter parameter;
    Cent_Token semicolon_token;
    int res = Cent_Parser_parse_base_declaration(parser, &parameter);
    if (res <= 0) {
        fprintf(stderr, "Error: In parsing a declaration\n");
        return res;
    }

    declaration->type = Cent_Declaration_Variable;
    declaration->value.variable.type = parameter.type;
    declaration->value.variable.name = parameter.name;
    declaration->value.variable.value = 0;

parse_semicolon:
    if (!Cent_Tokenizer_next(&parser->tokenizer, &semicolon_token)) {
        fprintf(stderr,
                "Error: Found end of file when expected a semicolon after ");
        Cent_print_parameter(stderr, &parameter);
        fprintf(stderr, "\n");
        return -1;
    }
    if (semicolon_token.type == Cent_Token_Semicolon
        || semicolon_token.type == Cent_Token_Assign) {
        if (semicolon_token.type == Cent_Token_Semicolon) {
            return 1;
        } else {
            declaration->value.variable.value = malloc(sizeof(Cent_Expression));
            assert(declaration->value.variable.value);
            res = Cent_Parser_parse_expression(
                parser, declaration->value.variable.value);
            if (res <= 0) {
                fprintf(stderr, "Error: After ");
                Cent_print_type(stderr, &parameter.type);
                fprintf(stderr, " %s = \n", parameter.name);
                free(declaration->value.variable.value);
                return res;
            }
            goto parse_semicolon;
        }
    }

    fprintf(stderr, "Error: Expected a semicolon, found ");
    Cent_print_token(stderr, &semicolon_token);
    fprintf(stderr, " after ");
    Cent_print_parameter(stderr, &parameter);
    fprintf(stderr, "\n");
    return -1;
}

static int Cent_Parser_parse_expression_statement(Cent_Parser* parser,
                                                  Cent_Expression* expression) {
    Cent_Token semicolon_token;
    int res = Cent_Parser_parse_expression(parser, expression);
    if (res <= 0) {
        fprintf(stderr, "Error: While parsing an expression in a statement\n");
        return -1;
    }

    if (!Cent_Tokenizer_next(&parser->tokenizer, &semicolon_token)) {
        fprintf(
            stderr,
            "Error: End of file while expecting semicolon after expression ");
        Cent_print_expression(stderr, expression);
        fprintf(stderr, "\n");
        Cent_Expression_destroy(expression);
        return -1;
    }
    if (semicolon_token.type != Cent_Token_Semicolon) {
        fprintf(stderr, "Error: Expected a semicolon, found a ");
        Cent_print_token(stderr, &semicolon_token);
        fprintf(stderr, " after expression ");
        Cent_print_expression(stderr, expression);
        fprintf(stderr, "\n");
        Cent_Expression_destroy(expression);
        return -1;
    }
    return 1;
}

static int Cent_Parser_parse_condition_statement(Cent_Parser* parser,
                                                 Cent_Expression* condition,
                                                 Cent_Statement** body,
                                                 char* keyword) {
    int res;
    Cent_Token* paren = Cent_Tokenizer_peek(&parser->tokenizer, 0);
    if (!paren) {
        fprintf(stderr,
                "Error: End of file when expected %s statement condition\n",
                keyword);
        return -1;
    } else if (paren->type != Cent_Token_OpenParen) {
        fprintf(stderr, "Error: Expected open paren in %s statement, found ",
                keyword);
        Cent_print_token(stderr, paren);
        fprintf(stderr, "\n");
        return -1;
    }
    Cent_Tokenizer_pop_front(&parser->tokenizer);

    res = Cent_Parser_parse_expression(parser, condition);
    if (res <= 0) {
        fprintf(stderr, "Error: While parsing condition of %s statement\n",
                keyword);
        return -1;
    }

    paren = Cent_Tokenizer_peek(&parser->tokenizer, 0);
    if (!paren) {
        fprintf(
            stderr,
            "Error: End of file when expected close paren after %s statement "
            "condition:\n",
            keyword);
        fprintf(stderr, "Error: %s (", keyword);
        Cent_print_expression(stderr, condition);
        Cent_Expression_destroy(condition);
        return -1;
    } else if (paren->type != Cent_Token_CloseParen) {
        fprintf(stderr, "Error: Expected close paren, found ");
        Cent_print_token(stderr, paren);
        fprintf(stderr, " after:\n");
        fprintf(stderr, "Error: %s (, keyword", keyword);
        Cent_print_expression(stderr, condition);
        fprintf(stderr, "\n");
        Cent_Expression_destroy(condition);
        return -1;
    }
    Cent_Tokenizer_pop_front(&parser->tokenizer);

    {
        Cent_Statement* inner_statement = malloc(sizeof(Cent_Statement));
        assert(inner_statement);
        res = Cent_Parser_parse_statement(parser, inner_statement);
        if (res <= 0) {
            fprintf(stderr, "Error: Expected statement after:\n");
            fprintf(stderr, "Error: %s (", keyword);
            Cent_print_expression(stderr, condition);
            fprintf(stderr, ")\n");
            free(inner_statement);
            Cent_Expression_destroy(condition);
            return -1;
        }

        *body = inner_statement;
        return 1;
    }
}

static int Cent_Parser_parse_while_statement(Cent_Parser* parser,
                                             Cent_Statement* statement) {
    statement->type = Cent_Statement_While;
    return Cent_Parser_parse_condition_statement(
        parser, &statement->value.whiles.condition,
        &statement->value.whiles.body, "while");
}

static int Cent_Parser_parse_if_statement(Cent_Parser* parser,
                                          Cent_Statement* statement) {
    int res;
    statement->type = Cent_Statement_If;
    res = Cent_Parser_parse_condition_statement(
        parser, &statement->value.ifs.condition, &statement->value.ifs.trues,
        "if");
    if (res <= 0) {
        return -1;
    }

    {
        Cent_Token* else_token = Cent_Tokenizer_peek(&parser->tokenizer, 0);
        if (else_token && else_token->type == Cent_Token_Symbol
            && strcmp(else_token->value.symbol, "else") == 0) {
            Cent_Tokenizer_pop_front(&parser->tokenizer);
            statement->value.ifs.falses = malloc(sizeof(Cent_Statement));
            assert(statement->value.ifs.falses);

            res = Cent_Parser_parse_statement(parser,
                                              statement->value.ifs.falses);
            if (res <= 0) {
                fprintf(stderr, "Error: While parsing clause\n");
                free(statement->value.ifs.falses);
                Cent_Expression_destroy(&statement->value.ifs.condition);
                Cent_Statement_destroy(statement->value.ifs.trues);
                free(statement->value.ifs.trues);
                return -1;
            }
        } else {
            statement->value.ifs.falses = 0;
        }
        return 1;
    }
}

int Cent_Parser_parse_statement(Cent_Parser* parser,
                                Cent_Statement* statement) {
    int res;
    Cent_Token* token;

    token = Cent_Tokenizer_peek(&parser->tokenizer, 0);

    if (!token) {
        return 0;
    }
    if (token->type == Cent_Token_Semicolon) {
        Cent_Tokenizer_pop_front(&parser->tokenizer);
        statement->type = Cent_Statement_Empty;
        return 1;
    }
    if (token->type == Cent_Token_OpenCurly) {
        statement->type = Cent_Statement_Block;
        statement->value.block = malloc(sizeof(Cent_Block));
        res = Cent_Parser_parse_code_block(parser, statement->value.block);
        if (res <= 0) {
            free(statement->value.block);
        }
        return res;
    }
    if (token->type == Cent_Token_Symbol
        && strcmp(token->value.symbol, "return") == 0) {
        Cent_Tokenizer_pop_front(&parser->tokenizer);
        token = Cent_Tokenizer_peek(&parser->tokenizer, 0);
        if (!token) {
            fprintf(stderr,
                    "Error: Found end of file while trying to parse return "
                    "statement\n");
            return -1;
        }
        if (token->type == Cent_Token_Semicolon) {
            statement->type = Cent_Statement_Return;
            statement->value.returns.is_valid = 0;

            Cent_Tokenizer_pop_front(&parser->tokenizer);
        } else {
            Cent_Expression expression;

            if (Cent_Parser_parse_expression_statement(parser, &expression)
                < 0) {
                return -1;
            }

            statement->type = Cent_Statement_Return;
            statement->value.returns.is_valid = 1;
            statement->value.returns.expression = expression;
        }
        return 1;
    }
    if (token->type == Cent_Token_Symbol
        && (strcmp(token->value.symbol, "continue") == 0
            || strcmp(token->value.symbol, "break") == 0)) {
        Cent_StatementType statement_type;
        if (strcmp(token->value.symbol, "continue") == 0) {
            statement_type = Cent_Statement_Continue;
        } else {
            statement_type = Cent_Statement_Break;
        }
        Cent_Tokenizer_pop_front(&parser->tokenizer);
        token = Cent_Tokenizer_peek(&parser->tokenizer, 0);
        if (!token) {
            fprintf(
                stderr,
                "Error: Found end of file while trying to parse %s statement\n",
                token->value.symbol);
            return -1;
        }
        if (token->type == Cent_Token_Semicolon) {
            statement->type = statement_type;

            Cent_Tokenizer_pop_front(&parser->tokenizer);
            return 1;
        } else {
            fprintf(stderr,
                    "Error: Found end of file while trying to parse semicolon "
                    "after "
                    "%s statement\n",
                    token->value.symbol);
            return -1;
        }
    }
    if (token->type == Cent_Token_Symbol
        && strcmp(token->value.symbol, "while") == 0) {
        Cent_Tokenizer_pop_front(&parser->tokenizer);
        return Cent_Parser_parse_while_statement(parser, statement);
    }
    if (token->type == Cent_Token_Symbol
        && strcmp(token->value.symbol, "if") == 0) {
        Cent_Tokenizer_pop_front(&parser->tokenizer);
        return Cent_Parser_parse_if_statement(parser, statement);
    }

    {
        Cent_Expression expression;

        res = Cent_Parser_parse_expression_statement(parser, &expression);
        if (res < 0) {
            return res;
        }

        statement->type = Cent_Statement_Expression;
        statement->value.expression = expression;
        return 1;
    }
}
