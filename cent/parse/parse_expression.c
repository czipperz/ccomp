#include "panic.h"
#include "parse.h"
#include "print.h"
#include <string.h>

static int Cent_Parser_parse_arguments(Cent_Parser* parser,
                                       Cent_Expression* expression) {
    Deque arguments = Deque_new();
    Cent_Expression argument;
    int res;

    {
        Cent_Token* token = Cent_Tokenizer_peek(&parser->tokenizer, 0);
        if (token && token->type == Cent_Token_CloseParen) {
            Cent_Tokenizer_pop_front(&parser->tokenizer);
            goto ret;
        }
    }

    while (1) {
        res = Cent_Parser_parse_expression(parser, &argument);
        if (res < 0) {
            size_t i;
            fprintf(
                stderr,
                "Error: End of file while parsing an argument after\nError: (");
            for (i = 0; i < arguments.length; ++i) {
                if (i != 0) {
                    fprintf(stderr, ", ");
                }
                Cent_print_expression(
                    stderr, Deque_at(&arguments, i, sizeof(Cent_Expression)));
            }
            fprintf(stderr, "\n");
            return -1;
        } else if (res == 0) {
            size_t i;
            fprintf(
                stderr,
                "Error: End of file while parsing an argument after\nError: (");
            for (i = 0; i < arguments.length; ++i) {
                if (i != 0) {
                    fprintf(stderr, ", ");
                }
                Cent_print_expression(
                    stderr, Deque_at(&arguments, i, sizeof(Cent_Expression)));
            }
            fprintf(stderr, "\n");
            return -1;
        }

        {
            Cent_Token comma_token;
            res = Cent_Tokenizer_next(&parser->tokenizer, &comma_token);
            if (res <= 0) {
                size_t i;
                fprintf(stderr,
                        "Error: End of file while parsing an argument "
                        "after\nError: (");
                for (i = 0; i < arguments.length; ++i) {
                    if (i != 0) {
                        fprintf(stderr, ", ");
                    }
                    Cent_print_expression(
                        stderr,
                        Deque_at(&arguments, i, sizeof(Cent_Expression)));
                }
                fprintf(stderr, "\n");
                return -1;
            }
            if (comma_token.type == Cent_Token_Comma) {
                Deque_push_back(&arguments, &argument, sizeof(Cent_Expression));
            } else if (comma_token.type == Cent_Token_CloseParen) {
                Deque_push_back(&arguments, &argument, sizeof(Cent_Expression));
                break;
            } else {
                size_t i;
                fprintf(stderr, "Error: Expected comma, found ");
                Cent_print_token(stderr, &comma_token);
                fprintf(stderr, " after\nError: (");
                for (i = 0; i < arguments.length; ++i) {
                    if (i != 0) {
                        fprintf(stderr, ", ");
                    }
                    Cent_print_expression(
                        stderr,
                        Deque_at(&arguments, i, sizeof(Cent_Expression)));
                }
                fprintf(stderr, "\n");
                return -1;
            }
        }
    }

    {
        Cent_Expression* inner;
    ret:
        inner = malloc(sizeof(Cent_Expression));
        assert(inner);
        memcpy(inner, expression, sizeof(Cent_Expression));
        expression->type = Cent_Expression_FunctionCall;
        expression->value.function_call.function = inner;
        expression->value.function_call.num_arguments = arguments.length;
        expression->value.function_call.arguments = Deque_unwrap(&arguments);
        return 1;
    }
}

static void wrap_binary(Cent_Expression* expression, Cent_Operator operator,
                        Cent_Expression* value_raw) {
    Cent_Expression* left = malloc(sizeof(Cent_Expression));
    Cent_Expression* right = malloc(sizeof(Cent_Expression));
    assert(left);
    assert(right);
    *left = *expression;
    *right = *value_raw;
    expression->type = Cent_Expression_Binary;
    expression->value.binary.left = left;
    expression->value.binary.operator= operator;
    expression->value.binary.right = right;
}

static void wrap_dereference(Cent_Expression* expression) {
    Cent_Expression* new_expression = malloc(sizeof(Cent_Expression));
    assert(new_expression);
    memcpy(new_expression, expression, sizeof(Cent_Expression));
    expression->type = Cent_Expression_Dereference;
    expression->value.dereference.inner = new_expression;
}

enum Prefix {
    PrefixDereference,
};
typedef enum Prefix Prefix;

static void wrap_prefixes(Deque* prefix_stack, Cent_Expression* expression) {
    while (prefix_stack->length > 0) {
        Prefix prefix;
        Deque_pop_back(prefix_stack, &prefix, sizeof(Prefix));
        switch (prefix) {
        case PrefixDereference:
            wrap_dereference(expression);
            break;
        }
    }
}

static int expression_recurse(Cent_Parser* parser, Cent_Expression* expression,
                              Deque* prefix_stack) {
    int res;
    Cent_Token* first_token = Cent_Tokenizer_peek(&parser->tokenizer, 0);
    if (!first_token) {
        wrap_prefixes(prefix_stack, expression);
        return 1;
    }
    if (first_token->type == Cent_Token_OpenParen) {
        Cent_Tokenizer_pop_front(&parser->tokenizer);
        res = Cent_Parser_parse_arguments(parser, expression);
        if (res <= 0) {
            return res;
        }
        wrap_prefixes(prefix_stack, expression);
    } else if (first_token->type == Cent_Token_Assign) {
        Cent_Expression value;
        wrap_prefixes(prefix_stack, expression);
        Cent_Tokenizer_pop_front(&parser->tokenizer);
        res = Cent_Parser_parse_expression(parser, &value);
        if (res <= 0) {
            fprintf(stderr, "Error: After ");
            Cent_print_expression(stderr, expression);
            fprintf(stderr, " =\n");
            return res;
        }
        wrap_binary(expression, Cent_Operator_Assign, &value);
    } else {
        wrap_prefixes(prefix_stack, expression);
    }
    return 1;
}

int Cent_Parser_parse_expression(Cent_Parser* parser,
                                 Cent_Expression* expression) {
    Deque prefix_stack = Deque_new();
    Cent_Token* first_token = Cent_Tokenizer_peek(&parser->tokenizer, 0);
    if (!first_token) {
        return 0;
    }

    while (1) {
        if (!first_token) {
            fprintf(stderr, "Error: End of file when parsing an expression\n");
            return -1;
        }

        if (first_token->type == Cent_Token_Star) {
            Prefix prefix = PrefixDereference;
            Deque_push_back(&prefix_stack, &prefix, sizeof(Prefix));
            Cent_Tokenizer_pop_front(&parser->tokenizer);
            first_token = Cent_Tokenizer_peek(&parser->tokenizer, 0);
            continue;
        } else if (first_token->type == Cent_Token_OpenParen) {
            Cent_Tokenizer_pop_front(&parser->tokenizer);
            if (Cent_Parser_parse_expression(parser, expression) <= 0) {
                fprintf(stderr, "Error: Inside an open parenthesis\n");
                free(prefix_stack.buffer);
                return -1;
            }
            first_token = Cent_Tokenizer_peek(&parser->tokenizer, 0);
            if (!first_token || first_token->type != Cent_Token_CloseParen) {
                fprintf(stderr, "Error: End of file reached after (");
                Cent_print_expression(stderr, expression);
                fprintf(stderr, "\n");
                return -1;
            }
        } else if (first_token->type == Cent_Token_Symbol) {
            expression->type = Cent_Expression_Variable;
            expression->value.variable = first_token->value.symbol;
        } else if (first_token->type == Cent_Token_Constant) {
            expression->type = Cent_Expression_Constant;
            expression->value.constant = first_token->value.constant;
        } else {
            fprintf(stderr, "Error: Expected a symbol at start of expression ");
            Cent_print_token(stderr, first_token);
            fprintf(stderr, "\n");
            return -1;
        }

        Cent_Tokenizer_pop_front_no_destroy(&parser->tokenizer);
        {
            int res = expression_recurse(parser, expression, &prefix_stack);
            free(prefix_stack.buffer);
            return res;
        }
    }
}
