#include "panic.h"
#include "parse.h"
#include <stdlib.h>

static void wrap_pointer(Cent_Type* type) {
    Cent_Type* inner = malloc(sizeof(Cent_Type));
    assert(inner);
    *inner = *type;
    type->type = Cent_Type_Pointer;
    type->value.pointer.inner = inner;
}

int Cent_Parser_parse_type(Cent_Parser* parser, Cent_Type* type) {
    Cent_Token token;
    if (!Cent_Tokenizer_next(&parser->tokenizer, &token)) {
        return 0;
    }
    if (token.type != Cent_Token_Symbol) {
        fprintf(stderr, "Error: First token must be a symbol.  Found a ");
        Cent_print_token(stderr, &token);
        fprintf(stderr, "\n");
        return -1;
    }
    type->type = Cent_Type_Base;
    type->value.base.name = token.value.symbol;

    while (Cent_Tokenizer_next(&parser->tokenizer, &token)) {
        if (token.type == Cent_Token_Star) {
            wrap_pointer(type);
        } else {
            Cent_Tokenizer_push_front(&parser->tokenizer, &token);
            break;
        }
    }

    return 1;
}
