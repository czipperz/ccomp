#include "panic.h"
#include "parse.h"
#include "print.h"

static int parse_after_close_paren(Cent_Parser* parser,
                                   Cent_TopLevel* top_level, Deque* parameters,
                                   Cent_Type* return_type, char* name) {
    Cent_FunctionDeclaration function_declaration;
    Cent_Token semicolon_token;
    int res;
    function_declaration.return_type = *return_type;
    function_declaration.name = name;
    res = Cent_Tokenizer_next(&parser->tokenizer, &semicolon_token);
    if (res == 0) {
        size_t i;
        fprintf(stderr, "Error: End of file after end of parameters (");
        for (i = 0; i < parameters->length; ++i) {
            if (i != 0) {
                fprintf(stderr, ", ");
            }
            Cent_print_parameter(
                stderr, Deque_at(parameters, i, sizeof(Cent_Parameter)));
        }
        fprintf(stderr, ")\n");
        Deque_destroy(parameters, Cent_Parameter_destroy,
                      sizeof(Cent_Parameter));
        return -1;
    }

    if (semicolon_token.type == Cent_Token_Semicolon) {
        function_declaration.num_parameters = parameters->length;
        function_declaration.parameters = Deque_unwrap(parameters);

        top_level->type = Cent_TopLevel_Declaration;
        top_level->value.declaration.type = Cent_Declaration_Function;
        top_level->value.declaration.value.function = function_declaration;
        return 1;
    } else if (semicolon_token.type == Cent_Token_OpenCurly) {
        Cent_Tokenizer_push_front(&parser->tokenizer, &semicolon_token);

        function_declaration.num_parameters = parameters->length;
        function_declaration.parameters = Deque_unwrap(parameters);

        top_level->type = Cent_TopLevel_FunctionDefinition;
        top_level->value.function_definition.declaration = function_declaration;

        res = Cent_Parser_parse_code_block(
            parser, &top_level->value.function_definition.body);
        if (res <= 0) {
            size_t i;
            fprintf(stderr, "Error: In parsing function body after (");
            for (i = 0; i < parameters->length; ++i) {
                if (i != 0) {
                    fprintf(stderr, ", ");
                }
                Cent_print_parameter(
                    stderr, Deque_at(parameters, i, sizeof(Cent_Parameter)));
            }
            fprintf(stderr, ")\n");
            Deque_destroy(parameters, Cent_Parameter_destroy,
                          sizeof(Cent_Parameter));
            return -1;
        }
        return 1;
    } else {
        size_t i;
        fprintf(stderr, "Error: Expected semicolon or open curly but found ");
        Cent_print_token(stderr, &semicolon_token);
        fprintf(stderr, " after (");
        for (i = 0; i < parameters->length; ++i) {
            if (i != 0) {
                fprintf(stderr, ", ");
            }
            Cent_print_parameter(
                stderr, Deque_at(parameters, i, sizeof(Cent_Parameter)));
        }
        fprintf(stderr, ")\n");
        Deque_destroy(parameters, Cent_Parameter_destroy,
                      sizeof(Cent_Parameter));
        return -1;
    }
}

static int parse_top_level_function(Cent_Parser* parser,
                                    Cent_TopLevel* top_level,
                                    Cent_Type* return_type, char* name) {
    int res;
    Deque parameters = Deque_new();

    {
        Cent_Token* token;
        if ((token = Cent_Tokenizer_peek(&parser->tokenizer, 0))) {
            if (token->type == Cent_Token_CloseParen) {
                Cent_Tokenizer_pop_front(&parser->tokenizer);
                return parse_after_close_paren(parser, top_level, &parameters,
                                               return_type, name);
            }
        }
    }

    while (1) {
        Cent_Parameter parameter;
        Cent_Token comma_token;

        res = Cent_Parser_parse_parameter(parser, &parameter);
        if (res == -1) {
            size_t i;
            fprintf(stderr, "Error: While parsing top level function after (");
            for (i = 0; i < parameters.length; ++i) {
                Cent_print_parameter(
                    stderr, Deque_at(&parameters, i, sizeof(Cent_Parameter)));
                fprintf(stderr, ", ");
            }
            fprintf(stderr, "\n");
            Deque_destroy(&parameters, Cent_Parameter_destroy,
                          sizeof(Cent_Parameter));
            return -1;
        } else if (res == 0) {
            size_t i;
            fprintf(stderr,
                    "Error: End of file in middle of parsing top level "
                    "function after (");
            for (i = 0; i < parameters.length; ++i) {
                Cent_print_parameter(
                    stderr, Deque_at(&parameters, i, sizeof(Cent_Parameter)));
                fprintf(stderr, ", ");
            }
            fprintf(stderr, "\n");
            Deque_destroy(&parameters, Cent_Parameter_destroy,
                          sizeof(Cent_Parameter));
            return -1;
        }
        Deque_push_back(&parameters, &parameter, sizeof(Cent_Parameter));

        res = Cent_Tokenizer_next(&parser->tokenizer, &comma_token);
        if (res == 0) {
            size_t i;
            fprintf(stderr, "Error: End of file after parameters (");
            for (i = 0; i < parameters.length; ++i) {
                Cent_print_parameter(
                    stderr, Deque_at(&parameters, i, sizeof(Cent_Parameter)));
                fprintf(stderr, ", ");
            }
            Cent_print_parameter(stderr, &parameter);
            fprintf(stderr, "\n");
            Deque_destroy(&parameters, Cent_Parameter_destroy,
                          sizeof(Cent_Parameter));
            return -1;
        }

        if (comma_token.type == Cent_Token_Comma) {
        } else if (comma_token.type == Cent_Token_CloseParen) {
            return parse_after_close_paren(parser, top_level, &parameters,
                                           return_type, name);
        } else {
            fprintf(stderr, "Error: Expected comma or close paren but found ");
            Cent_print_token(stderr, &comma_token);
            fprintf(stderr, "\n");
            Deque_destroy(&parameters, Cent_Parameter_destroy,
                          sizeof(Cent_Parameter));
            return -1;
        }
    }
}

int Cent_Parser_next_top_level(Cent_Parser* parser, Cent_TopLevel* top_level) {
    while (1) {
        Cent_Token token;
        if (!Cent_Tokenizer_next(&parser->tokenizer, &token)) {
            return 0;
        }
        if (token.type != Cent_Token_Semicolon) {
            Cent_Tokenizer_push_front(&parser->tokenizer, &token);
            break;
        }
    }

    {
        Cent_Type type;
        Cent_Token name_token;
        Cent_Token semicolon_token;
        int res = Cent_Parser_parse_type(parser, &type);
        if (res == -1) {
            fprintf(stderr,
                    "Error: Could not parse start of top level object\n");
            return -1;
        }

        res = Cent_Tokenizer_next(&parser->tokenizer, &name_token);
        if (res == 0) {
            fprintf(stderr,
                    "Error: Could not parse name of top level of type ");
            Cent_print_type(stderr, &type);
            fprintf(stderr, "\n");
            Cent_Type_destroy(&type);
            return -1;
        }
        if (name_token.type != Cent_Token_Symbol) {
            fprintf(stderr,
                    "Error: In parsing top level object, expected a symbol but "
                    "got a ");
            Cent_print_token(stderr, &name_token);
            fprintf(stderr, " after type ");
            Cent_print_type(stderr, &type);
            fprintf(stderr, "\n");
            Cent_Type_destroy(&type);
            return -1;
        }

        res = Cent_Tokenizer_next(&parser->tokenizer, &semicolon_token);
        if (res == 0) {
            fprintf(stderr, "Error: Could not parse semicolon after ");
            Cent_print_type(stderr, &type);
            fprintf(stderr, " %s\n", name_token.value.symbol);
            Cent_Token_destroy(&name_token);
            Cent_Type_destroy(&type);
            return -1;
        }
        if (semicolon_token.type == Cent_Token_Semicolon) {
            top_level->type = Cent_TopLevel_Declaration;
            top_level->value.declaration.type = Cent_Declaration_Variable;
            top_level->value.declaration.value.variable.type = type;
            top_level->value.declaration.value.variable.name = name_token.value
                                                                   .symbol;
            top_level->value.declaration.value.variable.value = 0;
            return 1;
        } else if (semicolon_token.type == Cent_Token_OpenParen) {
            res = parse_top_level_function(parser, top_level, &type,
                                           name_token.value.symbol);
            if (res == -1) {
                fprintf(stderr, "Error: In parsing top level object, after ");
                Cent_print_type(stderr, &type);
                fprintf(stderr, " %s\n", name_token.value.symbol);
                Cent_Token_destroy(&name_token);
                Cent_Type_destroy(&type);
                return -1;
            }
            return res;
        } else {
            fprintf(stderr,
                    "Error: In parsing top level object, expected a "
                    "semicolon or open paren but got a ");
            Cent_print_token(stderr, &semicolon_token);
            fprintf(stderr, "\n");
            Cent_Token_destroy(&name_token);
            Cent_Type_destroy(&type);
            return -1;
        }
    }
    return 0;
}
