#include "panic.h"
#include "parse.h"

Cent_Parser Cent_Parser_new(Cent_Tokenizer tokenizer) {
    Cent_Parser parser;
    parser.tokenizer = tokenizer;
    return parser;
}

int Cent_Parser_open_file(Cent_Parser* parser, const char* file_name) {
    FileReader reader;
    int res = FileReader_open_file(&reader, file_name);
    if (res) {
        return res;
    }
    *parser = Cent_Parser_new(Cent_Tokenizer_new(reader));
    return 0;
}
Cent_Parser Cent_Parser_from_file(FILE* file) {
    return Cent_Parser_new(Cent_Tokenizer_new(FileReader_from_file(file)));
}
Cent_Parser Cent_Parser_from_string(const char* file_contents) {
    return Cent_Parser_new(
        Cent_Tokenizer_new(FileReader_from_string(file_contents)));
}

void Cent_Parser_destroy(Cent_Parser* parser) {
    Cent_Tokenizer_destroy(&parser->tokenizer);
}
