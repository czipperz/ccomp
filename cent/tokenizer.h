#pragma once

#include "file_reader.h"
#include "syntax/constant.h"
#include <stdint.h>
#include <stdio.h>

enum Cent_TokenType {
    Cent_Token_Symbol,
    Cent_Token_OpenCurly,
    Cent_Token_CloseCurly,
    Cent_Token_OpenParen,
    Cent_Token_CloseParen,
    Cent_Token_Star,
    Cent_Token_Semicolon,
    Cent_Token_Comma,
    Cent_Token_Assign,
    Cent_Token_Constant
};
typedef enum Cent_TokenType Cent_TokenType;

union Cent_TokenValue {
    char* symbol;
    Cent_Constant constant;
};
typedef union Cent_TokenValue Cent_TokenValue;

struct Cent_Token {
    Cent_TokenType type;
    Cent_TokenValue value;
};
typedef struct Cent_Token Cent_Token;

struct Cent_Tokenizer {
    FileReader file_reader;
    Deque token_queue;
};
typedef struct Cent_Tokenizer Cent_Tokenizer;

/**
 * Create a new tokenizer from a FileReader.
 *
 * # Notes
 *
 * This function will never fail.
 */
Cent_Tokenizer Cent_Tokenizer_new(FileReader);

/**
 * Get the next token.  Return if a token was retrieved.
 */
int Cent_Tokenizer_next(Cent_Tokenizer*, Cent_Token*);

/**
 * Peek index tokens into the future.
 *
 * Returns NULL if end of file is reached.
 */
Cent_Token* Cent_Tokenizer_peek(Cent_Tokenizer*, size_t index);

/**
 * Make the next call to [`Tokenizer_next`] return this token.
 *
 * [`Tokenizer_next`]: #Tokenizer_next
 */
void Cent_Tokenizer_push_front(Cent_Tokenizer*, const Cent_Token*);

/**
 * Pop the front element of the queue, panicing if it doesn't exist.
 */
void Cent_Tokenizer_pop_front(Cent_Tokenizer*);

/**
 * Pop the front element off the queue and don't destroy it.
 *
 * Panicks if it doesn't exist.
 */
void Cent_Tokenizer_pop_front_no_destroy(Cent_Tokenizer*);

void Cent_Tokenizer_destroy(Cent_Tokenizer*);

void Cent_Token_destroy(Cent_Token*);

void Cent_serialize_append_token(DequeString*, const Cent_Token*);
char* Cent_serialize_token(const Cent_Token*);
void Cent_print_token(FILE* file, const Cent_Token*);
