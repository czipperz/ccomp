#pragma once

#include "deque_string.h"
#include <stdio.h>

struct FileReader {
    FILE* file;
    DequeString buffer;
};
typedef struct FileReader FileReader;

int FileReader_open_file(FileReader* file_reader, const char* file_name);
FileReader FileReader_from_file(FILE* file);
FileReader FileReader_from_string(const char* file_contents);
void FileReader_close(FileReader* file_reader);

char FileReader_read(FileReader* file_reader);
char FileReader_peek(FileReader* file_reader, size_t offset);
void FileReader_push_front(FileReader* file_reader, char c);
