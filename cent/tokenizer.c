#include "tokenizer.h"
#include "panic.h"
#include "serialize.h"
#include <ctype.h>
#include <inttypes.h>
#include <stdlib.h>

Cent_Tokenizer Cent_Tokenizer_new(FileReader file_reader) {
    Cent_Tokenizer tokenizer = { file_reader, Deque_new() };
    return tokenizer;
}

static int identify_char(char ch, Cent_TokenType* type) {
    char singletons[] = { '{', '}', '(', ')', '*', ';', ',', '=' };
    Cent_TokenType singletonTypes[] = {
        Cent_Token_OpenCurly,  Cent_Token_CloseCurly, Cent_Token_OpenParen,
        Cent_Token_CloseParen, Cent_Token_Star,       Cent_Token_Semicolon,
        Cent_Token_Comma,      Cent_Token_Assign
    };
    size_t i;
    for (i = 0; i < sizeof(singletons); ++i) {
        if (ch == singletons[i]) {
            *type = singletonTypes[i];
            return 1;
        }
    }
    if (isspace(ch)) {
        return 0;
    }
    if (isdigit(ch)) {
        return 2;
    }
    if (ch == '"') {
        return 3;
    }
    if (isalpha(ch) || ch == '_') {
        return 4;
    }
    unimplemented();
}

static int valueof(char ch) { return ch - '0'; }

static int parse_constant_integer(Cent_Tokenizer* tokenizer,
                                  Cent_Token* token) {
    uintmax_t constant_value = 0;
    char ch;
    while ((ch = FileReader_read(&tokenizer->file_reader))) {
        if (isdigit(ch)) {
            constant_value *= 10;
            constant_value += valueof(ch);
        } else if (isalpha(ch)) {
            fprintf(stderr,
                    "Error: Constant value %" PRIuMAX
                    " followed by letter %c\n",
                    constant_value, ch);
            unimplemented();
            return -1;
        } else {
            FileReader_push_front(&tokenizer->file_reader, ch);
            break;
        }
    }

    token->type = Cent_Token_Constant;
    token->value.constant.type = Cent_Constant_Integer;
    token->value.constant.value.integer = constant_value;
    return 1;
}

static int parse_constant_string(Cent_Tokenizer* tokenizer, Cent_Token* token) {
    DequeString string = DequeString_new();
    char ch;
    while ((ch = FileReader_read(&tokenizer->file_reader))) {
        if (ch == '"') {
            break;
        } else if (ch == '\\') {
            unimplemented();
        } else {
            DequeString_push_back(&string, ch);
        }
    }
    if (ch == 0) {
        fprintf(stderr, "Error: Unterminated string %s\n",
                DequeString_unwrap(&string));
        unimplemented();
        return -1;
    }

    DequeString_push_back(&string, 0);

    token->type = Cent_Token_Constant;
    token->value.constant.type = Cent_Constant_String;
    token->value.constant.value.string = DequeString_unwrap(&string);
    return 1;
}

static int Cent_Tokenizer_read_next(Cent_Tokenizer* tokenizer,
                                    Cent_Token* token) {
    DequeString symbol = DequeString_new();
    while (1) {
        char ch = FileReader_read(&tokenizer->file_reader);
        if (ch == 0) {
            if (symbol.deque.length == 0) {
                return 0;
            } else {
                break;
            }
        } else {
            Cent_TokenType type;
            int res = identify_char(ch, &type);
            if (res == 0) {
                if (symbol.deque.length != 0) {
                    break;
                }
            } else if (res == 1) {
                if (symbol.deque.length == 0) {
                    token->type = type;
                    return 1;
                } else {
                    FileReader_push_front(&tokenizer->file_reader, ch);
                    break;
                }
            } else if (res == 2) {
                if (symbol.deque.length != 0) {
                    DequeString_push_back(&symbol, ch);
                } else {
                    FileReader_push_front(&tokenizer->file_reader, ch);
                    return parse_constant_integer(tokenizer, token);
                }
            } else if (res == 3) {
                return parse_constant_string(tokenizer, token);
            } else if (res == 4) {
                DequeString_push_back(&symbol, ch);
            } else {
                unreachable();
            }
        }
    }
    DequeString_push_back(&symbol, '\0');
    token->type = Cent_Token_Symbol;
    token->value.symbol = DequeString_unwrap(&symbol);
    return 1;
}

Cent_Token* Cent_Tokenizer_peek(Cent_Tokenizer* tokenizer, size_t index) {
    while (index >= tokenizer->token_queue.length) {
        Cent_Token token;
        if (!Cent_Tokenizer_read_next(tokenizer, &token)) {
            return 0;
        }
        Deque_push_back(&tokenizer->token_queue, &token, sizeof(Cent_Token));
    }

    return Deque_at(&tokenizer->token_queue, index, sizeof(Cent_Token));
}

int Cent_Tokenizer_next(Cent_Tokenizer* tokenizer, Cent_Token* token) {
    if (tokenizer->token_queue.length >= 1) {
        Deque_pop_front(&tokenizer->token_queue, token, sizeof(Cent_Token));
        return 1;
    }
    return Cent_Tokenizer_read_next(tokenizer, token);
}

void Cent_Tokenizer_push_front(Cent_Tokenizer* tokenizer,
                               const Cent_Token* token) {
    Deque_push_front(&tokenizer->token_queue, token, sizeof(Cent_Token));
}

void Cent_Tokenizer_pop_front(Cent_Tokenizer* tokenizer) {
    Cent_Token token;
    Deque_pop_front(&tokenizer->token_queue, &token, sizeof(Cent_Token));
    Cent_Token_destroy(&token);
}

void Cent_Tokenizer_pop_front_no_destroy(Cent_Tokenizer* tokenizer) {
    Cent_Token token;
    Deque_pop_front(&tokenizer->token_queue, &token, sizeof(Cent_Token));
}

void Cent_Tokenizer_destroy(Cent_Tokenizer* tokenizer) {
    FileReader_close(&tokenizer->file_reader);
    Deque_destroy(&tokenizer->token_queue, Cent_Token_destroy,
                  sizeof(Cent_Token));
}

void Cent_Token_destroy(Cent_Token* token) {
    if (token->type == Cent_Token_Symbol) {
        free(token->value.symbol);
    }
}

void Cent_serialize_append_token(DequeString* string, const Cent_Token* token) {
    switch (token->type) {
    case Cent_Token_Symbol:
        DequeString_push_back_str(string, token->value.symbol);
        break;
    case Cent_Token_OpenCurly:
        DequeString_push_back(string, '{');
        break;
    case Cent_Token_CloseCurly:
        DequeString_push_back(string, '}');
        break;
    case Cent_Token_OpenParen:
        DequeString_push_back(string, '(');
        break;
    case Cent_Token_CloseParen:
        DequeString_push_back(string, ')');
        break;
    case Cent_Token_Star:
        DequeString_push_back(string, '*');
        break;
    case Cent_Token_Semicolon:
        DequeString_push_back(string, ';');
        break;
    case Cent_Token_Comma:
        DequeString_push_back(string, ',');
        break;
    case Cent_Token_Assign:
        DequeString_push_back(string, '=');
        break;
    case Cent_Token_Constant:
        Cent_serialize_append_constant_expression(string,
                                                  &token->value.constant);
        break;
    }
}

char* Cent_serialize_token(const Cent_Token* token) {
    DequeString string = DequeString_new();
    Cent_serialize_append_token(&string, token);
    return DequeString_unwrap(&string);
}

void Cent_print_token(FILE* file, const Cent_Token* token) {
    char* serialized = Cent_serialize_token(token);
    fputs(serialized, file);
    free(serialized);
}
