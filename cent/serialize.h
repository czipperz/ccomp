#pragma once

#include "deque_string.h"
#include "syntax.h"

/*** Serialize an object to a string.  This string must be `free`d. ***/
char* Cent_serialize_variable_name(const char*);

char* Cent_serialize_base_type(const Cent_BaseType*);
char* Cent_serialize_pointer_type(const Cent_PointerType*);
char* Cent_serialize_type(const Cent_Type*);

char* Cent_serialize_parameter(const Cent_Parameter*);

char* Cent_serialize_integer_constant_expression(const uintmax_t);
char* Cent_serialize_string_constant_expression(const char*);
char* Cent_serialize_constant_expression(const Cent_Constant*);
char* Cent_serialize_variable_expression(const char*);
char* Cent_serialize_argument(const Cent_Expression*);
char* Cent_serialize_function_call_expression(
    const Cent_FunctionCallExpression*);
char* Cent_serialize_dereference_expression(const Cent_DereferenceExpression*);
char* Cent_serialize_binary_operator(const Cent_Operator);
char* Cent_serialize_binary_expression(const Cent_BinaryExpression*);
char* Cent_serialize_expression(const Cent_Expression*);
char* Cent_serialize_condition_expression(const Cent_Expression*);

char* Cent_serialize_function_declaration(const Cent_FunctionDeclaration*);
char* Cent_serialize_variable_declaration(const Cent_VariableDeclaration*);
char* Cent_serialize_declaration(const Cent_Declaration*);
char* Cent_serialize_block_declaration(const Cent_Declaration*);
char* Cent_serialize_top_level_declaration(const Cent_Declaration*);

char* Cent_serialize_expression_statement(const Cent_Expression*);
char* Cent_serialize_block_statement(const Cent_Block*);
char* Cent_serialize_return_statement(const Cent_ReturnStatement*);
char* Cent_serialize_continue_statement(void);
char* Cent_serialize_break_statement(void);
char* Cent_serialize_while_statement(const Cent_WhileStatement*);
char* Cent_serialize_if_statement(const Cent_IfStatement*);
char* Cent_serialize_empty_statement(void);
char* Cent_serialize_statement(const Cent_Statement*);

char* Cent_serialize_block(const Cent_Block*);
char* Cent_serialize_top_level_block(const Cent_Block*);
char* Cent_serialize_function_definition(const Cent_FunctionDefinition*);
char* Cent_serialize_top_level(const Cent_TopLevel*);

/*** Serialize an object, appending it to a string. ***/
void Cent_serialize_append_variable_name(DequeString*, const char*);

void Cent_serialize_append_base_type(DequeString*, const Cent_BaseType*);
void Cent_serialize_append_pointer_type(DequeString*, const Cent_PointerType*);
void Cent_serialize_append_type(DequeString*, const Cent_Type*);

void Cent_serialize_append_parameter(DequeString*, const Cent_Parameter*);

void Cent_serialize_append_integer_constant_expression(DequeString*,
                                                       const uintmax_t);
void Cent_serialize_append_string_constant_expression(DequeString*,
                                                      const char*);
void Cent_serialize_append_constant_expression(DequeString*,
                                               const Cent_Constant*);
void Cent_serialize_append_variable_expression(DequeString*, const char*);
void Cent_serialize_append_argument(DequeString*, const Cent_Expression*);
void Cent_serialize_append_function_call_expression(
    DequeString*, const Cent_FunctionCallExpression*);
void Cent_serialize_append_dereference_expression(
    DequeString*, const Cent_DereferenceExpression*);
void Cent_serialize_append_binary_operator(DequeString*, const Cent_Operator);
void Cent_serialize_append_binary_expression(DequeString*,
                                             const Cent_BinaryExpression*);
void Cent_serialize_append_expression(DequeString*, const Cent_Expression*);
void Cent_serialize_append_condition_expression(DequeString*,
                                                const Cent_Expression*);

void Cent_serialize_append_function_declaration(
    DequeString*, const Cent_FunctionDeclaration*);
void Cent_serialize_append_variable_declaration(
    DequeString*, const Cent_VariableDeclaration*);
void Cent_serialize_append_declaration(DequeString*, const Cent_Declaration*);
void Cent_serialize_append_block_declaration(DequeString*,
                                             const Cent_Declaration*);
void Cent_serialize_append_top_level_declaration(DequeString*,
                                                 const Cent_Declaration*);

void Cent_serialize_append_expression_statement(DequeString*,
                                                const Cent_Expression*);
void Cent_serialize_append_block_statement(DequeString*, const Cent_Block*);
void Cent_serialize_append_return_statement(DequeString*,
                                            const Cent_ReturnStatement*);
void Cent_serialize_append_continue_statement(DequeString*);
void Cent_serialize_append_break_statement(DequeString*);
void Cent_serialize_append_while_statement(DequeString*,
                                           const Cent_WhileStatement*);
void Cent_serialize_append_if_statement(DequeString*, const Cent_IfStatement*);
void Cent_serialize_append_empty_statement(DequeString*);
void Cent_serialize_append_statement(DequeString*, const Cent_Statement*);

void Cent_serialize_append_block(DequeString*, const Cent_Block*);
void Cent_serialize_append_top_level_block(DequeString*, const Cent_Block*);
void Cent_serialize_append_function_definition(DequeString*,
                                               const Cent_FunctionDefinition*);
void Cent_serialize_append_top_level(DequeString*, const Cent_TopLevel*);
