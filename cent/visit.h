#pragma once

#include "parse.h"

/**
 * A visitor to visit a top level object.
 */
struct Cent_TopLevelVisitor {
    void (*visit_variable_name)(void*, char*);

    void (*visit_base_type)(void*, Cent_BaseType*);
    void (*visit_pointer_type)(void*, Cent_PointerType*);
    void (*visit_type)(void*, Cent_Type*);

    void (*visit_parameter)(void*, Cent_Parameter*);

    void (*visit_integer_constant_expression)(void*, uintmax_t);
    void (*visit_string_constant_expression)(void*, char*);
    void (*visit_constant_expression)(void*, Cent_Constant*);
    void (*visit_variable_expression)(void*, char*);
    void (*visit_argument)(void*, Cent_Expression*);
    void (*visit_function_call_expression)(void*, Cent_FunctionCallExpression*);
    void (*visit_dereference_expression)(void*, Cent_DereferenceExpression*);
    void (*visit_binary_operator)(void*, Cent_Operator);
    void (*visit_binary_expression)(void*, Cent_BinaryExpression*);
    void (*visit_expression)(void*, Cent_Expression*);
    void (*visit_condition_expression)(void*, Cent_Expression*);

    void (*visit_function_declaration)(void*, Cent_FunctionDeclaration*);
    void (*visit_variable_declaration)(void*, Cent_VariableDeclaration*);
    void (*visit_declaration)(void*, Cent_Declaration*);
    void (*visit_block_declaration)(void*, Cent_Declaration*);
    void (*visit_top_level_declaration)(void*, Cent_Declaration*);

    void (*visit_expression_statement)(void*, Cent_Expression*);
    void (*visit_block_statement)(void*, Cent_Block*);
    void (*visit_return_statement)(void*, Cent_ReturnStatement*);
    void (*visit_continue_statement)(void*);
    void (*visit_break_statement)(void*);
    void (*visit_while_statement)(void*, Cent_WhileStatement*);
    void (*visit_if_statement)(void*, Cent_IfStatement*);
    void (*visit_empty_statement)(void*);
    void (*visit_statement)(void*, Cent_Statement*);

    void (*visit_block)(void*, Cent_Block*);
    void (*visit_top_level_block)(void*, Cent_Block*);
    void (*visit_function_definition)(void*, Cent_FunctionDefinition*);
    void (*visit_top_level)(void*, Cent_TopLevel*);
};
typedef struct Cent_TopLevelVisitor Cent_TopLevelVisitor;

/**
 * Make a new visitor to recursively visit the entire top level
 * object.
 *
 * To actually visit an object, call an `Cent_TopLevelVisitor_accept`
 * function.  This wraps the relevant function pointer into a prettier
 * interface.
 *
 * This sets each function pointer to the relevant
 * `Cent_TopLevelVisitor_visit_` function.
 *
 * If you override one of these methods (to actually have it perform
 * custom behavior) and would like it to still act recursively, call
 * the relevant `Cent_TopLevelVisitor_visit` function.
 *
 * For example, if you override `visit_statement`, call
 * `Cent_TopLevelVisitor_visit_statement` to recurse properly.
 */
Cent_TopLevelVisitor Cent_TopLevelVisitor_new(void);

/*** The accept functions call the relevant function pointer in the visitor ***/
void Cent_TopLevelVisitor_accept_variable_name(Cent_TopLevelVisitor*, char*);

void Cent_TopLevelVisitor_accept_base_type(Cent_TopLevelVisitor*,
                                           Cent_BaseType*);
void Cent_TopLevelVisitor_accept_pointer_type(Cent_TopLevelVisitor*,
                                              Cent_PointerType*);
void Cent_TopLevelVisitor_accept_type(Cent_TopLevelVisitor*, Cent_Type*);

void Cent_TopLevelVisitor_accept_parameter(Cent_TopLevelVisitor*,
                                           Cent_Parameter*);

void Cent_TopLevelVisitor_accept_integer_constant_expression(
    Cent_TopLevelVisitor*, uintmax_t);
void Cent_TopLevelVisitor_accept_string_constant_expression(
    Cent_TopLevelVisitor*, char*);
void Cent_TopLevelVisitor_accept_constant_expression(Cent_TopLevelVisitor*,
                                                     Cent_Constant*);
void Cent_TopLevelVisitor_accept_variable_expression(Cent_TopLevelVisitor*,
                                                     char*);
void Cent_TopLevelVisitor_accept_argument(Cent_TopLevelVisitor*,
                                          Cent_Expression*);
void Cent_TopLevelVisitor_accept_function_call_expression(
    Cent_TopLevelVisitor*, Cent_FunctionCallExpression*);
void Cent_TopLevelVisitor_accept_dereference_expression(
    Cent_TopLevelVisitor*, Cent_DereferenceExpression*);
void Cent_TopLevelVisitor_accept_binary_operator(Cent_TopLevelVisitor*,
                                                 Cent_Operator);
void Cent_TopLevelVisitor_accept_binary_expression(Cent_TopLevelVisitor*,
                                                   Cent_BinaryExpression*);
void Cent_TopLevelVisitor_accept_expression(Cent_TopLevelVisitor*,
                                            Cent_Expression*);
void Cent_TopLevelVisitor_accept_condition_expression(Cent_TopLevelVisitor*,
                                                      Cent_Expression*);

void Cent_TopLevelVisitor_accept_function_declaration(
    Cent_TopLevelVisitor*, Cent_FunctionDeclaration*);
void Cent_TopLevelVisitor_accept_variable_declaration(
    Cent_TopLevelVisitor*, Cent_VariableDeclaration*);
void Cent_TopLevelVisitor_accept_declaration(Cent_TopLevelVisitor*,
                                             Cent_Declaration*);
void Cent_TopLevelVisitor_accept_block_declaration(Cent_TopLevelVisitor*,
                                                   Cent_Declaration*);
void Cent_TopLevelVisitor_accept_top_level_declaration(Cent_TopLevelVisitor*,
                                                       Cent_Declaration*);

void Cent_TopLevelVisitor_accept_expression_statement(Cent_TopLevelVisitor*,
                                                      Cent_Expression*);
void Cent_TopLevelVisitor_accept_block_statement(Cent_TopLevelVisitor*,
                                                 Cent_Block*);
void Cent_TopLevelVisitor_accept_return_statement(Cent_TopLevelVisitor*,
                                                  Cent_ReturnStatement*);
void Cent_TopLevelVisitor_accept_continue_statement(Cent_TopLevelVisitor*);
void Cent_TopLevelVisitor_accept_break_statement(Cent_TopLevelVisitor*);
void Cent_TopLevelVisitor_accept_while_statement(Cent_TopLevelVisitor*,
                                                 Cent_WhileStatement*);
void Cent_TopLevelVisitor_accept_if_statement(Cent_TopLevelVisitor*,
                                              Cent_IfStatement*);
void Cent_TopLevelVisitor_accept_empty_statement(Cent_TopLevelVisitor*);
void Cent_TopLevelVisitor_accept_statement(Cent_TopLevelVisitor*,
                                           Cent_Statement*);

void Cent_TopLevelVisitor_accept_block(Cent_TopLevelVisitor*, Cent_Block*);
void Cent_TopLevelVisitor_accept_top_level_block(Cent_TopLevelVisitor*,
                                                 Cent_Block*);
void Cent_TopLevelVisitor_accept_function_definition(Cent_TopLevelVisitor*,
                                                     Cent_FunctionDefinition*);
void Cent_TopLevelVisitor_accept_top_level(Cent_TopLevelVisitor*,
                                           Cent_TopLevel*);

/*** The visit functions implement recursive visiting of the input ***/
void Cent_TopLevelVisitor_visit_variable_name(void*, char*);

void Cent_TopLevelVisitor_visit_base_type(void*, Cent_BaseType*);
void Cent_TopLevelVisitor_visit_pointer_type(void*, Cent_PointerType*);
void Cent_TopLevelVisitor_visit_type(void*, Cent_Type*);

void Cent_TopLevelVisitor_visit_parameter(void*, Cent_Parameter*);

void Cent_TopLevelVisitor_visit_integer_constant_expression(void*, uintmax_t);
void Cent_TopLevelVisitor_visit_string_constant_expression(void*, char*);
void Cent_TopLevelVisitor_visit_constant_expression(void*, Cent_Constant*);
void Cent_TopLevelVisitor_visit_variable_expression(void*, char*);
void Cent_TopLevelVisitor_visit_argument(void*, Cent_Expression*);
void Cent_TopLevelVisitor_visit_function_call_expression(
    void*, Cent_FunctionCallExpression*);
void Cent_TopLevelVisitor_visit_dereference_expression(
    void*, Cent_DereferenceExpression*);
void Cent_TopLevelVisitor_visit_binary_operator(void*, Cent_Operator);
void Cent_TopLevelVisitor_visit_binary_expression(void*,
                                                  Cent_BinaryExpression*);
void Cent_TopLevelVisitor_visit_expression(void*, Cent_Expression*);
void Cent_TopLevelVisitor_visit_condition_expression(void*, Cent_Expression*);

void Cent_TopLevelVisitor_visit_function_declaration(void*,
                                                     Cent_FunctionDeclaration*);
void Cent_TopLevelVisitor_visit_variable_declaration(void*,
                                                     Cent_VariableDeclaration*);
void Cent_TopLevelVisitor_visit_declaration(void*, Cent_Declaration*);
void Cent_TopLevelVisitor_visit_block_declaration(void*, Cent_Declaration*);
void Cent_TopLevelVisitor_visit_top_level_declaration(void*, Cent_Declaration*);

void Cent_TopLevelVisitor_visit_expression_statement(void*, Cent_Expression*);
void Cent_TopLevelVisitor_visit_block_statement(void*, Cent_Block*);
void Cent_TopLevelVisitor_visit_return_statement(void*, Cent_ReturnStatement*);
void Cent_TopLevelVisitor_visit_continue_statement(void*);
void Cent_TopLevelVisitor_visit_break_statement(void*);
void Cent_TopLevelVisitor_visit_while_statement(void*, Cent_WhileStatement*);
void Cent_TopLevelVisitor_visit_if_statement(void*, Cent_IfStatement*);
void Cent_TopLevelVisitor_visit_empty_statement(void*);
void Cent_TopLevelVisitor_visit_statement(void*, Cent_Statement*);

void Cent_TopLevelVisitor_visit_block(void*, Cent_Block*);
void Cent_TopLevelVisitor_visit_top_level_block(void*, Cent_Block*);
void Cent_TopLevelVisitor_visit_function_definition(void*,
                                                    Cent_FunctionDefinition*);
void Cent_TopLevelVisitor_visit_top_level(void*, Cent_TopLevel*);
