#include "print.h"
#include "serialize.h"
#include <stdlib.h>

#define make_print(function, type)                                             \
    void Cent_print_##function(FILE* file, type x) {                           \
        char* serialized = Cent_serialize_##function(x);                       \
        fputs(serialized, file);                                               \
        free(serialized);                                                      \
    }

#define make_print_empty(function)                                             \
    void Cent_print_##function(FILE* file) {                                   \
        char* serialized = Cent_serialize_##function();                        \
        fputs(serialized, file);                                               \
        free(serialized);                                                      \
    }

make_print(variable_name, char*);

make_print(base_type, Cent_BaseType*);
make_print(pointer_type, Cent_PointerType*);
make_print(type, Cent_Type*);

make_print(parameter, Cent_Parameter*);

make_print(integer_constant_expression, uintmax_t);
make_print(string_constant_expression, char*);
make_print(constant_expression, Cent_Constant*);
make_print(variable_expression, char*);
make_print(argument, Cent_Expression*);
make_print(function_call_expression, Cent_FunctionCallExpression*);
make_print(dereference_expression, Cent_DereferenceExpression*);
make_print(binary_operator, Cent_Operator);
make_print(binary_expression, Cent_BinaryExpression*);
make_print(expression, Cent_Expression*);
make_print(condition_expression, Cent_Expression*);

make_print(function_declaration, Cent_FunctionDeclaration*);
make_print(variable_declaration, Cent_VariableDeclaration*);
make_print(declaration, Cent_Declaration*);
make_print(block_declaration, Cent_Declaration*);
make_print(top_level_declaration, Cent_Declaration*);

make_print(expression_statement, Cent_Expression*);
make_print(block_statement, Cent_Block*);
make_print(return_statement, Cent_ReturnStatement*);
make_print_empty(continue_statement);
make_print_empty(break_statement);
make_print(while_statement, Cent_WhileStatement*);
make_print(if_statement, Cent_IfStatement*);
make_print_empty(empty_statement);
make_print(statement, Cent_Statement*);

make_print(block, Cent_Block*);
make_print(top_level_block, Cent_Block*);
make_print(function_definition, Cent_FunctionDefinition*);
make_print(top_level, Cent_TopLevel*);
