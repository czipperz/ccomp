#include "file_reader.h"
#include <stdlib.h>

int FileReader_open_file(FileReader* file_reader, const char* file_name) {
    FILE* file = fopen(file_name, "r");
    if (!file) {
        return -1;
    }
    *file_reader = FileReader_from_file(file);
    return 0;
}

FileReader FileReader_from_file(FILE* file) {
    FileReader file_reader = { file, DequeString_new() };
    return file_reader;
}

FileReader FileReader_from_string(const char* file_contents) {
    FileReader file_reader = { 0, DequeString_copy(file_contents) };
    return file_reader;
}

void FileReader_close(FileReader* file_reader) {
    if (file_reader->file) {
        fclose(file_reader->file);
    }
    DequeString_destroy(&file_reader->buffer);
}

char FileReader_read(FileReader* file_reader) {
    int ch = DequeString_pop_front(&file_reader->buffer);
    if (ch == 0) {
        if (file_reader->file) {
            ch = getc(file_reader->file);
            if (ch == EOF) {
                ch = 0;
            }
        }
    }
    return ch;
}

char FileReader_peek(FileReader* file_reader, size_t offset) {
    while (file_reader->buffer.deque.length < offset) {
        int ch = getc(file_reader->file);
        if (ch == EOF) {
            return 0;
        }
        DequeString_push_back(&file_reader->buffer, ch);
    }
    return DequeString_at(&file_reader->buffer, offset);
}

void FileReader_push_front(FileReader* file_reader, char ch) {
    DequeString_push_front(&file_reader->buffer, ch);
}
