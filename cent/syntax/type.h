#pragma once

typedef struct Cent_Type Cent_Type;

struct Cent_BaseType {
    char* name;
};
typedef struct Cent_BaseType Cent_BaseType;

struct Cent_PointerType {
    Cent_Type* inner;
};
typedef struct Cent_PointerType Cent_PointerType;

enum Cent_TypeType { Cent_Type_Base, Cent_Type_Pointer };
typedef enum Cent_TypeType Cent_TypeType;

union Cent_TypeValue {
    Cent_BaseType base;
    Cent_PointerType pointer;
};
typedef union Cent_TypeValue Cent_TypeValue;

struct Cent_Type {
    Cent_TypeType type;
    Cent_TypeValue value;
};

void Cent_Type_destroy(Cent_Type* type);
