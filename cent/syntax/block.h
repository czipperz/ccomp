#pragma once

#include "declaration.h"
#include "statement.h"
#include <stddef.h>

struct Cent_Block {
    size_t num_declarations;
    Cent_Declaration* declarations;
    size_t num_statements;
    Cent_Statement* statements;
};
typedef struct Cent_Block Cent_Block;

void Cent_Block_destroy(Cent_Block*);
