#pragma once

#include <stdint.h>

enum Cent_ConstantType { Cent_Constant_Integer, Cent_Constant_String };
typedef enum Cent_ConstantType Cent_ConstantType;

union Cent_ConstantValue {
    uintmax_t integer;
    char* string;
};
typedef union Cent_ConstantValue Cent_ConstantValue;

struct Cent_Constant {
    Cent_ConstantType type;
    Cent_ConstantValue value;
};
typedef struct Cent_Constant Cent_Constant;

void Cent_Constant_destroy(Cent_Constant* constant);
