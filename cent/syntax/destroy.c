#include "panic.h"
#include "parse.h"

void Cent_Type_destroy(Cent_Type* type) {
    switch (type->type) {
    case Cent_Type_Base:
        free(type->value.base.name);
        break;
    case Cent_Type_Pointer:
        Cent_Type_destroy(type->value.pointer.inner);
        free(type->value.pointer.inner);
        break;
    }
}

void Cent_Constant_destroy(Cent_Constant* constant) {
    switch (constant->type) {
    case Cent_Constant_Integer:
        return;
    case Cent_Constant_String:
        free(constant->value.string);
        return;
    }
    unreachable();
}

void Cent_FunctionCallExpression_destroy(
    Cent_FunctionCallExpression* function_call) {
    size_t i;
    Cent_Expression_destroy(function_call->function);
    free(function_call->function);
    for (i = 0; i < function_call->num_arguments; ++i) {
        Cent_Expression_destroy(&function_call->arguments[i]);
    }
    free(function_call->arguments);
}

void Cent_DereferenceExpression_destroy(
    Cent_DereferenceExpression* dereference) {
    Cent_Expression_destroy(dereference->inner);
    free(dereference->inner);
}

void Cent_BinaryExpression_destroy(Cent_BinaryExpression* binary) {
    Cent_Expression_destroy(binary->left);
    free(binary->left);
    Cent_Expression_destroy(binary->right);
    free(binary->right);
}

void Cent_Expression_destroy(Cent_Expression* expression) {
    switch (expression->type) {
    case Cent_Expression_Constant:
        Cent_Constant_destroy(&expression->value.constant);
        break;
    case Cent_Expression_Variable:
        free(expression->value.variable);
        break;
    case Cent_Expression_FunctionCall:
        Cent_FunctionCallExpression_destroy(&expression->value.function_call);
        break;
    case Cent_Expression_Dereference:
        Cent_DereferenceExpression_destroy(&expression->value.dereference);
        break;
    case Cent_Expression_Binary:
        Cent_BinaryExpression_destroy(&expression->value.binary);
        break;
    }
}

void Cent_Parameter_destroy(Cent_Parameter* parameter) {
    Cent_Type_destroy(&parameter->type);
    free(parameter->name);
}

void Cent_FunctionDeclaration_destroy(
    Cent_FunctionDeclaration* function_declaration) {
    size_t i;
    Cent_Type_destroy(&function_declaration->return_type);
    free(function_declaration->name);
    for (i = 0; i < function_declaration->num_parameters; ++i) {
        Cent_Parameter_destroy(&function_declaration->parameters[i]);
    }
    free(function_declaration->parameters);
}

void Cent_VariableDeclaration_destroy(
    Cent_VariableDeclaration* variable_declaration) {
    Cent_Type_destroy(&variable_declaration->type);
    free(variable_declaration->name);
    if (variable_declaration->value) {
        Cent_Expression_destroy(variable_declaration->value);
        free(variable_declaration->value);
    }
}

void Cent_Declaration_destroy(Cent_Declaration* declaration) {
    switch (declaration->type) {
    case Cent_Declaration_Function:
        Cent_FunctionDeclaration_destroy(&declaration->value.function);
        break;
    case Cent_Declaration_Variable:
        Cent_VariableDeclaration_destroy(&declaration->value.variable);
        break;
    }
}

void Cent_Statement_destroy(Cent_Statement* statement) {
    switch (statement->type) {
    case Cent_Statement_Expression:
        Cent_Expression_destroy(&statement->value.expression);
        break;
    case Cent_Statement_Block:
        Cent_Block_destroy(statement->value.block);
        free(statement->value.block);
        break;
    case Cent_Statement_Return:
        if (statement->value.returns.is_valid) {
            Cent_Expression_destroy(&statement->value.returns.expression);
        }
        break;
    case Cent_Statement_While:
        Cent_Expression_destroy(&statement->value.whiles.condition);
        Cent_Statement_destroy(statement->value.whiles.body);
        free(statement->value.whiles.body);
        break;
    case Cent_Statement_If:
        Cent_Expression_destroy(&statement->value.ifs.condition);
        Cent_Statement_destroy(statement->value.ifs.trues);
        free(statement->value.ifs.trues);
        if (statement->value.ifs.falses) {
            Cent_Statement_destroy(statement->value.ifs.falses);
            free(statement->value.ifs.falses);
        }
        break;
    case Cent_Statement_Continue:
    case Cent_Statement_Break:
    case Cent_Statement_Empty:
        break;
    }
}

void Cent_Block_destroy(Cent_Block* block) {
    size_t i;
    for (i = 0; i < block->num_declarations; ++i) {
        Cent_Declaration_destroy(&block->declarations[i]);
    }
    free(block->declarations);

    for (i = 0; i < block->num_statements; ++i) {
        Cent_Statement_destroy(&block->statements[i]);
    }
    free(block->statements);
}

void Cent_FunctionDefinition_destroy(
    Cent_FunctionDefinition* function_definition) {
    Cent_FunctionDeclaration_destroy(&function_definition->declaration);
    Cent_Block_destroy(&function_definition->body);
}

void Cent_TopLevel_destroy(Cent_TopLevel* top_level) {
    switch (top_level->type) {
    case Cent_TopLevel_Declaration:
        Cent_Declaration_destroy(&top_level->value.declaration);
        break;
    case Cent_TopLevel_FunctionDefinition:
        Cent_FunctionDefinition_destroy(&top_level->value.function_definition);
        break;
    }
}
