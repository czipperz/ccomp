#pragma once

#include "constant.h"
#include <stddef.h>

typedef struct Cent_Expression Cent_Expression;

struct Cent_FunctionCallExpression {
    Cent_Expression* function;
    size_t num_arguments;
    Cent_Expression* arguments;
};
typedef struct Cent_FunctionCallExpression Cent_FunctionCallExpression;

struct Cent_DereferenceExpression {
    Cent_Expression* inner;
};
typedef struct Cent_DereferenceExpression Cent_DereferenceExpression;

enum Cent_Operator { Cent_Operator_Assign };
typedef enum Cent_Operator Cent_Operator;

struct Cent_BinaryExpression {
    Cent_Expression* left;
    Cent_Operator operator;
    Cent_Expression* right;
};
typedef struct Cent_BinaryExpression Cent_BinaryExpression;

enum Cent_ExpressionType {
    Cent_Expression_Constant,
    Cent_Expression_Variable,
    Cent_Expression_FunctionCall,
    Cent_Expression_Dereference,
    Cent_Expression_Binary
};
typedef enum Cent_ExpressionType Cent_ExpressionType;

union Cent_ExpressionValue {
    Cent_Constant constant;
    char* variable;
    Cent_FunctionCallExpression function_call;
    Cent_DereferenceExpression dereference;
    Cent_BinaryExpression binary;
};
typedef union Cent_ExpressionValue Cent_ExpressionValue;

struct Cent_Expression {
    Cent_ExpressionType type;
    Cent_ExpressionValue value;
};

void Cent_FunctionCallExpression_destroy(Cent_FunctionCallExpression*);
void Cent_DereferenceExpression_destroy(
    Cent_DereferenceExpression* dereference);
void Cent_Expression_destroy(Cent_Expression*);
