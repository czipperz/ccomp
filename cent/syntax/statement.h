#pragma once

#include "bool.h"
#include "expression.h"

typedef struct Cent_Statement Cent_Statement;

struct Cent_ReturnStatement {
    bool is_valid;
    Cent_Expression expression;
};
typedef struct Cent_ReturnStatement Cent_ReturnStatement;

struct Cent_WhileStatement {
    Cent_Expression condition;
    Cent_Statement* body;
};
typedef struct Cent_WhileStatement Cent_WhileStatement;

struct Cent_IfStatement {
    Cent_Expression condition;
    Cent_Statement* trues;
    Cent_Statement* falses;
};
typedef struct Cent_IfStatement Cent_IfStatement;

enum Cent_StatementType {
    Cent_Statement_Expression,
    Cent_Statement_Block,
    Cent_Statement_Return,
    Cent_Statement_Continue,
    Cent_Statement_Break,
    Cent_Statement_While,
    Cent_Statement_If,
    Cent_Statement_Empty
};
typedef enum Cent_StatementType Cent_StatementType;

struct Cent_Block;

union Cent_StatementValue {
    Cent_Expression expression;
    struct Cent_Block* block;
    Cent_ReturnStatement returns;
    Cent_WhileStatement whiles;
    Cent_IfStatement ifs;
};
typedef union Cent_StatementValue Cent_StatementValue;

struct Cent_Statement {
    Cent_StatementType type;
    Cent_StatementValue value;
};

void Cent_Statement_destroy(Cent_Statement*);
