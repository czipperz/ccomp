#pragma once

#include "block.h"
#include "declaration.h"

struct Cent_FunctionDefinition {
    Cent_FunctionDeclaration declaration;
    Cent_Block body;
};
typedef struct Cent_FunctionDefinition Cent_FunctionDefinition;

enum Cent_TopLevelType {
    Cent_TopLevel_Declaration,
    Cent_TopLevel_FunctionDefinition
};
typedef enum Cent_TopLevelType Cent_TopLevelType;

union Cent_TopLevelValue {
    Cent_Declaration declaration;
    Cent_FunctionDefinition function_definition;
};
typedef union Cent_TopLevelValue Cent_TopLevelValue;

struct Cent_TopLevel {
    Cent_TopLevelType type;
    Cent_TopLevelValue value;
};
typedef struct Cent_TopLevel Cent_TopLevel;

void Cent_FunctionDefinition_destroy(Cent_FunctionDefinition*);
void Cent_Declaration_destroy(Cent_Declaration*);
void Cent_TopLevel_destroy(Cent_TopLevel*);
