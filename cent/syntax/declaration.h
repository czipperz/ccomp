#pragma once

#include "expression.h"
#include "type.h"
#include <stddef.h>

struct Cent_Parameter {
    Cent_Type type;
    char* name;
};
typedef struct Cent_Parameter Cent_Parameter;

struct Cent_FunctionDeclaration {
    Cent_Type return_type;
    char* name;
    size_t num_parameters;
    Cent_Parameter* parameters;
};
typedef struct Cent_FunctionDeclaration Cent_FunctionDeclaration;

struct Cent_VariableDeclaration {
    Cent_Type type;
    char* name;
    Cent_Expression* value;
};
typedef struct Cent_VariableDeclaration Cent_VariableDeclaration;

enum Cent_DeclarationType {
    Cent_Declaration_Function,
    Cent_Declaration_Variable
};
typedef enum Cent_DeclarationType Cent_DeclarationType;

union Cent_DeclarationValue {
    Cent_FunctionDeclaration function;
    Cent_VariableDeclaration variable;
};
typedef union Cent_DeclarationValue Cent_DeclarationValue;

struct Cent_Declaration {
    Cent_DeclarationType type;
    Cent_DeclarationValue value;
};
typedef struct Cent_Declaration Cent_Declaration;

void Cent_Parameter_destroy(Cent_Parameter*);
void Cent_FunctionDeclaration_destroy(Cent_FunctionDeclaration*);
void Cent_VariableDeclaration_destroy(Cent_VariableDeclaration*);
