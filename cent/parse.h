#pragma once

#include "syntax.h"
#include "tokenizer.h"

struct Cent_Parser {
    Cent_Tokenizer tokenizer;
};
typedef struct Cent_Parser Cent_Parser;

/**
 * Initialize the Parser with a given Tokenizer.
 *
 * As of right now, this will retain a reference the Tokenizer and will destroy
 * it on `Parser_destroy`.
 */
Cent_Parser Cent_Parser_new(Cent_Tokenizer);

int Cent_Parser_open_file(Cent_Parser*, const char* file_name);
Cent_Parser Cent_Parser_from_file(FILE* file);
Cent_Parser Cent_Parser_from_string(const char* file_contents);

/**
 * Destroy the Parser
 */
void Cent_Parser_destroy(Cent_Parser*);

/**
 * Parse the next type.
 *
 * Return -1 if an error occured.
 * Return 0 if no type was retrieved.
 * Return 1 if a type was retrieved.
 */
int Cent_Parser_parse_type(Cent_Parser*, Cent_Type*);

/**
 * Parse the next parameter.
 *
 * Return -1 if an error occured.
 * Return 0 if no parameter was retrieved.
 * Return 1 if a parameter was retrieved.
 */
int Cent_Parser_parse_parameter(Cent_Parser*, Cent_Parameter*);

/**
 * Parse the next expression.
 *
 * Return -1 if an error occured.
 * Return 0 if no expression was retrieved.
 * Return 1 if a expression was retrieved.
 */
int Cent_Parser_parse_expression(Cent_Parser*, Cent_Expression*);

/**
 * Parse the next statement or declaration.
 *
 * Return -1 if an error occured.
 * Return 0 if no statement or declaration was retrieved.
 * Return 1 if a statement was retrieved.
 * Return 2 if a declaration was retrieved.
 */
int Cent_Parser_parse_statement_or_declaration(Cent_Parser*, Cent_Statement*,
                                               Cent_Declaration*);

/**
 * Parse the next statement.
 *
 * Return -1 if an error occured.
 * Return 0 if no statement was retrieved.
 * Return 1 if a statement was retrieved.
 */
int Cent_Parser_parse_statement(Cent_Parser*, Cent_Statement*);

/**
 * Parse the next declaration.
 *
 * Return -1 if an error occured.
 * Return 0 if no declaration was retrieved.
 * Return 1 if a declaration was retrieved.
 */
int Cent_Parser_parse_declaration(Cent_Parser*, Cent_Declaration*);

/**
 * Parse the next code block.
 *
 * Return -1 if an error occured.
 * Return 0 if no block was retrieved.
 * Return 1 if a block was retrieved.
 */
int Cent_Parser_parse_code_block(Cent_Parser*, Cent_Block*);

/**
 * Get the next top level object.
 *
 * Return -1 if an error occured.
 * Return 0 if no top level object was retrieved.
 * Return 1 if a top level object was retrieved.
 */
int Cent_Parser_next_top_level(Cent_Parser*, Cent_TopLevel*);
