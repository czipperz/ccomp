#include "serialize.h"
#include "visit.h"
#include <inttypes.h>
#include <stddef.h>

struct Serializer {
    Cent_TopLevelVisitor visitor;
    DequeString* string;
    size_t indent_level;
};
typedef struct Serializer Serializer;

static void Serializer_serialize_new_line(Serializer* serializer) {
    size_t i;
    DequeString_push_back_str(serializer->string, "\n");
    for (i = 0; i < serializer->indent_level; ++i) {
        DequeString_push_back_str(serializer->string, "    ");
    }
}

static void Serializer_serialize_variable_name(Serializer* serializer,
                                               char* name) {
    DequeString_push_back_str(serializer->string, name);
}

static void Serializer_serialize_base_type(Serializer* serializer,
                                           Cent_BaseType* base_type) {
    DequeString_push_back_str(serializer->string, base_type->name);
}

static void Serializer_serialize_pointer_type(Serializer* serializer,
                                              Cent_PointerType* pointer) {
    Cent_TopLevelVisitor_visit_pointer_type(serializer, pointer);
    DequeString_push_back_str(serializer->string, "*");
}

static void Serializer_serialize_parameter(Serializer* serializer,
                                           Cent_Parameter* parameter) {
    Cent_TopLevelVisitor_accept_type(serializer, &parameter->type);
    if (parameter->name) {
        DequeString_push_back_str(serializer->string, " ");
        Cent_TopLevelVisitor_accept_variable_name(serializer, parameter->name);
    }
}

static void Serializer_serialize_integer_constant_expression(
    Serializer* serializer, uintmax_t value) {
    char buffer[32];
    sprintf(buffer, "%" PRIuMAX, value);
    DequeString_push_back_str(serializer->string, buffer);
}
static void Serializer_serialize_string_constant_expression(
    Serializer* serializer, char* value) {
    DequeString_push_back(serializer->string, '"');
    DequeString_push_back_str(serializer->string, value);
    DequeString_push_back(serializer->string, '"');
}
static void Serializer_serialize_function_call_expression(
    Serializer* serializer, Cent_FunctionCallExpression* expression) {
    size_t i;
    Cent_TopLevelVisitor_accept_expression(serializer, expression->function);
    DequeString_push_back_str(serializer->string, "(");
    for (i = 0; i < expression->num_arguments; ++i) {
        if (i != 0) {
            DequeString_push_back_str(serializer->string, ", ");
        }
        Cent_TopLevelVisitor_accept_argument(serializer,
                                             &expression->arguments[i]);
    }
    DequeString_push_back_str(serializer->string, ")");
}
static void Serializer_serialize_dereference_expression(
    Serializer* serializer, Cent_DereferenceExpression* expression) {
    DequeString_push_back_str(serializer->string, "*");
    Cent_TopLevelVisitor_visit_dereference_expression(serializer, expression);
}
static void Serializer_serialize_binary_operator(Serializer* serializer,
                                                 Cent_Operator operator) {
    switch (operator) {
    case Cent_Operator_Assign:
        DequeString_push_back_str(serializer->string, " = ");
        break;
    }
}

static void Serializer_serialize_function_declaration(
    Serializer* serializer, Cent_FunctionDeclaration* function_declaration) {
    size_t i;
    Cent_TopLevelVisitor_accept_type(serializer,
                                     &function_declaration->return_type);
    DequeString_push_back_str(serializer->string, " ");
    Cent_TopLevelVisitor_accept_variable_name(serializer,
                                              function_declaration->name);
    DequeString_push_back_str(serializer->string, "(");
    for (i = 0; i < function_declaration->num_parameters; ++i) {
        if (i != 0) {
            DequeString_push_back_str(serializer->string, ", ");
        }
        Cent_TopLevelVisitor_accept_parameter(
            serializer, &function_declaration->parameters[i]);
    }
    DequeString_push_back_str(serializer->string, ")");
}
static void Serializer_serialize_variable_declaration(
    Serializer* serializer, Cent_VariableDeclaration* variable_declaration) {
    Cent_TopLevelVisitor_accept_type(serializer, &variable_declaration->type);
    DequeString_push_back_str(serializer->string, " ");
    Cent_TopLevelVisitor_accept_variable_name(serializer,
                                              variable_declaration->name);
    if (variable_declaration->value) {
        DequeString_push_back_str(serializer->string, " = ");
        Cent_TopLevelVisitor_accept_expression(serializer,
                                               variable_declaration->value);
    }
}
static void Serializer_serialize_declaration(Serializer* serializer,
                                             Cent_Declaration* declaration) {
    Cent_TopLevelVisitor_visit_declaration(serializer, declaration);
    DequeString_push_back_str(serializer->string, ";");
}
static void Serializer_serialize_block_declaration(
    Serializer* serializer, Cent_Declaration* declaration) {
    Serializer_serialize_new_line(serializer);
    Cent_TopLevelVisitor_visit_block_declaration(serializer, declaration);
}

static void Serializer_serialize_expression_statement(
    Serializer* serializer, Cent_Expression* expression) {
    Cent_TopLevelVisitor_visit_expression_statement(serializer, expression);
    DequeString_push_back_str(serializer->string, ";");
}
static void Serializer_serialize_return_statement(
    Serializer* serializer, Cent_ReturnStatement* statement) {
    DequeString_push_back_str(serializer->string, "return");
    if (statement->is_valid) {
        DequeString_push_back_str(serializer->string, " ");
        Cent_TopLevelVisitor_accept_expression(serializer,
                                               &statement->expression);
    }
    DequeString_push_back_str(serializer->string, ";");
}
static void Serializer_serialize_continue_statement(Serializer* serializer) {
    DequeString_push_back_str(serializer->string, "continue;");
}
static void Serializer_serialize_break_statement(Serializer* serializer) {
    DequeString_push_back_str(serializer->string, "break;");
}
static void Serializer_serialize_while_statement(
    Serializer* serializer, Cent_WhileStatement* statement) {
    DequeString_push_back_str(serializer->string, "while (");
    Cent_TopLevelVisitor_accept_expression(serializer, &statement->condition);
    DequeString_push_back_str(serializer->string, ") ");
    Cent_TopLevelVisitor_accept_statement(serializer, statement->body);
}
static void Serializer_serialize_if_statement(Serializer* serializer,
                                              Cent_IfStatement* statement) {
    DequeString_push_back_str(serializer->string, "if (");
    Cent_TopLevelVisitor_accept_expression(serializer, &statement->condition);
    DequeString_push_back_str(serializer->string, ")");
    Cent_TopLevelVisitor_accept_statement(serializer, statement->trues);
    if (statement->falses) {
        Serializer_serialize_new_line(serializer);
        DequeString_push_back_str(serializer->string, "else");
        Cent_TopLevelVisitor_accept_statement(serializer, statement->falses);
    }
}
static void Serializer_serialize_empty_statement(Serializer* serializer) {
    DequeString_push_back_str(serializer->string, ";");
}
static void Serializer_serialize_statement(Serializer* visitor,
                                           Cent_Statement* statement) {
    Serializer_serialize_new_line(visitor);
    Cent_TopLevelVisitor_visit_statement(visitor, statement);
}

static void Serializer_serialize_block(Serializer* serializer,
                                       Cent_Block* block) {
    size_t i;
    DequeString_push_back_str(serializer->string, "{");
    ++serializer->indent_level;
    for (i = 0; i < block->num_declarations; ++i) {
        Cent_TopLevelVisitor_accept_block_declaration(serializer,
                                                      &block->declarations[i]);
    }
    for (i = 0; i < block->num_statements; ++i) {
        Cent_TopLevelVisitor_accept_statement(serializer,
                                              &block->statements[i]);
    }
    --serializer->indent_level;
    Serializer_serialize_new_line(serializer);
    DequeString_push_back_str(serializer->string, "}");
}
static void Serializer_serialize_function_definition(
    Serializer* serializer, Cent_FunctionDefinition* function_definition) {
    Cent_TopLevelVisitor_accept_function_declaration(
        serializer, &function_definition->declaration);
    Serializer_serialize_new_line(serializer);
    Cent_TopLevelVisitor_accept_top_level_block(serializer,
                                                &function_definition->body);
}

static Serializer Serializer_for(DequeString* string) {
    Serializer serializer = {
        {
            Serializer_serialize_variable_name,

            Serializer_serialize_base_type,
            Serializer_serialize_pointer_type,
            Cent_TopLevelVisitor_visit_type,

            Serializer_serialize_parameter,

            Serializer_serialize_integer_constant_expression,
            Serializer_serialize_string_constant_expression,
            Cent_TopLevelVisitor_visit_constant_expression,
            Cent_TopLevelVisitor_visit_variable_expression,
            Cent_TopLevelVisitor_visit_argument,
            Serializer_serialize_function_call_expression,
            Serializer_serialize_dereference_expression,
            Serializer_serialize_binary_operator,
            Cent_TopLevelVisitor_visit_binary_expression,
            Cent_TopLevelVisitor_visit_expression,
            Cent_TopLevelVisitor_visit_condition_expression,

            Serializer_serialize_function_declaration,
            Serializer_serialize_variable_declaration,
            Serializer_serialize_declaration,
            Serializer_serialize_block_declaration,
            Cent_TopLevelVisitor_visit_top_level_declaration,

            Serializer_serialize_expression_statement,
            Cent_TopLevelVisitor_visit_block_statement,
            Serializer_serialize_return_statement,
            Serializer_serialize_continue_statement,
            Serializer_serialize_break_statement,
            Serializer_serialize_while_statement,
            Serializer_serialize_if_statement,
            Serializer_serialize_empty_statement,
            Serializer_serialize_statement,

            Serializer_serialize_block,
            Cent_TopLevelVisitor_visit_top_level_block,
            Serializer_serialize_function_definition,
            Cent_TopLevelVisitor_visit_top_level,
        },
        string,
        0,
    };
    return serializer;
}

#define make_serialize(function, type)                                         \
    void Cent_serialize_append_##function(DequeString* string, const type x) { \
        Serializer serializer = Serializer_for(string);                        \
        Cent_TopLevelVisitor_accept_##function(&serializer.visitor, (type)x);  \
    }                                                                          \
    char* Cent_serialize_##function(const type x) {                            \
        DequeString string = DequeString_new();                                \
        Cent_serialize_append_##function(&string, x);                          \
        DequeString_push_back(&string, 0);                                     \
        return DequeString_unwrap(&string);                                    \
    }

#define make_serialize_empty(function)                                         \
    void Cent_serialize_append_##function(DequeString* string) {               \
        Serializer serializer = Serializer_for(string);                        \
        Cent_TopLevelVisitor_accept_##function(&serializer.visitor);           \
    }                                                                          \
    char* Cent_serialize_##function(void) {                                    \
        DequeString string = DequeString_new();                                \
        Cent_serialize_append_##function(&string);                             \
        DequeString_push_back(&string, 0);                                     \
        return DequeString_unwrap(&string);                                    \
    }

make_serialize(variable_name, char*);

make_serialize(base_type, Cent_BaseType*);
make_serialize(pointer_type, Cent_PointerType*);
make_serialize(type, Cent_Type*);

make_serialize(parameter, Cent_Parameter*);

make_serialize(integer_constant_expression, uintmax_t);
make_serialize(string_constant_expression, char*);
make_serialize(constant_expression, Cent_Constant*);
make_serialize(variable_expression, char*);
make_serialize(argument, Cent_Expression*);
make_serialize(function_call_expression, Cent_FunctionCallExpression*);
make_serialize(dereference_expression, Cent_DereferenceExpression*);
make_serialize(binary_operator, Cent_Operator);
make_serialize(binary_expression, Cent_BinaryExpression*);
make_serialize(expression, Cent_Expression*);
make_serialize(condition_expression, Cent_Expression*);

make_serialize(function_declaration, Cent_FunctionDeclaration*);
make_serialize(variable_declaration, Cent_VariableDeclaration*);
make_serialize(declaration, Cent_Declaration*);
make_serialize(block_declaration, Cent_Declaration*);
make_serialize(top_level_declaration, Cent_Declaration*);

make_serialize(expression_statement, Cent_Expression*);
make_serialize(block_statement, Cent_Block*);
make_serialize(return_statement, Cent_ReturnStatement*);
make_serialize_empty(continue_statement);
make_serialize_empty(break_statement);
make_serialize(while_statement, Cent_WhileStatement*);
make_serialize(if_statement, Cent_IfStatement*);
make_serialize_empty(empty_statement);
make_serialize(statement, Cent_Statement*);

make_serialize(block, Cent_Block*);
make_serialize(top_level_block, Cent_Block*);
make_serialize(function_definition, Cent_FunctionDefinition*);
make_serialize(top_level, Cent_TopLevel*);
