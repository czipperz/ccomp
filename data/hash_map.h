#pragma once

#include <stddef.h>

struct HashMap {
    char* buffer;
    int* alive;
    size_t elements;
    size_t capacity;
    size_t (*hash)(const void*);
    int (*equal)(const void*, const void*);
};
typedef struct HashMap HashMap;

/** Create a new hash map. */
HashMap HashMap_new(size_t (*hash_function)(const void*),
                    int (*equal)(const void*, const void*));

HashMap HashMap_of_string(void);
HashMap HashMap_of_size_t(void);
HashMap HashMap_of_int(void);
HashMap HashMap_of_pointer(void);

/**
 * Destroy a hash map.
 *
 * This will call the destructor on every key value pair then free the
 * buffer.  Pass in a NULL destructor if you don't want one to be
 * invoked.
 */
void HashMap_destroy(HashMap*, void (*destructor)(void* key, void* value),
                     size_t sizeof_key, size_t sizeof_value);

/**
 * Insert a key value pair into the hash map.
 *
 * Returns if the key already exists and won't override it.
 */
int HashMap_insert(HashMap*, const void* key, const void* value,
                   size_t sizeof_key, size_t sizeof_value);
/**
 * Set the value associated with a given key.
 *
 * This function will override the existing value.
 * If the key isn't in the hash map already, it is inserted.
 */
void HashMap_set(HashMap*, const void* key, const void* value,
                 size_t sizeof_key, size_t sizeof_value);

/** Remove a key and the associated value pair from the hash map. */
void HashMap_remove(HashMap*, const void* key, size_t sizeof_key,
                    size_t sizeof_value);

/** Get the value associated with the given key.  Returns NULL if the key
 * doesn't exist. */
void* HashMap_get(HashMap*, const void* key, size_t sizeof_key,
                  size_t sizeof_value);

void HashMap_accept(HashMap*,
                    void (*visitor)(void* user_data, void* key, void* value),
                    void* user_data, size_t sizeof_key, size_t sizeof_value);
