#pragma once

#include <stddef.h>

size_t hash_string(const char*);
size_t hash_size_t(size_t);
size_t hash_int(int);
size_t hash_pointer(const void*);
size_t hash_combine(const size_t* hashes, size_t length);
