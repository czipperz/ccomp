#pragma once

#include <stddef.h>

/**
 * An implementation of a double ended queue.
 *
 * Data can be inserted on either end and retrieved from either end.
 *
 * The pop routines do not edit the buffer, onlf the offset and length.
 */
struct Deque {
    /**
     * The memory buffer storing the elements in the `Deque`.
     */
    char* buffer;
    /**
     * The offset into the buffer, in elements.
     */
    size_t offset;
    /**
     * The number of elements stored in the buffer.
     */
    size_t length;
    /**
     * The number of elements the buffer could store.
     */
    size_t capacity;
};
typedef struct Deque Deque;

/** Create a new Deque in an empty state */
Deque Deque_new(void);

/** Create a new Deque as a copy of elems */
Deque Deque_copy(const void* elems, size_t length, size_t sizeof_data);

/**
 * Destroy each element in the deque then free the deque's buffer.
 *
 * If you have a Deque of pointers, then this will loop over pointers
 * to the pointers; the addresses the pointers reside at in the Deque.
 *
 * This will leave the deque with dangling pointers to the old data.
 * Use `Deque_new` to reset the state of the deque.
 */
void Deque_destroy(Deque*, void destroy(void*), size_t sizeof_data);

/**
 * Destroy each pointer in the deque then free the deque's buffer.
 *
 * If you have a Deque of pointers, then this will loop over the pointers.
 * Don't use this routine if your Deque is not over pointers.
 *
 * This will leave the deque with dangling pointers to the old data.
 * Use `Deque_init` to reset the state of the deque.
 */
void Deque_destroy_deref(Deque*, void destroy(void*), size_t sizeof_data);

/**
 * Set the length and offset of the deque to 0 but do not deallocate
 * any memory.
 */
void Deque_clear(Deque*);

/**
 * Get the buffer the deque is stored at.  If there is an offset, panic.
 */
void* Deque_unwrap(Deque*);

/** Reserve spots in the deque for size more elements */
void Deque_ensure_one_more(Deque* deque, size_t size);
void Deque_ensure_n_more(Deque* deque, size_t length, size_t size);

/** Push an element onto the front of the Deque */
void Deque_push_front(Deque*, const void*, size_t sizeof_data);
void Deque_push_front_n(Deque*, const void*, size_t length, size_t sizeof_data);

/** Push an element onto the back of the Deque */
void Deque_push_back(Deque*, const void*, size_t sizeof_data);
void Deque_push_back_n(Deque*, const void*, size_t length, size_t sizeof_data);

/** Attempt to pop an element off the front of the queue.  Return if it is was
 * successful */
int Deque_pop_front(Deque*, void*, size_t sizeof_data);

/** Attempt to pop an element off the back of the queue.  Return if it is was
 * successful */
int Deque_pop_back(Deque*, void*, size_t sizeof_data);

/** Remove the element at the index */
int Deque_remove(Deque*, size_t index, void*, size_t sizeof_data);

/**
 * Get the element at an index.
 *
 * # Panics
 *
 * This function will panic if index is greater than the length of the deque.
 */
void* Deque_at(const Deque*, size_t index, size_t sizeof_data);
