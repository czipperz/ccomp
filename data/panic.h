#pragma once

#include <stdio.h>
#include <stdlib.h>

#define panic(message)                                                         \
    do {                                                                       \
        fprintf(stderr, "%s:%d: %s\n", __FILE__, __LINE__, message);           \
        abort();                                                               \
    } while (0)

#define unimplemented(message) panic("Unimplemented code reached")

#define unreachable(message) panic("Unreachable code reached")

#define assert(condition)                                                      \
    do {                                                                       \
        if (!(condition)) {                                                    \
            panic(#condition);                                                 \
        }                                                                      \
    } while (0)
