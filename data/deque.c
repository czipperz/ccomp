#include "deque.h"
#include "panic.h"
#include <math.h>
#include <string.h>

Deque Deque_new(void) {
    Deque deque = { 0 };
    return deque;
}

Deque Deque_copy(const void* elems, size_t length, size_t sizeof_data) {
    Deque deque;
    deque.buffer = malloc(length * sizeof_data);
    assert(deque.buffer);
    memcpy(deque.buffer, elems, length * sizeof_data);
    deque.offset = 0;
    deque.length = length;
    deque.capacity = length;
    return deque;
}

void Deque_destroy(Deque* deque, void destroy(void*), size_t size) {
    size_t i;
    for (i = 0; i < deque->length; ++i) {
        destroy(Deque_at(deque, i, size));
    }
    free(deque->buffer);
}

void Deque_destroy_deref(Deque* deque, void destroy(void*), size_t size) {
    size_t i;
    for (i = 0; i < deque->length; ++i) {
        void** ptr = Deque_at(deque, i, size);
        destroy(*ptr);
    }
    free(deque->buffer);
}

void Deque_clear(Deque* deque) {
    deque->length = 0;
    deque->offset = 0;
}

void* Deque_unwrap(Deque* deque) {
    assert(deque->offset == 0);
    return deque->buffer;
}

void Deque_ensure_one_more(Deque* deque, size_t size) {
    if (deque->length == deque->capacity) {
        char* new_buffer;
        size_t new_capacity;
        if (deque->capacity == 0) {
            new_capacity = 16;
            new_buffer = malloc(new_capacity * size);
            assert(new_buffer);
        } else {
            new_capacity = deque->capacity * 2;
            new_buffer = malloc(new_capacity * size);
            assert(new_buffer);

            if (deque->offset + deque->length > deque->capacity) {
                size_t split = deque->capacity - deque->offset;
                memcpy(new_buffer, deque->buffer + deque->offset * size,
                       split * size);
                memcpy(new_buffer + split * size, deque->buffer,
                       (deque->length - split) * size);
            } else {
                memcpy(new_buffer, deque->buffer, deque->length * size);
            }

            free(deque->buffer);
            deque->offset = 0;
        }
        deque->buffer = new_buffer;
        deque->capacity = new_capacity;
    }
}

static size_t next_greatest_power_of_two(size_t x) {
    /* return pow(2, ceil(log(x)/log(2))); */
    size_t i;
    x--;
    for (i = 1; i < 8 * sizeof(x); i <<= 1) {
        x |= x >> 1;
    }
    x++;
    return x;
}

void Deque_ensure_n_more(Deque* deque, size_t length, size_t size) {
    size_t total_length = deque->length + length;
    if (total_length >= deque->capacity + 1) {
        size_t next_length = next_greatest_power_of_two(total_length);
        char* new_buffer;
        size_t new_capacity;
        if (deque->capacity == 0) {
            new_capacity = 16;
            if (new_capacity < next_length) {
                new_capacity = next_length;
            }
            new_buffer = malloc(new_capacity * size);
            assert(new_buffer);
        } else {
            new_capacity = deque->capacity * 2;
            if (new_capacity < next_length) {
                new_capacity = next_length;
            }
            new_buffer = malloc(new_capacity * size);
            assert(new_buffer);

            if (deque->offset + deque->length > deque->capacity) {
                size_t split = deque->capacity - deque->offset;
                memcpy(new_buffer, deque->buffer + deque->offset * size,
                       split * size);
                memcpy(new_buffer + split * size, deque->buffer,
                       (deque->length - split) * size);
            } else {
                memcpy(new_buffer, deque->buffer, deque->length * size);
            }

            free(deque->buffer);
            deque->offset = 0;
        }
        deque->buffer = new_buffer;
        deque->capacity = new_capacity;
    }
}

void Deque_push_back(Deque* deque, const void* elem, size_t size) {
    assert(deque->length <= deque->capacity);

    Deque_ensure_one_more(deque, size);

    memcpy(Deque_at(deque, deque->length, size), elem, size);
    ++deque->length;
}

void Deque_push_back_n(Deque* deque, const void* elem, size_t length,
                       size_t size) {
    assert(deque->length <= deque->capacity);

    Deque_ensure_n_more(deque, length, size);

    memcpy(Deque_at(deque, deque->length, size), elem, length * size);
    deque->length += length;
}

void Deque_push_front(Deque* deque, const void* elem, size_t size) {
    assert(deque->length <= deque->capacity);

    Deque_ensure_one_more(deque, size);

    if (deque->offset == 0) {
        deque->offset = deque->capacity;
    }
    --deque->offset;

    memcpy(Deque_at(deque, 0, size), elem, size);
    ++deque->length;
}

void Deque_push_front_n(Deque* deque, const void* elem, size_t length,
                        size_t size) {
    assert(deque->length <= deque->capacity);

    Deque_ensure_n_more(deque, length, size);

    if (deque->offset < length) {
        deque->offset = deque->capacity + deque->offset + 1;
    }
    deque->offset -= length;

    memcpy(Deque_at(deque, 0, size), elem, length * size);
    deque->length += length;
}

int Deque_pop_front(Deque* deque, void* elem, size_t size) {
    if (deque->length == 0) {
        return 0;
    } else {
        if (elem) {
            memcpy(elem, Deque_at(deque, 0, size), size);
        }
        ++deque->offset;
        if (deque->offset == deque->capacity) {
            deque->offset = 0;
        }
        --deque->length;
        return 1;
    }
}

int Deque_pop_back(Deque* deque, void* elem, size_t size) {
    if (deque->length == 0) {
        return 0;
    } else {
        --deque->length;
        if (elem) {
            memcpy(elem, Deque_at(deque, deque->length, size), size);
        }
        return 1;
    }
}

int Deque_remove(Deque* deque, size_t index, void* elem, size_t size) {
    if (deque->length == 0) {
        return 0;
    } else {
        assert(index < deque->length);
        if (elem) {
            memcpy(elem, Deque_at(deque, index, size), size);
        }
        if (deque->offset + deque->length > deque->capacity) {
            /* efgabcd delete b */
            /* index = 1, offset = 3 */
            /* 1. shift cd over => efgacd_ */
            /* 2. rotate e over => _fgabce */
            /* 3. shift fg over => fg_abce */
            {
                char* dest = Deque_at(deque, index, size);
                char* source = Deque_at(deque, index + 1, size);
                size_t num = deque->capacity - index - deque->offset - 1;
                memmove(dest, source, num * size);
            }
            {
                char* dest = deque->buffer + (deque->length - 1) * size;
                char* source = deque->buffer;
                size_t num = 1;
                memmove(dest, source, num * size);
            }
            {
                char* dest = deque->buffer;
                char* source = deque->buffer + size;
                size_t num = deque->capacity - index - deque->offset - 1;
                memmove(dest, source, num * size);
            }
        } else {
            /* ___abcd delet b */
            /* 1. shift cd over => ___acd_ */
            char* dest = Deque_at(deque, index, size);
            char* source = Deque_at(deque, index + 1, size);
            size_t num = deque->length - index - 1;
            memmove(dest, source, num * size);
        }
        --deque->length;
        return 1;
    }
}

void* Deque_at(const Deque* deque, size_t index, size_t size) {
    assert(index <= deque->length);
    return &deque->buffer[((deque->offset + index) % deque->capacity) * size];
}
