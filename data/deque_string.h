#pragma once

#include "deque.h"

/**
 * A double ended queue of characters.
 *
 * This is a facade of the [`Deque`] type, making it easiert to use with
 * strings.
 *
 * [`Deque`]: deque.h
 */
struct DequeString {
    Deque deque;
};
typedef struct DequeString DequeString;

DequeString DequeString_new(void);
DequeString DequeString_copy(const char* string);
DequeString DequeString_copy_n(const char* string, size_t length);
void DequeString_destroy(DequeString*);

void DequeString_clear(DequeString*);
char* DequeString_unwrap(DequeString*);

void DequeString_push_front(DequeString*, char);
void DequeString_push_front_str(DequeString*, const char*);
void DequeString_push_front_n(DequeString*, const char*, size_t length);

void DequeString_push_back(DequeString*, char);
void DequeString_push_back_str(DequeString*, const char*);
void DequeString_push_back_n(DequeString*, const char*, size_t length);

char DequeString_pop_front(DequeString*);
char DequeString_pop_back(DequeString*);

char DequeString_remove(DequeString*, size_t index);

char DequeString_at(const DequeString*, size_t index);
