#include "deque_string.h"
#include "panic.h"
#include <string.h>

DequeString DequeString_new() {
    DequeString deque_string = { Deque_new() };
    return deque_string;
}
DequeString DequeString_copy(const char* string) {
    return DequeString_copy_n(string, strlen(string));
}
DequeString DequeString_copy_n(const char* string, size_t length) {
    DequeString deque_string = { Deque_copy(string, length, 1) };
    return deque_string;
}
void DequeString_destroy(DequeString* deque_string) {
    free(deque_string->deque.buffer);
}

void DequeString_clear(DequeString* deque_string) {
    Deque_clear(&deque_string->deque);
}
char* DequeString_unwrap(DequeString* deque_string) {
    return Deque_unwrap(&deque_string->deque);
}

void DequeString_push_front(DequeString* deque_string, char ch) {
    Deque_push_front(&deque_string->deque, &ch, 1);
}
void DequeString_push_front_n(DequeString* deque_string, const char* string,
                              size_t length) {
    Deque_push_front_n(&deque_string->deque, string, length, 1);
}
void DequeString_push_front_str(DequeString* deque_string, const char* string) {
    return DequeString_push_front_n(deque_string, string, strlen(string));
}

void DequeString_push_back(DequeString* deque_string, char ch) {
    Deque_push_back(&deque_string->deque, &ch, 1);
}
void DequeString_push_back_n(DequeString* deque_string, const char* string,
                             size_t length) {
    Deque_push_back_n(&deque_string->deque, string, length, 1);
}
void DequeString_push_back_str(DequeString* deque_string, const char* string) {
    return DequeString_push_back_n(deque_string, string, strlen(string));
}

char DequeString_pop_front(DequeString* deque_string) {
    char ch = 0;
    Deque_pop_front(&deque_string->deque, &ch, 1);
    return ch;
}
char DequeString_pop_back(DequeString* deque_string) {
    char ch = 0;
    Deque_pop_back(&deque_string->deque, &ch, 1);
    return ch;
}

char DequeString_remove(DequeString* deque_string, size_t index) {
    char ch = 0;
    Deque_remove(&deque_string->deque, index, &ch, 1);
    return ch;
}

char DequeString_at(const DequeString* deque_string, size_t index) {
    return *(char*)Deque_at(&deque_string->deque, index, 1);
}
