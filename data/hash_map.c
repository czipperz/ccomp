#include "hash_map.h"
#include "hash.h"
#include "panic.h"
#include <stdlib.h>
#include <string.h>

#define MAX_LOAD_FACTOR 0.6

HashMap HashMap_new(size_t (*hash)(const void*),
                    int (*equal)(const void*, const void*)) {
    HashMap hash_map = { 0, 0, 0, 0, hash, equal };
    return hash_map;
}

static size_t string_hash(const void* i) {
    const char* const* s = i;
    return hash_string(*s);
}
static int string_equals(const void* i1, const void* i2) {
    const char* const* s1 = i1;
    const char* const* s2 = i2;
    return strcmp(*s1, *s2) == 0;
}
HashMap HashMap_of_string(void) {
    return HashMap_new(string_hash, string_equals);
}

static size_t size_t_hash(const void* i) {
    const size_t* s = i;
    return hash_size_t(*s);
}
static int size_t_equals(const void* i1, const void* i2) {
    const size_t* s1 = i1;
    const size_t* s2 = i2;
    return *s1 == *s2;
}
HashMap HashMap_of_size_t(void) {
    return HashMap_new(size_t_hash, size_t_equals);
}

static size_t int_hash(const void* i) {
    const int* s = i;
    return hash_int(*s);
}
static int int_equals(const void* i1, const void* i2) {
    const int* s1 = i1;
    const int* s2 = i2;
    return *s1 == *s2;
}
HashMap HashMap_of_int(void) { return HashMap_new(int_hash, int_equals); }

static size_t pointer_hash(const void* i) {
    const void* const* s = i;
    return hash_pointer(*s);
}
static int pointer_equals(const void* i1, const void* i2) {
    const void* const* s1 = i1;
    const void* const* s2 = i2;
    return *s1 == *s2;
}
HashMap HashMap_of_pointer(void) {
    return HashMap_new(pointer_hash, pointer_equals);
}

static void apply2(void (*f)(void*, void*), void* key, void* value) {
    f(key, value);
}
void HashMap_destroy(HashMap* hash_map,
                     void (*destructor)(void* key, void* value),
                     size_t sizeof_key, size_t sizeof_value) {
    if (destructor) {
        HashMap_accept(hash_map, apply2, destructor, sizeof_key, sizeof_value);
    }
    free(hash_map->buffer);
    free(hash_map->alive);
}

static int HashMap_is_alive(const HashMap* hash_map, size_t i) {
    size_t d = i / (sizeof(int) * 8);
    size_t m = i % (sizeof(int) * 8);
    return hash_map->alive[d] & (1 << m);
}

static void HashMap_set_alive(HashMap* hash_map, size_t i) {
    size_t d = i / (sizeof(int) * 8);
    size_t m = i % (sizeof(int) * 8);
    hash_map->alive[d] |= (1 << m);
}

static void HashMap_set_dead(HashMap* hash_map, size_t i) {
    size_t d = i / (sizeof(int) * 8);
    size_t m = i % (sizeof(int) * 8);
    hash_map->alive[d] &= ~(1 << m);
}

static int HashMap_keys_equals(HashMap* hash_map, size_t key_hash,
                               const void* key, const char* pair) {
    /* Comparing to a hash first is a potential optimization in that
     * only one element has to be revisited.  I'm not sure if this
     * provides an actual benefit.  I would need to benchmark it
     * first.
     * Pros: Differing elements are faster.
     * Cons: Equivalent elements are slower at the point of match.  This may
     *       not even be relevant because they likely will find differing
     *       elements they have to be compared to along the way.
     */
    return key_hash == hash_map->hash(pair) && hash_map->equal(key, pair);
}

static size_t HashMap_index_to_set(HashMap* hash_map, size_t key_hash,
                                   const void* key, size_t combined_size) {
    size_t initial_index = key_hash % hash_map->capacity;
    size_t index = initial_index;
    size_t jump = 0;
    while (HashMap_is_alive(hash_map, index)) {
        char* pair = &hash_map->buffer[index * combined_size];
        if (HashMap_keys_equals(hash_map, key_hash, key, pair)) {
            /* this index is alive */
            return index;
        }

        ++jump;
        index += jump * jump;
        index %= hash_map->capacity;
        if (index == initial_index) {
            /* no match, return capacity */
            return hash_map->capacity;
        }
    }
    /* this index is dead */
    return index;
}

static size_t HashMap_index_of(HashMap* hash_map, size_t key_hash,
                               const void* key, size_t combined_size) {
    size_t set_index = HashMap_index_to_set(hash_map, key_hash, key,
                                            combined_size);
    if (!HashMap_is_alive(hash_map, set_index)) {
        return hash_map->capacity;
    } else {
        return set_index;
    }
}

static void HashMap_insert_all(HashMap* destination, const HashMap* source,
                               size_t sizeof_key, size_t sizeof_value) {
    size_t i;
    for (i = 0; i < source->capacity; ++i) {
        if (HashMap_is_alive(source, i)) {
            char* key = &source->buffer[i * (sizeof_key + sizeof_value)];
            HashMap_insert(destination, key, key + sizeof_key, sizeof_key,
                           sizeof_value);
        }
    }
}

static void HashMap_resize(HashMap* hash_map, size_t sizeof_key,
                           size_t sizeof_value) {
    size_t new_capacity;
    HashMap new_hash_map;
    if (hash_map->capacity == 0) {
        new_capacity = 16;
    } else {
        new_capacity = hash_map->capacity * 2;
    }

    new_hash_map = HashMap_new(hash_map->hash, hash_map->equal);
    new_hash_map.buffer = malloc(new_capacity * (sizeof_key + sizeof_value));
    assert(new_hash_map.buffer);
    new_hash_map.alive = calloc((new_capacity - 1) / (8 * sizeof(int)) + 1,
                                sizeof(int));
    assert(new_hash_map.alive);
    new_hash_map.capacity = new_capacity;

    HashMap_insert_all(&new_hash_map, hash_map, sizeof_key, sizeof_value);

    free(hash_map->buffer);
    free(hash_map->alive);
    *hash_map = new_hash_map;
}

int HashMap_insert(HashMap* hash_map, const void* key, const void* value,
                   size_t sizeof_key, size_t sizeof_value) {
    size_t combined_size = sizeof_key + sizeof_value;
    size_t i;
    char* pair;
    if (/* hash_map->capacity == 0 || */
        hash_map->elements >= MAX_LOAD_FACTOR * hash_map->capacity) {
        HashMap_resize(hash_map, sizeof_key, sizeof_value);
    }
    i = HashMap_index_to_set(hash_map, hash_map->hash(key), key, combined_size);
    if (i == hash_map->capacity) {
        /* no spot to insert */
        unimplemented();
    }
    if (HashMap_is_alive(hash_map, i)) {
        return 1;
    }

    pair = &hash_map->buffer[i * combined_size];
    memcpy(pair, key, sizeof_key);
    memcpy(pair + sizeof_key, value, sizeof_value);

    HashMap_set_alive(hash_map, i);
    ++hash_map->elements;
    return 0;
}

void HashMap_set(HashMap* hash_map, const void* key, const void* value,
                 size_t sizeof_key, size_t sizeof_value) {
    size_t combined_size = sizeof_key + sizeof_value;
    size_t i;
    char* pair;
    if (/* hash_map->capacity == 0 || */
        hash_map->elements >= MAX_LOAD_FACTOR * hash_map->capacity) {
        HashMap_resize(hash_map, sizeof_key, sizeof_value);
    }
    i = HashMap_index_to_set(hash_map, hash_map->hash(key), key, combined_size);
    if (i == hash_map->capacity) {
        /* no spot to insert */
        unimplemented();
    }

    pair = &hash_map->buffer[i * combined_size];
    memcpy(pair, key, sizeof_key);
    memcpy(pair + sizeof_key, value, sizeof_value);

    if (!HashMap_is_alive(hash_map, i)) {
        HashMap_set_alive(hash_map, i);
        ++hash_map->elements;
    }
}

void HashMap_remove(HashMap* hash_map, const void* key, size_t sizeof_key,
                    size_t sizeof_value) {
    if (hash_map->capacity > 0) {
        size_t combined_size = sizeof_key + sizeof_value;
        size_t i = HashMap_index_of(hash_map, hash_map->hash(key), key,
                                    combined_size);
        if (i != hash_map->capacity) {
            HashMap_set_dead(hash_map, i);
        }
    }
}

void* HashMap_get(HashMap* hash_map, const void* key, size_t sizeof_key,
                  size_t sizeof_value) {
    if (hash_map->capacity > 0) {
        size_t combined_size = sizeof_key + sizeof_value;
        size_t i = HashMap_index_of(hash_map, hash_map->hash(key), key,
                                    combined_size);
        if (i == hash_map->capacity) {
            return 0;
        } else {
            return &hash_map->buffer[i * combined_size] + sizeof_key;
        }
    } else {
        return 0;
    }
}

void HashMap_accept(HashMap* hash_map,
                    void (*visitor)(void* user_data, void* key, void* value),
                    void* user_data, size_t sizeof_key, size_t sizeof_value) {
    size_t combined_size = sizeof_key + sizeof_value;
    size_t i;
    for (i = 0; i < hash_map->capacity; ++i) {
        if (HashMap_is_alive(hash_map, i)) {
            char* pair = &hash_map->buffer[i * combined_size];
            visitor(user_data, pair, pair + sizeof_key);
        }
    }
}
