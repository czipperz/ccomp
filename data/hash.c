#include "hash.h"

size_t hash_string(const char* s) {
    size_t hash = 0;
    size_t i;
    for (i = 0; s[i]; ++i) {
        hash *= 31;
        hash += s[i];
    }
    return hash;
}
size_t hash_size_t(size_t x) { return x; }
size_t hash_int(int x) { return x; }
size_t hash_pointer(const void* p) { return (size_t)p; }
size_t hash_combine(const size_t* hashes, size_t length) {
    size_t hash = 0;
    size_t i;
    for (i = 0; i < length; ++i) {
        hash *= 31;
        hash += hashes[i];
    }
    return hash;
}
