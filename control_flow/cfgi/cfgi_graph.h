#pragma once

#include "cfgi_edge.h"
#include "hash_map.h"
#include "syntax.h"
#include <stdio.h>

/**
 * `CFGI_Graph` represents the control flow between `Cent_Expression`s.
 *
 * This step eliminates `Cent_Statement`s and simplifies `Cent_Expression`s used
 * in condition places.
 */
struct CFGI_Graph {
    /**
     * A map of expressions to their successsors.
     *
     * This is a mapping of `CFGI_Node`s to a `Deque` of `CFGI_Edge`s.
     */
    HashMap successors;
};
typedef struct CFGI_Graph CFGI_Graph;

CFGI_Graph CFGI_Graph_new(void);
void CFGI_Graph_destroy(CFGI_Graph* graph);

void CFGI_Graph_add(CFGI_Graph* graph, Cent_TopLevel* top_level);

void CFGI_Graph_accept(CFGI_Graph* graph,
                       void (*visitor)(void*, CFGI_Node*, Deque*),
                       void* user_data);

int CFGI_Graph_has_string_edge(const CFGI_Graph* graph, const char* from,
                               CFGI_EdgeType edge_type, const char* to);
size_t CFGI_Graph_count_edges(const CFGI_Graph* graph);

void CFGI_Graph_serialize_append(const CFGI_Graph* graph, DequeString* string,
                                 int use_addresses);
char* CFGI_Graph_serialize(const CFGI_Graph* graph, int use_addresses);
void CFGI_Graph_print(const CFGI_Graph* graph, FILE* file, int use_addresses);
