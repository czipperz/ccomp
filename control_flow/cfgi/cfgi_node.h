#pragma once

#include "deque_string.h"
#include "syntax.h"

enum CFGI_NodeType {
    CFGI_Node_FunctionStart,
    CFGI_Node_FunctionEnd,
    CFGI_Node_Expression,
    CFGI_Node_Return,
};
typedef enum CFGI_NodeType CFGI_NodeType;

union CFGI_NodeValue {
    char* function;
    Cent_Expression* expression;
    Cent_ReturnStatement* returns;
};
typedef union CFGI_NodeValue CFGI_NodeValue;

/**
 * A node represents a `Cent_Expression`.
 *
 * The variants for functions allow for us to understand what functions are
 * defined.  The return statement variant (see `CFGI_NodeType`) is an escape
 * hatch to allow the next step (generating a `CFG_Graph`) to properly return
 * values instead of `void`.
 */
struct CFGI_Node {
    CFGI_NodeType type;
    CFGI_NodeValue value;
};
typedef struct CFGI_Node CFGI_Node;

CFGI_Node CFGI_Node_function(CFGI_NodeType, char*);
CFGI_Node CFGI_Node_expression(Cent_Expression*);
CFGI_Node CFGI_Node_return(Cent_ReturnStatement*);

size_t CFGI_Node_hash(const CFGI_Node*);
int CFGI_Node_equals(const CFGI_Node*, const CFGI_Node*);

void CFGI_Node_serialize_append(const CFGI_Node* node, DequeString* string,
                                int use_addresses);
