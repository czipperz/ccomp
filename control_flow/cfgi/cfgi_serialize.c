#include "cfgi_edge.h"
#include "cfgi_graph.h"
#include "cfgi_node.h"
#include "deque_string.h"
#include "serialize.h"
#include <stdio.h>
#include <stdlib.h>

struct SerializeVisitor {
    const CFGI_Graph* graph;
    DequeString* string;
    int use_addresses;
};
typedef struct SerializeVisitor SerializeVisitor;

static void serialize_node_use_addresses(DequeString* string,
                                         const CFGI_Node* node) {
    switch (node->type) {
        char buffer[32];
    case CFGI_Node_FunctionStart:
        sprintf(buffer, "%p", node->value.function);
        DequeString_push_back_str(string, buffer);
        DequeString_push_back_str(string, " <<start>>");
        break;
    case CFGI_Node_FunctionEnd:
        sprintf(buffer, "%p", node->value.function);
        DequeString_push_back_str(string, buffer);
        DequeString_push_back_str(string, " <<end>>");
        break;
    case CFGI_Node_Expression:
        sprintf(buffer, "%p", node->value.expression);
        DequeString_push_back_str(string, buffer);
        break;
    case CFGI_Node_Return:
        sprintf(buffer, "%p", node->value.returns);
        DequeString_push_back_str(string, buffer);
        break;
    }
}

static void serialize_node_no_addresses(DequeString* string,
                                        const CFGI_Node* node) {
    switch (node->type) {
    case CFGI_Node_FunctionStart:
        DequeString_push_back_str(string, node->value.function);
        DequeString_push_back_str(string, " <<start>>");
        break;
    case CFGI_Node_FunctionEnd:
        DequeString_push_back_str(string, node->value.function);
        DequeString_push_back_str(string, " <<end>>");
        break;
    case CFGI_Node_Expression:
        Cent_serialize_append_expression(string, node->value.expression);
        break;
    case CFGI_Node_Return:
        Cent_serialize_append_return_statement(string, node->value.returns);
        break;
    }
}

void CFGI_Node_serialize_append(const CFGI_Node* node, DequeString* string,
                                int use_addresses) {
    DequeString_push_back(string, '"');
    if (use_addresses) {
        serialize_node_use_addresses(string, node);
    } else {
        serialize_node_no_addresses(string, node);
    }
    DequeString_push_back(string, '"');
}

static void serialize_label(DequeString* string, const CFGI_Node* node) {
    CFGI_Node_serialize_append(node, string, 1);

    DequeString_push_back_str(string, " [label=\"");
    serialize_node_no_addresses(string, node);
    DequeString_push_back_str(string, "\"]\n");
}

static void CFGI_Graph_serialize_pair(SerializeVisitor* serializer,
                                      const CFGI_Node* node,
                                      const Deque* successors) {
    size_t i;
    if (serializer->use_addresses) {
        serialize_label(serializer->string, node);
    }
    for (i = 0; i < successors->length; ++i) {
        CFGI_Node_serialize_append(node, serializer->string,
                                   serializer->use_addresses);
        DequeString_push_back_str(serializer->string, " -> ");
        {
            CFGI_Edge* edge = Deque_at(successors, i, sizeof(CFGI_Edge));
            CFGI_Node_serialize_append(&edge->next, serializer->string,
                                       serializer->use_addresses);
            switch (edge->type) {
            case CFGI_Edge_Sequential:
                break;
            case CFGI_Edge_False:
                DequeString_push_back_str(serializer->string, " [color=red]");
                break;
            case CFGI_Edge_True:
                DequeString_push_back_str(serializer->string, " [color=green]");
                break;
            }
        }
        DequeString_push_back(serializer->string, '\n');
    }
}

void CFGI_Graph_serialize_append(const CFGI_Graph* graph, DequeString* string,
                                 int use_addresses) {
    SerializeVisitor serialize = { graph, string, use_addresses };
    DequeString_push_back_str(string, "digraph G {\n");
    CFGI_Graph_accept(graph, CFGI_Graph_serialize_pair, &serialize);
    DequeString_push_back_str(string, "}\n");
}
char* CFGI_Graph_serialize(const CFGI_Graph* graph, int use_addresses) {
    DequeString string = DequeString_new();
    CFGI_Graph_serialize_append(graph, &string, use_addresses);
    DequeString_push_back(&string, 0);
    return DequeString_unwrap(&string);
}
void CFGI_Graph_print(const CFGI_Graph* graph, FILE* file, int use_addresses) {
    char* str = CFGI_Graph_serialize(graph, use_addresses);
    fprintf(file, "%s", str);
    free(str);
}
