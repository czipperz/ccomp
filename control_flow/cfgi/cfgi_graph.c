#include "cfgi_graph.h"
#include "cfgi_edge.h"
#include "cfgi_node.h"
#include "panic.h"
#include "visit.h"
#include <string.h>

CFGI_Graph CFGI_Graph_new(void) {
    CFGI_Graph graph = { HashMap_new(CFGI_Node_hash, CFGI_Node_equals) };
    return graph;
}

static void destroy_node(CFGI_Node* key, Deque* value) { free(value->buffer); }
void CFGI_Graph_destroy(CFGI_Graph* graph) {
    HashMap_destroy(&graph->successors, destroy_node, sizeof(CFGI_Node),
                    sizeof(Deque));
}

struct CFGI_GraphVisitor {
    Cent_TopLevelVisitor visitor;
    CFGI_Graph* graph;
    Deque preceding_edges;
    Deque continuing_edges;
    Deque breaking_edges;
    Deque returning_edges;
};
typedef struct CFGI_GraphVisitor CFGI_GraphVisitor;

static Deque* CFGI_Graph_add_node(CFGI_Graph* graph, CFGI_Node* node) {
    Deque* successors = HashMap_get(&graph->successors, node, sizeof(CFGI_Node),
                                    sizeof(Deque));
    if (!successors) {
        Deque succ = Deque_new();
        HashMap_insert(&graph->successors, node, &succ, sizeof(CFGI_Node),
                       sizeof(Deque));
        successors = HashMap_get(&graph->successors, node, sizeof(CFGI_Node),
                                 sizeof(Deque));
    }
    return successors;
}

static void CFGI_Graph_add_edge(CFGI_Graph* graph, CFGI_Edge* preceding,
                                CFGI_Node next) {
    CFGI_Edge edge = { preceding->type, next };
    Deque* successors = CFGI_Graph_add_node(graph, &preceding->next);
    Deque_push_back(successors, &edge, sizeof(CFGI_Edge));
}

static void CFGI_Graph_add_edges(CFGI_GraphVisitor* visitor, Deque* edges,
                                 CFGI_Node next) {
    size_t i;
    for (i = 0; i < edges->length; ++i) {
        CFGI_Edge* preceding_edge = Deque_at(edges, i, sizeof(CFGI_Edge));
        CFGI_Graph_add_edge(visitor->graph, preceding_edge, next);
    }
}

static void CFGI_Graph_set_preceding_edges(CFGI_GraphVisitor* visitor,
                                           CFGI_Edge* edge) {
    Deque_clear(&visitor->preceding_edges);
    Deque_push_back(&visitor->preceding_edges, edge, sizeof(CFGI_Edge));
}

static void CFGI_Graph_visit_expression_statement(CFGI_GraphVisitor* visitor,
                                                  Cent_Expression* expression) {
    CFGI_Node node = CFGI_Node_expression(expression);
    CFGI_Edge edge = { CFGI_Edge_Sequential, node };
    CFGI_Graph_add_edges(visitor, &visitor->preceding_edges, node);
    CFGI_Graph_set_preceding_edges(visitor, &edge);
}

static void CFGI_Graph_set_edge_types(Deque* edges, CFGI_EdgeType edge_type) {
    size_t i;
    for (i = 0; i < edges->length; ++i) {
        CFGI_Edge* preceding_edge = Deque_at(edges, i, sizeof(CFGI_Edge));
        if (preceding_edge->type == CFGI_Edge_Sequential) {
            preceding_edge->type = edge_type;
        }
    }
}

static void CFGI_Graph_visit_if_statement(CFGI_GraphVisitor* visitor,
                                          Cent_IfStatement* statement) {
    Deque falses_incoming;
    Deque trues_outgoing;
    CFGI_Graph_visit_expression_statement(visitor, &statement->condition);

    assert(visitor->preceding_edges.offset == 0);
    falses_incoming = Deque_copy(visitor->preceding_edges.buffer,
                                 visitor->preceding_edges.length,
                                 sizeof(CFGI_Edge));
    CFGI_Graph_set_edge_types(&falses_incoming, CFGI_Edge_False);

    CFGI_Graph_set_edge_types(&visitor->preceding_edges, CFGI_Edge_True);
    Cent_TopLevelVisitor_accept_statement(visitor, statement->trues);
    trues_outgoing = visitor->preceding_edges;
    visitor->preceding_edges = falses_incoming;

    if (statement->falses) {
        Cent_TopLevelVisitor_accept_statement(visitor, statement->falses);
    }

    Deque_push_back_n(&visitor->preceding_edges, trues_outgoing.buffer,
                      trues_outgoing.length, sizeof(CFGI_Edge));

    free(trues_outgoing.buffer);
}

static void CFGI_Graph_visit_continue_statement(CFGI_GraphVisitor* visitor) {
    Deque_push_back_n(&visitor->continuing_edges,
                      Deque_unwrap(&visitor->preceding_edges),
                      visitor->preceding_edges.length, sizeof(CFGI_Edge));
    Deque_clear(&visitor->preceding_edges);
}

static void CFGI_Graph_visit_break_statement(CFGI_GraphVisitor* visitor) {
    Deque_push_back_n(&visitor->breaking_edges,
                      Deque_unwrap(&visitor->preceding_edges),
                      visitor->preceding_edges.length, sizeof(CFGI_Edge));
    Deque_clear(&visitor->preceding_edges);
}

static void CFGI_Graph_visit_while_statement(CFGI_GraphVisitor* visitor,
                                             Cent_WhileStatement* statement) {
    CFGI_Graph_visit_expression_statement(visitor, &statement->condition);

    CFGI_Graph_set_edge_types(&visitor->preceding_edges, CFGI_Edge_True);
    Cent_TopLevelVisitor_accept_statement(visitor, statement->body);

    Deque_push_back_n(&visitor->preceding_edges,
                      Deque_unwrap(&visitor->continuing_edges),
                      visitor->continuing_edges.length, sizeof(CFGI_Edge));
    CFGI_Graph_visit_expression_statement(visitor, &statement->condition);
    CFGI_Graph_set_edge_types(&visitor->preceding_edges, CFGI_Edge_False);

    Deque_push_back_n(&visitor->preceding_edges,
                      Deque_unwrap(&visitor->breaking_edges),
                      visitor->breaking_edges.length, sizeof(CFGI_Edge));
    Deque_clear(&visitor->breaking_edges);
    Deque_clear(&visitor->continuing_edges);
}

static void CFGI_Graph_visit_return_statement(CFGI_GraphVisitor* visitor,
                                              Cent_ReturnStatement* statement) {
    CFGI_Node node = CFGI_Node_return(statement);
    CFGI_Edge edge = { CFGI_Edge_Sequential, node };
    CFGI_Graph_add_edges(visitor, &visitor->preceding_edges, node);
    Deque_clear(&visitor->preceding_edges);
    Deque_push_back(&visitor->returning_edges, &edge, sizeof(CFGI_Edge));
}

static void CFGI_Graph_visit_function_definition(
    CFGI_GraphVisitor* visitor, Cent_FunctionDefinition* definition) {
    CFGI_Edge edge = { CFGI_Edge_Sequential,
                       CFGI_Node_function(CFGI_Node_FunctionStart,
                                          definition->declaration.name) };
    CFGI_Graph_set_preceding_edges(visitor, &edge);

    Cent_TopLevelVisitor_visit_function_definition(visitor, definition);

    {
        CFGI_Node end = CFGI_Node_function(CFGI_Node_FunctionEnd,
                                           definition->declaration.name);
        CFGI_Graph_add_edges(visitor, &visitor->preceding_edges, end);
        CFGI_Graph_add_edges(visitor, &visitor->returning_edges, end);
        CFGI_Graph_add_node(visitor->graph, &end);
    }

    Deque_clear(&visitor->preceding_edges);
    Deque_clear(&visitor->returning_edges);
}

void CFGI_Graph_add(CFGI_Graph* graph, Cent_TopLevel* top_level) {
    CFGI_GraphVisitor visitor = { Cent_TopLevelVisitor_new(),
                                  graph,
                                  Deque_new(),
                                  Deque_new(),
                                  Deque_new(),
                                  Deque_new() };
    visitor.visitor.visit_expression_statement
        = CFGI_Graph_visit_expression_statement;
    visitor.visitor.visit_if_statement = CFGI_Graph_visit_if_statement;
    visitor.visitor.visit_continue_statement
        = CFGI_Graph_visit_continue_statement;
    visitor.visitor.visit_break_statement = CFGI_Graph_visit_break_statement;
    visitor.visitor.visit_while_statement = CFGI_Graph_visit_while_statement;
    visitor.visitor.visit_return_statement = CFGI_Graph_visit_return_statement;
    visitor.visitor.visit_function_definition
        = CFGI_Graph_visit_function_definition;

    Cent_TopLevelVisitor_accept_top_level(&visitor, top_level);

    free(visitor.preceding_edges.buffer);
    free(visitor.returning_edges.buffer);
    free(visitor.continuing_edges.buffer);
    free(visitor.breaking_edges.buffer);
}

void CFGI_Graph_accept(CFGI_Graph* graph,
                       void (*visitor)(void*, CFGI_Node*, Deque*),
                       void* user_data) {
    HashMap_accept(&graph->successors, visitor, user_data, sizeof(CFGI_Node),
                   sizeof(Deque));
}

struct Edge {
    const char* from;
    CFGI_EdgeType type;
    const char* to;
    int equals;
};
typedef struct Edge Edge;

static void has_edge_visitor(Edge* edge, CFGI_Node* node, Deque* successors) {
    if (edge->equals) {
        return;
    }

    {
        int equals;
        DequeString node_serialized = DequeString_new();
        CFGI_Node_serialize_append(node, &node_serialized, 0);
        DequeString_push_back(&node_serialized, 0);
        equals = strcmp(DequeString_unwrap(&node_serialized), edge->from) == 0;
        DequeString_destroy(&node_serialized);
        if (!equals) {
            return;
        }
    }

    {
        size_t i;
        for (i = 0; i < successors->length; ++i) {
            CFGI_Edge* edge_to = Deque_at(successors, i, sizeof(CFGI_Edge));
            if (edge_to->type == edge->type) {
                int equals;
                DequeString node_serialized = DequeString_new();
                CFGI_Node_serialize_append(&edge_to->next, &node_serialized, 0);
                DequeString_push_back(&node_serialized, 0);
                equals = strcmp(DequeString_unwrap(&node_serialized), edge->to)
                    == 0;
                DequeString_destroy(&node_serialized);
                if (equals) {
                    edge->equals = 1;
                    return;
                }
            }
        }
    }
}
int CFGI_Graph_has_string_edge(const CFGI_Graph* graph, const char* from,
                               CFGI_EdgeType type, const char* to) {
    Edge edge = { from, type, to, 0 };
    CFGI_Graph_accept(graph, has_edge_visitor, &edge);
    return edge.equals;
}

static void count_edges_visitor(size_t* count, CFGI_Node* node,
                                Deque* successors) {
    *count += successors->length;
}
size_t CFGI_Graph_count_edges(const CFGI_Graph* graph) {
    size_t count = 0;
    CFGI_Graph_accept(graph, count_edges_visitor, &count);
    return count;
}
