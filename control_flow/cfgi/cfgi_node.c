#include "cfgi_node.h"
#include "hash.h"
#include "panic.h"
#include <string.h>

CFGI_Node CFGI_Node_function(CFGI_NodeType type, char* function) {
    CFGI_Node node;
    node.type = type;
    node.value.function = function;
    return node;
}

CFGI_Node CFGI_Node_expression(Cent_Expression* expression) {
    CFGI_Node node;
    node.type = CFGI_Node_Expression;
    node.value.expression = expression;
    return node;
}

CFGI_Node CFGI_Node_return(Cent_ReturnStatement* statement) {
    CFGI_Node node;
    node.type = CFGI_Node_Return;
    node.value.returns = statement;
    return node;
}

size_t CFGI_Node_hash(const CFGI_Node* node) {
    size_t hashes[2];
    hashes[0] = hash_size_t(node->type);
    switch (node->type) {
    case CFGI_Node_FunctionStart:
    case CFGI_Node_FunctionEnd:
        hashes[1] = hash_string(node->value.function);
        break;
    case CFGI_Node_Expression:
        hashes[1] = hash_pointer(node->value.expression);
        break;
    case CFGI_Node_Return:
        hashes[1] = hash_pointer(node->value.returns);
        break;
    }
    return hash_combine(hashes, 2);
}

int CFGI_Node_equals(const CFGI_Node* a, const CFGI_Node* b) {
    if (a->type != b->type) {
        return 0;
    }
    switch (a->type) {
    case CFGI_Node_FunctionStart:
    case CFGI_Node_FunctionEnd:
        return strcmp(a->value.function, b->value.function) == 0;
    case CFGI_Node_Expression:
        return a->value.expression == b->value.expression;
    case CFGI_Node_Return:
        return a->value.returns == b->value.returns;
    }
    unreachable();
}
