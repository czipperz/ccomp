#pragma once

#include "cfgi_node.h"

enum CFGI_EdgeType {
    CFGI_Edge_Sequential,
    CFGI_Edge_True,
    CFGI_Edge_False,
};
typedef enum CFGI_EdgeType CFGI_EdgeType;

struct CFGI_Edge {
    CFGI_EdgeType type;
    CFGI_Node next;
};
typedef struct CFGI_Edge CFGI_Edge;
