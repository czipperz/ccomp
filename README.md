# ccomp

ccomp is a C Compiler in C.  It is a side project of czipperz with the
goal of creating a C analysis framework.  One usage of this framework
is to create an executable.

## Building

1. Install `cmake` and a c compiler (`gcc` or `msvc` will work).
2. Run `make` to build `ccomp` in the `build/` directory.
3. Run `make check` to run the tests and ensure `ccomp` works correctly.

## Usage

General usage: `./build/ccomp INPUT_FILES... COMPILATION_MODE -o
OUTPUT_FILE`.

Help is accessible via `./build/ccomp --help`.

`COMPILATION_MODE` should be `-S` to generate assembly, `-c` to
generate an object file, or `-l` to generate an executable.

These don't have to be in the order written above, but `-o` must be
followed by the `OUTPUT_FILE`.
