BUILD_DIRECTORY=build
CMAKE_FLAGS=
MAKE_FLAGS=-k
VALGRIND_FLAGS=--leak-check=full

.PHONY: release debug tags check debug-check format clean

release:
	mkdir -p ${BUILD_DIRECTORY}
	cd ${BUILD_DIRECTORY} && cmake -DCMAKE_BUILD_TYPE=Release ${CMAKE_FLAGS} ..
	cd ${BUILD_DIRECTORY} && ${MAKE} ${MAKE_FLAGS}

debug:
	mkdir -p ${BUILD_DIRECTORY}
	cd ${BUILD_DIRECTORY} && cmake -DCMAKE_BUILD_TYPE=Debug ${CMAKE_FLAGS} ..
	cd ${BUILD_DIRECTORY} && ${MAKE} ${MAKE_FLAGS}

tags:
	cd ${BUILD_DIRECTORY} && ctags -eR ..

check: ${BUILD_DIRECTORY}/test
	./${BUILD_DIRECTORY}/test --fork-tests=true --quiet=true

debug-check: ${BUILD_DIRECTORY}/test
	valgrind ${VALGRIND_FLAGS} ./${BUILD_DIRECTORY}/test --fork-tests=false --quiet=false

format:
	clang-format -i `find -name "*.c" -or -name "*.h"`

clean:
	rm -Rf ${BUILD_DIRECTORY}
