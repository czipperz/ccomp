#include "transpose_if_else.h"
#include "panic.h"
#include "parse.h"
#include "visit.h"
#include <string.h>

static void transpose__if_statement(void* visitor,
                                    Cent_IfStatement* statement) {
    if (statement->falses) {
        Cent_Statement* temp = statement->falses;
        statement->falses = statement->trues;
        statement->trues = temp;
    }
    Cent_TopLevelVisitor_visit_if_statement(visitor, statement);
}

void transpose_if_else(Cent_TopLevel* top_level) {
    Cent_TopLevelVisitor visitor = Cent_TopLevelVisitor_new();
    visitor.visit_if_statement = transpose__if_statement;
    Cent_TopLevelVisitor_accept_top_level(&visitor, top_level);
}
