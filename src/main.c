#include "arguments.h"
#include "call_graph.h"
#include "cent.h"
#include "cfgi.h"
#include "keyword_analysis.h"
#include "panic.h"
#include "transpose_if_else.h"

int run_arguments(const Arguments*);

int run_main(int argc, char** argv) {
    int ret = 0;
    Arguments arguments;

    ret = arguments_parse(&arguments, argc - 1, argv + 1);
    if (ret) {
        goto cleanup;
    }

    ret = run_arguments(&arguments);
    if (ret) {
        goto cleanup;
    }

cleanup:
    arguments_destroy(&arguments);
    return ret;
}

static int Cent_Parser_debug_print(Cent_Parser* parser) {
    Deque top_levels = Deque_new();
    int total_result = 0;
    while (1) {
        Cent_TopLevel top_level;

        int res = Cent_Parser_next_top_level(parser, &top_level);
        if (res == -1) {
            fprintf(stderr, "Error: Aborting\n");
            Deque_destroy(&top_levels, Cent_TopLevel_destroy,
                          sizeof(Cent_TopLevel));
            return 1;
        } else if (res == 0) {
            break;
        }

        if (analyze_keywords(&top_level)) {
            fprintf(stderr, "Error: In analyzing keywords for:\n");
            total_result = 1;
        }

        Cent_print_top_level(stdout, &top_level);
        printf("\n");
        fflush(stdout);

        Deque_push_back(&top_levels, &top_level, sizeof(Cent_TopLevel));
    }

    {
        size_t i;
        CallGraph call_graph = CallGraph_new();
        for (i = 0; i < top_levels.length; ++i) {
            CallGraph_add(&call_graph,
                          Deque_at(&top_levels, i, sizeof(Cent_TopLevel)));
        }
        CallGraph_print(&call_graph, stdout);
        CallGraph_destroy(&call_graph);
    }

    {
        size_t i;
        CFGI_Graph call_graph = CFGI_Graph_new();
        for (i = 0; i < top_levels.length; ++i) {
            Cent_TopLevel* top_level = Deque_at(&top_levels, i,
                                                sizeof(Cent_TopLevel));
            if (top_level->type == Cent_TopLevel_FunctionDefinition) {
                CFGI_Graph_add(&call_graph, top_level);
            }
        }
        CFGI_Graph_print(&call_graph, stdout, 1);
        CFGI_Graph_destroy(&call_graph);
        printf("\n");
    }

    {
        size_t i;
        for (i = 0; i < top_levels.length; ++i) {
            Cent_TopLevel* top_level = Deque_at(&top_levels, i,
                                                sizeof(Cent_TopLevel));
            transpose_if_else(top_level);
            Cent_print_top_level(stdout, top_level);
            printf("\n");
        }
    }

    Deque_destroy(&top_levels, Cent_TopLevel_destroy, sizeof(Cent_TopLevel));
    return total_result;
}

int run_arguments(const Arguments* arguments) {
    if (arguments->compilation_mode >= GenerateAssembly) {
        /* generate assembly */
        Cent_Parser parser;
        if (arguments->num_input_files != 1) {
            unimplemented();
        }
        if (Cent_Parser_open_file(&parser, *arguments->input_files)) {
            fprintf(stderr, "Error: Failed to open %s",
                    *arguments->input_files);
            return 1;
        }
        if (Cent_Parser_debug_print(&parser)) {
            Cent_Parser_destroy(&parser);
            return 1;
        }
        Cent_Parser_destroy(&parser);
    }
    if (arguments->compilation_mode == GenerateAssembly) {
        /* output assembly */
    }
    if (arguments->compilation_mode >= GenerateObjectCode) {
        /* generate object code ( assembly ) */
    }
    if (arguments->compilation_mode == GenerateObjectCode) {
        /* output object code ( object code ) */
    }
    if (arguments->compilation_mode >= GenerateLinkedExecutable) {
        /* output linked executable */
    }
    return 0;
}
