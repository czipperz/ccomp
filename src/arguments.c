#include "arguments.h"
#include "deque.h"
#include "panic.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void arguments_init(Arguments* arguments) {
    arguments->compilation_mode = GenerateAssembly;
    arguments->num_input_files = 0;
    arguments->input_files = 0;
    arguments->output_file = 0;
}

static void help(void) {
    fprintf(stderr, "Usage:\n\
\n\
Flags:\n\
  -s        Generate assembly\n\
  -c        Generate object code\n\
  -l        Generate a linked executable\n\
  -o FILE   Set FILE as the output file\n\
\n\
Other arguments are treated as input files\n\
");
}

int arguments_parse(Arguments* arguments, int argc, char** argv) {
    Deque input_files = Deque_new();
    int i;
    arguments_init(arguments);

    for (i = 0; i < argc; ++i) {
        if (strcmp(argv[i], "--help") == 0) {
            help();
            return 1;
        } else if (strcmp(argv[i], "-S") == 0) {
            arguments->compilation_mode = GenerateAssembly;
        } else if (strcmp(argv[i], "-c") == 0) {
            arguments->compilation_mode = GenerateObjectCode;
        } else if (strcmp(argv[i], "-l") == 0) {
            arguments->compilation_mode = GenerateLinkedExecutable;
        } else if (strcmp(argv[i], "-o") == 0) {
            if (i + 1 == argc) {
                fprintf(stderr,
                        "Error: Argument '-o' must be followed by a file to "
                        "output to");
                help();
                return 1;
            }
            arguments->output_file = argv[i + 1];
            ++i;
        } else {
            Deque_push_back(&input_files, &argv[i], sizeof(char*));
        }
    }

    arguments->num_input_files = input_files.length;
    arguments->input_files = (char**)input_files.buffer;
    return 0;
}

void arguments_destroy(Arguments* arguments) { free(arguments->input_files); }
