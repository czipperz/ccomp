#pragma once

#include <stddef.h>

enum CompilationMode {
    GenerateAssembly,
    GenerateObjectCode,
    GenerateLinkedExecutable
};
typedef enum CompilationMode CompilationMode;

typedef struct Arguments Arguments;
struct Arguments {
    CompilationMode compilation_mode;
    size_t num_input_files;
    char** input_files;
    char* output_file;
};

int arguments_parse(Arguments*, int argc, char** argv);
void arguments_destroy(Arguments*);
