#include "call_graph.h"
#include "panic.h"
#include "parse.h"
#include "visit.h"
#include <stdlib.h>
#include <string.h>

CallGraph CallGraph_new(void) {
    CallGraph call_graph = { HashMap_of_string() };
    return call_graph;
}

static void destroy_pair(char** key, Deque* successors) {
    free(successors->buffer);
}

void CallGraph_destroy(CallGraph* call_graph) {
    HashMap_destroy(&call_graph->map, destroy_pair, sizeof(char*),
                    sizeof(Deque));
}

struct CallGraphVisitor {
    Cent_TopLevelVisitor visitor;
    CallGraph* call_graph;
    char* name;
};
typedef struct CallGraphVisitor CallGraphVisitor;

static void CallGraph_bind(CallGraph* call_graph, char* pred, char* succ) {
    Deque* successors = HashMap_get(&call_graph->map, &pred, sizeof(char*),
                                    sizeof(Deque));
    if (!successors) {
        Deque succ = Deque_new();
        HashMap_insert(&call_graph->map, &pred, &succ, sizeof(char*),
                       sizeof(Deque));
        successors = HashMap_get(&call_graph->map, &pred, sizeof(char*),
                                 sizeof(Deque));
    }
    Deque_push_back(successors, &succ, sizeof(char*));
}

static void CallGraph_visit_function_call_expression(
    CallGraphVisitor* visitor, Cent_FunctionCallExpression* expression) {
    if (expression->function->type == Cent_Expression_Variable) {
        assert(visitor->name);
        CallGraph_bind(visitor->call_graph, visitor->name,
                       expression->function->value.variable);
    }
    Cent_TopLevelVisitor_visit_function_call_expression(visitor, expression);
}

static void CallGraph_visit_function_definition(
    CallGraphVisitor* visitor, Cent_FunctionDefinition* definition) {
    visitor->name = definition->declaration.name;
    Cent_TopLevelVisitor_visit_function_definition(visitor, definition);
}

void CallGraph_add(CallGraph* call_graph, struct Cent_TopLevel* top_level) {
    CallGraphVisitor visitor = { Cent_TopLevelVisitor_new(), call_graph, 0 };
    visitor.visitor.visit_function_call_expression
        = CallGraph_visit_function_call_expression;
    visitor.visitor.visit_function_definition
        = CallGraph_visit_function_definition;
    Cent_TopLevelVisitor_accept_top_level(&visitor, top_level);
}

static void visit_pair(FILE* file, char** key, Deque* successors) {
    size_t j;
    for (j = 0; j < successors->length; ++j) {
        char** successor = Deque_at(successors, j, sizeof(char*));
        fprintf(file, "%s -> %s\n", *key, *successor);
    }
}
void CallGraph_print(const CallGraph* call_graph, FILE* file) {
    fprintf(file, "digraph G {\n");
    HashMap_accept(&call_graph->map, visit_pair, file, sizeof(char*),
                   sizeof(Deque));
    fprintf(file, "}\n");
}
