#pragma once

#include "hash_map.h"
#include "syntax.h"
#include <stdio.h>

struct CallGraph {
    HashMap map;
};
typedef struct CallGraph CallGraph;

CallGraph CallGraph_new(void);
void CallGraph_destroy(CallGraph*);

void CallGraph_add(CallGraph*, Cent_TopLevel*);
void CallGraph_print(const CallGraph*, FILE*);
