#include "keyword_analysis.h"
#include "panic.h"
#include "parse.h"
#include "visit.h"
#include <string.h>

struct KeywordAnalyzer {
    Cent_TopLevelVisitor visitor;
    int result;
};
typedef struct KeywordAnalyzer KeywordAnalyzer;

static int is_reserved_type(char* name) {
    char* keywords[] = { "int", "void" };
    size_t i;
    for (i = 0; i < sizeof(keywords) / sizeof(*keywords); ++i) {
        if (strcmp(name, keywords[i]) == 0) {
            return 1;
        }
    }
    return 0;
}

static int is_keyword(char* name) {
    char* keywords[] = { "return", "while", "if", "else", "continue", "break" };
    size_t i;
    for (i = 0; i < sizeof(keywords) / sizeof(*keywords); ++i) {
        if (strcmp(name, keywords[i]) == 0) {
            return 1;
        }
    }
    return 0;
}

static void analyze_keywords_variable_name(void* visitor, char* name) {
    if (is_reserved_type(name) || is_keyword(name)) {
        ((KeywordAnalyzer*)visitor)->result = 1;
    }
    Cent_TopLevelVisitor_visit_variable_name(visitor, name);
}

static void analyze_keywords_base_type(void* visitor, Cent_BaseType* base) {
    if (is_keyword(base->name)) {
        ((KeywordAnalyzer*)visitor)->result = 1;
    }
    Cent_TopLevelVisitor_visit_base_type(visitor, base);
}

int analyze_keywords(Cent_TopLevel* top_level) {
    KeywordAnalyzer analyzer = { Cent_TopLevelVisitor_new(), 0 };
    analyzer.visitor.visit_variable_name = analyze_keywords_variable_name;
    analyzer.visitor.visit_base_type = analyze_keywords_base_type;
    Cent_TopLevelVisitor_accept_top_level(&analyzer.visitor, top_level);
    return analyzer.result;
}
