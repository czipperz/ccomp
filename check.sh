#!/bin/bash

cd "$(dirname "$0")" || exit 1

function build {
    echo -e "\033[1;97m>>> Starting ${build_command} build\033[0m"
    time make ${build_command} -j4 -s
    ret="$?"

    if [ "$ret" -eq 0 ]; then
        echo -e "\033[1;97m>>> Finished ${build_command} build\033[0m"
    else
        echo -e "\033[1;91m>>> Error: ${build_command} build failed\033[0m"
    fi
}

function generate_tags {
    if ! make tags -s; then
        ret="$?"
        echo -e "\033[1;91m>>> Error: generating tags failed\033[0m"
        exit "$ret"
    fi
}

function check {
    echo -e "\033[1;97m>>> Running tests\033[0m"
    if time make ${check_command} -s; then
        echo -e "\033[1;97m>>> Finished running tests\033[0m"
    else
        ret="$?"
        echo -e "\033[1;91m>>> Error: running tests\033[0m"
        exit "$ret"
    fi
}

check_command=check
build_command=debug

for i in "$@"; do
    case "$i" in
        --valgrind)
            check_command=debug-check
            ;;
        --release)
            build_command=release
            ;;
        --clean)
            make clean
            ;;
    esac
done

build
generate_tags

if [ "$ret" -eq 0 ]; then
    check
else
    exit "$ret"
fi
