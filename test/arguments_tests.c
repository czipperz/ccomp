#include "arguments.h"
#include "test.h"

int arguments_parse__empty_args(void) {
    Arguments arguments;
    arguments_parse(&arguments, 0, 0);

    ASSERT_EQ(arguments.compilation_mode, GenerateAssembly);
    ASSERT_EQ(arguments.num_input_files, 0);
    ASSERT_EQ(arguments.input_files, 0);
    ASSERT_EQ(arguments.output_file, 0);

    arguments_destroy(&arguments);
    FINISH();
}

int arguments_parse__normal_arguments(void) {
    char* argv[] = { "a.c", "-o", "a.o", "-c" };
    Arguments arguments;
    arguments_parse(&arguments, 4, argv);

    ASSERT_EQ(arguments.compilation_mode, GenerateObjectCode);
    ASSERT_EQ(arguments.num_input_files, 1);
    ASSERT_NE(arguments.input_files, 0);
    ASSERT_STR_EQ(arguments.input_files[0], "a.c");
    ASSERT_STR_EQ(arguments.output_file, "a.o");

    arguments_destroy(&arguments);
    FINISH();
}

void arguments_tests(TestRunner* runner) {
    REGISTER(runner, arguments_parse__empty_args);
    REGISTER(runner, arguments_parse__normal_arguments);
}
