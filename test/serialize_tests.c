#include "serialize.h"
#include "test.h"
#include <stdlib.h>

int serialize__expression__dereference_variable(void) {
    char* serialized;
    Cent_Expression expression;
    Cent_Expression inner;

    expression.type = Cent_Expression_Dereference;
    expression.value.dereference.inner = &inner;
    inner.type = Cent_Expression_Variable;
    inner.value.variable = "abc";

    serialized = Cent_serialize_expression(&expression);
    ASSERT(serialized);
    ASSERT_STR_EQ(serialized, "*abc");
    free(serialized);

    FINISH();
}

int serialize__block_with_declaration_and_statement(void) {
    char* serialized;
    Cent_Statement outer;
    Cent_Block block;
    Cent_Declaration declaration;
    Cent_Expression value;
    Cent_Statement statement;

    outer.type = Cent_Statement_Block;
    outer.value.block = &block;

    block.num_declarations = 1;
    block.declarations = &declaration;
    declaration.type = Cent_Declaration_Variable;
    declaration.value.variable.type.type = Cent_Type_Base;
    declaration.value.variable.type.value.base.name = "int";
    declaration.value.variable.name = "x";
    declaration.value.variable.value = &value;
    value.type = Cent_Expression_Constant;
    value.value.constant.type = Cent_Constant_Integer;
    value.value.constant.value.integer = 1383;

    block.num_statements = 1;
    block.statements = &statement;
    statement.type = Cent_Statement_Empty;

    serialized = Cent_serialize_statement(&outer);
    ASSERT(serialized);
    ASSERT_STR_EQ(serialized, "\n\
{\n\
    int x = 1383;\n\
    ;\n\
}");
    free(serialized);

    FINISH();
}

void serialize_tests(TestRunner* runner) {
    REGISTER(runner, serialize__expression__dereference_variable);
    REGISTER(runner, serialize__block_with_declaration_and_statement);
}
