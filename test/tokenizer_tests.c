#include "test.h"
#include "tokenizer.h"
#include <stdlib.h>

int tokenizer__next(void) {
    Cent_Tokenizer tokenizer = Cent_Tokenizer_new(FileReader_from_string(
        "int f; int finish_function(int* var1, char var2);"));
    Cent_Token token;

    ASSERT_EQ(Cent_Tokenizer_next(&tokenizer, &token), 1);
    ASSERT_EQ(token.type, Cent_Token_Symbol);
    ASSERT_STR_EQ(token.value.symbol, "int");
    free(token.value.symbol);

    ASSERT_EQ(Cent_Tokenizer_next(&tokenizer, &token), 1);
    ASSERT_EQ(token.type, Cent_Token_Symbol);
    ASSERT_STR_EQ(token.value.symbol, "f");
    free(token.value.symbol);

    ASSERT_EQ(Cent_Tokenizer_next(&tokenizer, &token), 1);
    ASSERT_EQ(token.type, Cent_Token_Semicolon);

    ASSERT_EQ(Cent_Tokenizer_next(&tokenizer, &token), 1);
    ASSERT_EQ(token.type, Cent_Token_Symbol);
    ASSERT_STR_EQ(token.value.symbol, "int");
    free(token.value.symbol);

    ASSERT_EQ(Cent_Tokenizer_next(&tokenizer, &token), 1);
    ASSERT_EQ(token.type, Cent_Token_Symbol);
    ASSERT_STR_EQ(token.value.symbol, "finish_function");
    free(token.value.symbol);

    ASSERT_EQ(Cent_Tokenizer_next(&tokenizer, &token), 1);
    ASSERT_EQ(token.type, Cent_Token_OpenParen);

    ASSERT_EQ(Cent_Tokenizer_next(&tokenizer, &token), 1);
    ASSERT_EQ(token.type, Cent_Token_Symbol);
    ASSERT_STR_EQ(token.value.symbol, "int");
    free(token.value.symbol);

    ASSERT_EQ(Cent_Tokenizer_next(&tokenizer, &token), 1);
    ASSERT_EQ(token.type, Cent_Token_Star);

    ASSERT_EQ(Cent_Tokenizer_next(&tokenizer, &token), 1);
    ASSERT_EQ(token.type, Cent_Token_Symbol);
    ASSERT_STR_EQ(token.value.symbol, "var1");
    free(token.value.symbol);

    ASSERT_EQ(Cent_Tokenizer_next(&tokenizer, &token), 1);
    ASSERT_EQ(token.type, Cent_Token_Comma);

    ASSERT_EQ(Cent_Tokenizer_next(&tokenizer, &token), 1);
    ASSERT_EQ(token.type, Cent_Token_Symbol);
    ASSERT_STR_EQ(token.value.symbol, "char");
    free(token.value.symbol);

    ASSERT_EQ(Cent_Tokenizer_next(&tokenizer, &token), 1);
    ASSERT_EQ(token.type, Cent_Token_Symbol);
    ASSERT_STR_EQ(token.value.symbol, "var2");
    free(token.value.symbol);

    ASSERT_EQ(Cent_Tokenizer_next(&tokenizer, &token), 1);
    ASSERT_EQ(token.type, Cent_Token_CloseParen);

    ASSERT_EQ(Cent_Tokenizer_next(&tokenizer, &token), 1);
    ASSERT_EQ(token.type, Cent_Token_Semicolon);

    Cent_Tokenizer_destroy(&tokenizer);
    FINISH();
}

int tokenizer__string(void) {
    Cent_Tokenizer tokenizer = Cent_Tokenizer_new(
        FileReader_from_string("\"abc\""));
    Cent_Token token;

    ASSERT_EQ(Cent_Tokenizer_next(&tokenizer, &token), 1);
    ASSERT_EQ(token.type, Cent_Token_Constant);
    ASSERT_EQ(token.value.constant.type, Cent_Constant_String);
    ASSERT_STR_EQ(token.value.constant.value.string, "abc");
    free(token.value.constant.value.string);

    ASSERT(!Cent_Tokenizer_next(&tokenizer, &token));

    Cent_Tokenizer_destroy(&tokenizer);
    FINISH();
}

void tokenizer_tests(TestRunner* runner) {
    REGISTER(runner, tokenizer__next);
    REGISTER(runner, tokenizer__string);
}
