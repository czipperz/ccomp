#include "../test.h"
#include "hash_map.h"

int hash_map__insert_get(void) {
    HashMap hash_map = HashMap_of_int();
    int k;
    int v;

    k = 1;
    v = 2;
    ASSERT_EQ(HashMap_insert(&hash_map, &k, &v, sizeof(int), sizeof(int)), 0);
    ASSERT_NE(HashMap_get(&hash_map, &k, sizeof(int), sizeof(int)), 0);
    ASSERT_EQ(*(int*)HashMap_get(&hash_map, &k, sizeof(int), sizeof(int)), v);
    k = 2;
    ASSERT_EQ(HashMap_get(&hash_map, &k, sizeof(int), sizeof(int)), 0);

    HashMap_destroy(&hash_map, 0, sizeof(int), sizeof(int));
    FINISH();
}

int hash_map__insert_stress_test(void) {
    HashMap hash_map = HashMap_of_int();
    int i;
    double* ptr;
    for (i = -10; i <= 10; ++i) {
        double v = (double)i / 4.;
        ASSERT_EQ(
            HashMap_insert(&hash_map, &i, &v, sizeof(int), sizeof(double)), 0);
    }

    i = -3;
    ptr = HashMap_get(&hash_map, &i, sizeof(int), sizeof(double));
    ASSERT(ptr);
    ASSERT_EQ(*ptr, -.75);

    i = 9;
    ptr = HashMap_get(&hash_map, &i, sizeof(int), sizeof(double));
    ASSERT(ptr);
    ASSERT_EQ(*ptr, 2.25);

    HashMap_destroy(&hash_map, 0, sizeof(int), sizeof(double));
    FINISH();
}

int hash_map__set_stress_test(void) {
    HashMap hash_map = HashMap_of_int();
    int i;
    double* ptr;
    for (i = -10; i <= 10; ++i) {
        double v = (double)i / 4.;
        HashMap_set(&hash_map, &i, &v, sizeof(int), sizeof(double));
    }

    i = -3;
    ptr = HashMap_get(&hash_map, &i, sizeof(int), sizeof(double));
    ASSERT(ptr);
    ASSERT_EQ(*ptr, -.75);

    i = 9;
    ptr = HashMap_get(&hash_map, &i, sizeof(int), sizeof(double));
    ASSERT(ptr);
    ASSERT_EQ(*ptr, 2.25);

    HashMap_destroy(&hash_map, 0, sizeof(int), sizeof(double));
    FINISH();
}

int hash_map__remove_stress_test(void) {
    HashMap hash_map = HashMap_of_int();
    int i;
    double* ptr;
    for (i = -10; i <= 10; ++i) {
        double v = (double)i / 4.;
        HashMap_set(&hash_map, &i, &v, sizeof(int), sizeof(double));
    }

    i = -3;
    ptr = HashMap_get(&hash_map, &i, sizeof(int), sizeof(double));
    ASSERT(ptr);
    ASSERT_EQ(*ptr, -.75);
    HashMap_remove(&hash_map, &i, sizeof(int), sizeof(double));
    ASSERT(!HashMap_get(&hash_map, &i, sizeof(int), sizeof(double)));

    i = 9;
    ptr = HashMap_get(&hash_map, &i, sizeof(int), sizeof(double));
    ASSERT(ptr);
    ASSERT_EQ(*ptr, 2.25);
    HashMap_remove(&hash_map, &i, sizeof(int), sizeof(double));
    ASSERT(!HashMap_get(&hash_map, &i, sizeof(int), sizeof(double)));

    for (i = -10; i <= 10; ++i) {
        double v = (double)i / 4.;
        ptr = HashMap_get(&hash_map, &i, sizeof(int), sizeof(double));
        if (i == -3 || i == 9) {
            ASSERT(!ptr);
        } else {
            ASSERT(ptr);
            ASSERT_EQ(*ptr, v);
        }
    }

    for (i = -10; i <= 10; ++i) {
        HashMap_remove(&hash_map, &i, sizeof(int), sizeof(double));
    }

    for (i = -10; i <= 10; ++i) {
        ASSERT(!HashMap_get(&hash_map, &i, sizeof(int), sizeof(double)));
    }

    HashMap_destroy(&hash_map, 0, sizeof(int), sizeof(double));
    FINISH();
}

void hash_map_tests(TestRunner* runner) {
    REGISTER(runner, hash_map__insert_get);
    REGISTER(runner, hash_map__insert_stress_test);
    REGISTER(runner, hash_map__set_stress_test);
    REGISTER(runner, hash_map__remove_stress_test);
}
