#include "deque_string.h"
#include "test.h"
#include <stdlib.h>
#include <string.h>

int deque_string__push_back__no_resize(void) {
    DequeString string = DequeString_new();
    size_t i;
    size_t cap;

    DequeString_push_back(&string, 'a');
    cap = string.deque.capacity;

    for (i = 1; i < cap; ++i) {
        char ch = 'a' + i;
        DequeString_push_back(&string, ch);
        ASSERT_EQ(string.deque.buffer[i], ch);
        ASSERT_EQ(string.deque.length, i + 1);
        ASSERT_EQ(string.deque.capacity, cap);
        ASSERT_EQ(string.deque.offset, 0);
    }

    ASSERT(strncmp(string.deque.buffer, "abcdefghijklmnop", 16) == 0);

    DequeString_destroy(&string);

    FINISH();
}

int deque_string__push_back__resize_works_correctly(void) {
    DequeString string = DequeString_new();
    size_t i;

    for (i = 0; i < 26; ++i) {
        char ch = 'a' + i;
        DequeString_push_back(&string, ch);
        ASSERT_STRNEQ(string.deque.buffer, "abcdefghijklmnopqrstuvwxyz", i + 1);
        ASSERT_EQ(string.deque.length, i + 1);
        ASSERT_EQ(string.deque.offset, 0);
    }

    for (i = 0; i < 26; ++i) {
        char ch = 'a' + i;
        DequeString_push_back(&string, ch);
        ASSERT_STRNEQ(string.deque.buffer,
                      "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz",
                      i + 27);
        ASSERT_EQ(string.deque.length, i + 27);
        ASSERT_EQ(string.deque.offset, 0);
    }

    DequeString_destroy(&string);

    FINISH();
}

int deque_string__push_back__wrap(void) {
    DequeString string;
    char* buffer = malloc(16);

    string.deque.buffer = buffer;
    ASSERT(string.deque.buffer);
    string.deque.capacity = 16;
    memcpy(string.deque.buffer, "Xbcdefghijklmnop", 16);
    string.deque.length = 15;
    string.deque.offset = 1;

    DequeString_push_back(&string, 'a');
    ASSERT_STRNEQ(string.deque.buffer, "abcdefghijklmnop", 16);
    ASSERT_EQ(string.deque.buffer, buffer);
    ASSERT_EQ(string.deque.length, 16);
    ASSERT_EQ(string.deque.offset, 1);
    ASSERT_EQ(string.deque.capacity, 16);

    DequeString_destroy(&string);
    FINISH();
}

int deque_string__push_front__no_resize(void) {
    DequeString string = DequeString_new();
    size_t i;
    size_t cap;

    DequeString_push_front(&string, 'p');
    cap = string.deque.capacity;

    ASSERT_EQ(string.deque.buffer[cap - 1], 'p');
    ASSERT_EQ(string.deque.length, 1);
    ASSERT_EQ(string.deque.capacity, cap);
    ASSERT_EQ(string.deque.offset, cap - 1);

    for (i = 1; i < cap; ++i) {
        char ch = 'p' - i;
        DequeString_push_front(&string, ch);
        ASSERT_EQ(string.deque.buffer[cap - i - 1], ch);
        ASSERT_EQ(string.deque.length, i + 1);
        ASSERT_EQ(string.deque.capacity, cap);
        ASSERT_EQ(string.deque.offset, cap - i - 1);
    }

    ASSERT_STRNEQ(string.deque.buffer, "abcdefghijklmnop", 16);

    DequeString_destroy(&string);

    FINISH();
}

int deque_string__push_front_back(void) {
    DequeString string = DequeString_new();
    size_t i;
    size_t cap;

    DequeString_push_front(&string, 'p');
    cap = string.deque.capacity;

    ASSERT_EQ(string.deque.buffer[cap - 1], 'p');
    ASSERT_EQ(string.deque.length, 1);
    ASSERT_EQ(string.deque.capacity, cap);
    ASSERT_EQ(string.deque.offset, cap - 1);

    for (i = 1; i < cap / 2; ++i) {
        char ch = 'p' - i;
        DequeString_push_front(&string, ch);
        ASSERT_EQ(string.deque.buffer[cap - i - 1], ch);
        ASSERT_EQ(string.deque.length, i + 1);
        ASSERT_EQ(string.deque.capacity, cap);
        ASSERT_EQ(string.deque.offset, cap - i - 1);
    }

    for (i = 0; i < cap / 2; ++i) {
        char ch = 'a' + i;
        DequeString_push_back(&string, ch);
    }

    ASSERT_STRNEQ(string.deque.buffer, "abcdefghijklmnop", 16);

    DequeString_destroy(&string);

    FINISH();
}

int deque_string__pop_front__empty(void) {
    DequeString string = DequeString_new();

    ASSERT_EQ(DequeString_pop_front(&string), 0);
    ASSERT_EQ(string.deque.capacity, 0);
    ASSERT_EQ(string.deque.length, 0);

    DequeString_destroy(&string);
    FINISH();
}

int deque_string__pop_front__one_element(void) {
    DequeString string = DequeString_new();

    DequeString_push_back(&string, 'a');

    ASSERT_EQ(DequeString_pop_front(&string), 'a');
    ASSERT(string.deque.capacity >= 1);
    ASSERT_EQ(string.deque.length, 0);

    DequeString_destroy(&string);
    FINISH();
}

int deque_string__pop_front__wrap(void) {
    DequeString string;
    char* buffer = malloc(16);

    string.deque.buffer = buffer;
    ASSERT(string.deque.buffer);
    string.deque.capacity = 16;
    memcpy(string.deque.buffer, "abcdefghijklmnop", 16);
    string.deque.length = 16;
    string.deque.offset = 1;

    ASSERT_EQ(DequeString_pop_front(&string), 'b');
    ASSERT_STRNEQ(string.deque.buffer, "abcdefghijklmnop", 16);
    ASSERT_EQ(string.deque.buffer, buffer);
    ASSERT_EQ(string.deque.length, 15);
    ASSERT_EQ(string.deque.offset, 2);
    ASSERT_EQ(string.deque.capacity, 16);

    DequeString_destroy(&string);
    FINISH();
}

int deque_string__push_pop_interspersed(void) {
    DequeString string = DequeString_new();
    int i;

    for (i = 0; i < 10; ++i) {
        DequeString_push_back(&string, 'a' + i);
    }
    ASSERT_STRNEQ(string.deque.buffer, "abcdefghij", 10);
    ASSERT_EQ(string.deque.offset, 0);
    ASSERT_EQ(string.deque.length, 10);
    for (i = 0; i < 10; ++i) {
        ASSERT_EQ(DequeString_pop_front(&string), 'a' + i);
    }
    ASSERT_EQ(string.deque.offset, 10);
    ASSERT_EQ(string.deque.length, 0);

    {
        char* prev_buffer = string.deque.buffer;
        for (i = 0; i < 10; ++i) {
            DequeString_push_back(&string, 'a' + i);
            ASSERT_EQ(string.deque.buffer, prev_buffer);
        }
    }
    ASSERT_STRNEQ(string.deque.buffer + 10, "abcdefghij", 6);
    ASSERT_STRNEQ(string.deque.buffer, "abcdefghij" + 6, 4);
    ASSERT_EQ(string.deque.offset, 10);
    ASSERT_EQ(string.deque.length, 10);
    for (i = 0; i < 10; ++i) {
        ASSERT_EQ(DequeString_pop_front(&string), 'a' + i);
    }

    DequeString_destroy(&string);
    FINISH();
}

int deque_string__push_pop_interspersed__resize(void) {
    DequeString string = DequeString_new();
    int i;

    for (i = 0; i < 10; ++i) {
        DequeString_push_back(&string, 'a' + i);
    }
    ASSERT_STRNEQ(string.deque.buffer, "abcdefghij", 10);
    ASSERT_EQ(string.deque.offset, 0);
    ASSERT_EQ(string.deque.length, 10);
    for (i = 0; i < 10; ++i) {
        ASSERT_EQ(DequeString_pop_front(&string), 'a' + i);
    }
    ASSERT_EQ(string.deque.offset, 10);
    ASSERT_EQ(string.deque.length, 0);

    {
        char* prev_buffer = string.deque.buffer;
        for (i = 0; i < 16; ++i) {
            DequeString_push_back(&string, 'a' + i);
            ASSERT_EQ(string.deque.buffer, prev_buffer);
        }
        for (i = 16; i < 20; ++i) {
            DequeString_push_back(&string, 'a' + i);
            ASSERT_NE(string.deque.buffer, prev_buffer);
        }
    }
    ASSERT_STRNEQ(string.deque.buffer, "abcdefghijklmnopqrst", 20);
    ASSERT_EQ(string.deque.offset, 0);
    ASSERT_EQ(string.deque.length, 20);
    for (i = 0; i < 20; ++i) {
        ASSERT_EQ(DequeString_pop_front(&string), 'a' + i);
    }

    DequeString_destroy(&string);
    FINISH();
}

int deque_string__remove__basic(void) {
    DequeString string = DequeString_copy("   abcd");
    string.deque.offset = 3;
    string.deque.length = 4;

    ASSERT_EQ(DequeString_remove(&string, 1), 'b');
    ASSERT_EQ(string.deque.offset, 3);
    ASSERT_EQ(string.deque.length, 3);
    ASSERT(memcmp(string.deque.buffer + 3, "acd", 3) == 0);

    DequeString_destroy(&string);
    FINISH();
}

int deque_string__remove__wrapping(void) {
    DequeString string = DequeString_copy("efgabcd");
    string.deque.offset = 3;

    ASSERT_EQ(DequeString_remove(&string, 1), 'b');
    ASSERT_EQ(string.deque.offset, 3);
    ASSERT_EQ(string.deque.length, 6);
    ASSERT(memcmp(string.deque.buffer, "fg", 2) == 0);
    ASSERT(memcmp(string.deque.buffer + 3, "acde", 4) == 0);

    DequeString_destroy(&string);
    FINISH();
}

int deque_string__at(void) {
    DequeString string;
    char* buffer = malloc(16);

    string.deque.buffer = buffer;
    ASSERT(string.deque.buffer);
    string.deque.capacity = 16;
    memcpy(string.deque.buffer, "abcdefghijklmnop", 16);
    string.deque.length = 16;
    string.deque.offset = 8;

    ASSERT_EQ(DequeString_at(&string, 0), 'i');
    ASSERT_EQ(DequeString_at(&string, 8), 'a');
    ASSERT_EQ(DequeString_at(&string, 15), 'h');

    DequeString_destroy(&string);
    FINISH();
}

void deque_string_tests(TestRunner* runner) {
    REGISTER(runner, deque_string__push_back__no_resize);
    REGISTER(runner, deque_string__push_back__resize_works_correctly);
    REGISTER(runner, deque_string__push_back__wrap);
    REGISTER(runner, deque_string__push_front__no_resize);
    REGISTER(runner, deque_string__push_front_back);
    REGISTER(runner, deque_string__pop_front__empty);
    REGISTER(runner, deque_string__pop_front__one_element);
    REGISTER(runner, deque_string__pop_front__wrap);
    REGISTER(runner, deque_string__push_pop_interspersed);
    REGISTER(runner, deque_string__push_pop_interspersed__resize);
    REGISTER(runner, deque_string__remove__basic);
    REGISTER(runner, deque_string__remove__wrapping);
    REGISTER(runner, deque_string__at);
}
