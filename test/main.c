#include "test.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char** argv) {
    int status;
    TestRunner runner = TestRunner_new();
    TestRunnerOptions_parse(&runner.options, argc - 1, argv + 1);

#define run(test)                                                              \
    do {                                                                       \
        void test(TestRunner*);                                                \
        test(&runner);                                                         \
    } while (0)

    run(deque_string_tests);
    run(hash_map_tests);
    run(arguments_tests);
    run(tokenizer_tests);
    run(parser_tests);
    run(keyword_analysis_tests);
    run(serialize_tests);
    run(control_flow_tests);

    status = TestRunner_run(&runner);

    TestRunner_destroy(&runner);
    return status;
}
