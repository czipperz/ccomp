#include "parse.h"
#include "test.h"

int parser__parse_type__int() {
    Cent_Parser parser = Cent_Parser_from_string("int");
    Cent_Type type;

    ASSERT_EQ(Cent_Parser_parse_type(&parser, &type), 1);
    ASSERT_EQ(type.type, Cent_Type_Base);
    ASSERT_STR_EQ(type.value.base.name, "int");
    Cent_Type_destroy(&type);

    Cent_Parser_destroy(&parser);
    FINISH();
}

int parser__parse_type__intp() {
    Cent_Parser parser = Cent_Parser_from_string("int*");
    Cent_Type type;

    ASSERT_EQ(Cent_Parser_parse_type(&parser, &type), 1);
    ASSERT_EQ(type.type, Cent_Type_Pointer);
    ASSERT(type.value.pointer.inner);
    ASSERT_EQ(type.value.pointer.inner->type, Cent_Type_Base);
    ASSERT_STR_EQ(type.value.pointer.inner->value.base.name, "int");
    Cent_Type_destroy(&type);

    Cent_Parser_destroy(&parser);
    FINISH();
}

int parser__parse_type__intpp() {
    Cent_Parser parser = Cent_Parser_from_string("int * *");
    Cent_Type type;

    ASSERT_EQ(Cent_Parser_parse_type(&parser, &type), 1);
    ASSERT_EQ(type.type, Cent_Type_Pointer);
    ASSERT(type.value.pointer.inner);
    ASSERT_EQ(type.value.pointer.inner->type, Cent_Type_Pointer);
    ASSERT(type.value.pointer.inner->value.pointer.inner);
    ASSERT_EQ(type.value.pointer.inner->value.pointer.inner->type,
              Cent_Type_Base);
    ASSERT_STR_EQ(
        type.value.pointer.inner->value.pointer.inner->value.base.name, "int");
    Cent_Type_destroy(&type);

    Cent_Parser_destroy(&parser);
    FINISH();
}

int parser__parse_type__intpp_then_symbol() {
    Cent_Parser parser = Cent_Parser_from_string("int** x;");
    Cent_Type type;

    ASSERT_EQ(Cent_Parser_parse_type(&parser, &type), 1);
    ASSERT_EQ(type.type, Cent_Type_Pointer);
    ASSERT(type.value.pointer.inner);
    ASSERT_EQ(type.value.pointer.inner->type, Cent_Type_Pointer);
    ASSERT(type.value.pointer.inner->value.pointer.inner);
    ASSERT_EQ(type.value.pointer.inner->value.pointer.inner->type,
              Cent_Type_Base);
    ASSERT_STR_EQ(
        type.value.pointer.inner->value.pointer.inner->value.base.name, "int");
    Cent_Type_destroy(&type);

    {
        Cent_Token* first_token = Deque_at(&parser.tokenizer.token_queue, 0,
                                           sizeof(Cent_Token));
        ASSERT(first_token);
        ASSERT_EQ(first_token->type, Cent_Token_Symbol);
    }

    Cent_Parser_destroy(&parser);
    FINISH();
}
