#include "parse.h"
#include "test.h"

int parser__next_top_level__function_definition__empty_body() {
    Cent_Parser parser = Cent_Parser_from_string("int func(int x) {}");
    Cent_TopLevel top_level;

    ASSERT_EQ(Cent_Parser_next_top_level(&parser, &top_level), 1);
    ASSERT_EQ(top_level.type, Cent_TopLevel_FunctionDefinition);
    ASSERT_EQ(top_level.value.function_definition.declaration.return_type.type,
              Cent_Type_Base);
    ASSERT_STR_EQ(top_level.value.function_definition.declaration.return_type
                      .value.base.name,
                  "int");
    ASSERT_EQ(top_level.value.function_definition.body.num_declarations, 0);
    ASSERT_EQ(top_level.value.function_definition.body.num_statements, 0);

    Cent_TopLevel_destroy(&top_level);
    Cent_Parser_destroy(&parser);
    FINISH();
}

int parser__next_top_level__function_definition__return_constant() {
    Cent_Parser parser = Cent_Parser_from_string(
        "int func(int x) { return 3; }");
    Cent_TopLevel top_level;

    ASSERT_EQ(Cent_Parser_next_top_level(&parser, &top_level), 1);
    ASSERT_EQ(top_level.type, Cent_TopLevel_FunctionDefinition);
    ASSERT_EQ(top_level.value.function_definition.declaration.return_type.type,
              Cent_Type_Base);
    ASSERT_STR_EQ(top_level.value.function_definition.declaration.return_type
                      .value.base.name,
                  "int");
    ASSERT_EQ(top_level.value.function_definition.body.num_declarations, 0);
    ASSERT_EQ(top_level.value.function_definition.body.num_statements, 1);
    ASSERT_EQ(top_level.value.function_definition.body.statements[0].type,
              Cent_Statement_Return);
    ASSERT_EQ(top_level.value.function_definition.body.statements[0]
                  .value.returns.is_valid,
              1);
    ASSERT_EQ(top_level.value.function_definition.body.statements[0]
                  .value.returns.expression.type,
              Cent_Expression_Constant);
    ASSERT_EQ(top_level.value.function_definition.body.statements[0]
                  .value.returns.expression.value.constant.type,
              Cent_Constant_Integer);
    ASSERT_EQ(top_level.value.function_definition.body.statements[0]
                  .value.returns.expression.value.constant.value.integer,
              3);

    Cent_TopLevel_destroy(&top_level);
    Cent_Parser_destroy(&parser);
    FINISH();
}

int parser__next_top_level__function_definition__no_parameters() {
    Cent_Parser parser = Cent_Parser_from_string("void func() {}");
    Cent_TopLevel top_level;

    ASSERT_EQ(Cent_Parser_next_top_level(&parser, &top_level), 1);
    ASSERT_EQ(top_level.type, Cent_TopLevel_FunctionDefinition);
    ASSERT_EQ(top_level.value.function_definition.declaration.return_type.type,
              Cent_Type_Base);
    ASSERT_STR_EQ(top_level.value.function_definition.declaration.return_type
                      .value.base.name,
                  "void");
    ASSERT_EQ(top_level.value.function_definition.declaration.num_parameters,
              0);
    ASSERT_EQ(top_level.value.function_definition.body.num_declarations, 0);
    ASSERT_EQ(top_level.value.function_definition.body.num_statements, 0);

    ASSERT_EQ(Cent_Parser_next_top_level(&parser, &top_level), 0);

    Cent_TopLevel_destroy(&top_level);
    Cent_Parser_destroy(&parser);
    FINISH();
}

int parser__next_top_level__function_definition__expression_constant() {
    Cent_Parser parser = Cent_Parser_from_string("void func() { 3; }");
    Cent_TopLevel top_level;

    ASSERT_EQ(Cent_Parser_next_top_level(&parser, &top_level), 1);
    ASSERT_EQ(top_level.type, Cent_TopLevel_FunctionDefinition);
    ASSERT_EQ(top_level.value.function_definition.declaration.return_type.type,
              Cent_Type_Base);
    ASSERT_STR_EQ(top_level.value.function_definition.declaration.return_type
                      .value.base.name,
                  "void");
    ASSERT_EQ(top_level.value.function_definition.declaration.num_parameters,
              0);
    ASSERT_EQ(top_level.value.function_definition.body.num_declarations, 0);
    ASSERT_EQ(top_level.value.function_definition.body.num_statements, 1);
    ASSERT_EQ(top_level.value.function_definition.body.statements[0].type,
              Cent_Statement_Expression);
    ASSERT_EQ(top_level.value.function_definition.body.statements[0]
                  .value.expression.type,
              Cent_Expression_Constant);
    ASSERT_EQ(top_level.value.function_definition.body.statements[0]
                  .value.expression.value.constant.type,
              Cent_Constant_Integer);
    ASSERT_EQ(top_level.value.function_definition.body.statements[0]
                  .value.expression.value.constant.value.integer,
              3);

    Cent_TopLevel_destroy(&top_level);
    Cent_Parser_destroy(&parser);
    FINISH();
}

int parser__next_top_level__function_definition__declarare_variable_then_return_it() {
    Cent_Parser parser = Cent_Parser_from_string(
        "int func(int x) { int y; return y; }");
    Cent_TopLevel top_level;

    ASSERT_EQ(Cent_Parser_next_top_level(&parser, &top_level), 1);
    ASSERT_EQ(top_level.type, Cent_TopLevel_FunctionDefinition);
    ASSERT_EQ(top_level.value.function_definition.declaration.return_type.type,
              Cent_Type_Base);
    ASSERT_STR_EQ(top_level.value.function_definition.declaration.return_type
                      .value.base.name,
                  "int");
    ASSERT_EQ(top_level.value.function_definition.body.num_declarations, 1);
    ASSERT_EQ(top_level.value.function_definition.body.declarations[0].type,
              Cent_Declaration_Variable);
    ASSERT_EQ(top_level.value.function_definition.body.declarations[0]
                  .value.variable.type.type,
              Cent_Type_Base);
    ASSERT_STR_EQ(top_level.value.function_definition.body.declarations[0]
                      .value.variable.type.value.base.name,
                  "int");
    ASSERT_STR_EQ(top_level.value.function_definition.body.declarations[0]
                      .value.variable.name,
                  "y");
    ASSERT_EQ(top_level.value.function_definition.body.declarations[0]
                  .value.variable.value,
              0);

    ASSERT_EQ(top_level.value.function_definition.body.num_statements, 1);
    ASSERT_EQ(top_level.value.function_definition.body.statements[0].type,
              Cent_Statement_Return);
    ASSERT_EQ(top_level.value.function_definition.body.statements[0]
                  .value.returns.is_valid,
              1);
    ASSERT_EQ(top_level.value.function_definition.body.statements[0]
                  .value.returns.expression.type,
              Cent_Expression_Variable);
    ASSERT_STR_EQ(top_level.value.function_definition.body.statements[0]
                      .value.returns.expression.value.variable,
                  "y");

    Cent_TopLevel_destroy(&top_level);
    Cent_Parser_destroy(&parser);
    FINISH();
}
