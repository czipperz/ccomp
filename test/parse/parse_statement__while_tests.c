#include "../test.h"
#include "parse.h"

int parser__parse_statement__while_then_statement() {
    Cent_Parser parser = Cent_Parser_from_string("while(a)return x;");
    Cent_Statement statement;

    ASSERT_EQ(Cent_Parser_parse_statement(&parser, &statement), 1);
    ASSERT_EQ(statement.type, Cent_Statement_While);
    ASSERT_EQ(statement.value.whiles.condition.type, Cent_Expression_Variable);
    ASSERT_STR_EQ(statement.value.whiles.condition.value.variable, "a");
    ASSERT_EQ(statement.value.whiles.body->type, Cent_Statement_Return);
    ASSERT(statement.value.whiles.body->value.returns.is_valid);
    ASSERT_EQ(statement.value.whiles.body->value.returns.expression.type,
              Cent_Expression_Variable);
    ASSERT_STR_EQ(
        statement.value.whiles.body->value.returns.expression.value.variable,
        "x");

    Cent_Statement_destroy(&statement);
    Cent_Parser_destroy(&parser);
    FINISH();
}

int parser__parse_statement__while_then_semicolon() {
    Cent_Parser parser = Cent_Parser_from_string("while(a);");
    Cent_Statement statement;

    ASSERT_EQ(Cent_Parser_parse_statement(&parser, &statement), 1);
    ASSERT_EQ(statement.type, Cent_Statement_While);
    ASSERT_EQ(statement.value.whiles.condition.type, Cent_Expression_Variable);
    ASSERT_STR_EQ(statement.value.whiles.condition.value.variable, "a");
    ASSERT_EQ(statement.value.whiles.body->type, Cent_Statement_Empty);

    Cent_Statement_destroy(&statement);
    Cent_Parser_destroy(&parser);
    FINISH();
}
