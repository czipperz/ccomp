#include "../test.h"
#include "parse.h"

int parser__parse_statement__if_then_statement(void) {
    Cent_Parser parser = Cent_Parser_from_string("if(a) f(x);");
    Cent_Statement statement;

    ASSERT_EQ(Cent_Parser_parse_statement(&parser, &statement), 1);
    ASSERT_EQ(statement.type, Cent_Statement_If);
    ASSERT_EQ(statement.value.ifs.condition.type, Cent_Expression_Variable);
    ASSERT_STR_EQ(statement.value.ifs.condition.value.variable, "a");
    ASSERT_EQ(statement.value.ifs.trues->type, Cent_Statement_Expression);
    ASSERT_EQ(statement.value.ifs.trues->value.expression.type,
              Cent_Expression_FunctionCall);
    ASSERT_EQ(statement.value.ifs.trues->value.expression.value.function_call
                  .function->type,
              Cent_Expression_Variable);
    ASSERT_STR_EQ(statement.value.ifs.trues->value.expression.value
                      .function_call.function->value.variable,
                  "f");
    ASSERT_EQ(statement.value.ifs.trues->value.expression.value.function_call
                  .num_arguments,
              1);
    ASSERT_EQ(statement.value.ifs.trues->value.expression.value.function_call
                  .arguments[0]
                  .type,
              Cent_Expression_Variable);
    ASSERT_STR_EQ(
        statement.value.ifs.trues->value.expression.value.function_call
            .arguments[0]
            .value.variable,
        "x");
    ASSERT_EQ(statement.value.ifs.falses, 0);
    Cent_Statement_destroy(&statement);

    ASSERT_EQ(Cent_Parser_parse_statement(&parser, &statement), 0);

    Cent_Parser_destroy(&parser);
    FINISH();
}

int parser__parse_statement__if_then_else(void) {
    Cent_Parser parser = Cent_Parser_from_string("if(a) b; else c;");
    Cent_Statement statement;

    ASSERT_EQ(Cent_Parser_parse_statement(&parser, &statement), 1);
    ASSERT_EQ(statement.type, Cent_Statement_If);
    ASSERT_EQ(statement.value.ifs.condition.type, Cent_Expression_Variable);
    ASSERT_STR_EQ(statement.value.ifs.condition.value.variable, "a");
    ASSERT_NE(statement.value.ifs.trues, 0);
    ASSERT_EQ(statement.value.ifs.trues->type, Cent_Statement_Expression);
    ASSERT_EQ(statement.value.ifs.trues->value.expression.type,
              Cent_Expression_Variable);
    ASSERT_STR_EQ(statement.value.ifs.trues->value.expression.value.variable,
                  "b");
    ASSERT_EQ(statement.value.ifs.falses->value.expression.type,
              Cent_Expression_Variable);
    ASSERT_STR_EQ(statement.value.ifs.falses->value.expression.value.variable,
                  "c");
    Cent_Statement_destroy(&statement);

    ASSERT_EQ(Cent_Parser_parse_statement(&parser, &statement), 0);

    Cent_Parser_destroy(&parser);
    FINISH();
}

int parser__parse_statement__if_then_else_returns(void) {
    Cent_Parser parser = Cent_Parser_from_string(
        "if(a) { return; } else { return; }");
    Cent_Statement statement;

    ASSERT_EQ(Cent_Parser_parse_statement(&parser, &statement), 1);
    ASSERT_EQ(statement.type, Cent_Statement_If);
    ASSERT_EQ(statement.value.ifs.condition.type, Cent_Expression_Variable);
    ASSERT_STR_EQ(statement.value.ifs.condition.value.variable, "a");
    ASSERT_NE(statement.value.ifs.trues, 0);
    ASSERT_EQ(statement.value.ifs.trues->type, Cent_Statement_Block);
    ASSERT_EQ(statement.value.ifs.trues->value.block->num_declarations, 0);
    ASSERT_EQ(statement.value.ifs.trues->value.block->num_statements, 1);
    ASSERT_EQ(statement.value.ifs.trues->value.block->statements[0].type,
              Cent_Statement_Return);
    ASSERT_EQ(statement.value.ifs.trues->value.block->statements[0]
                  .value.returns.is_valid,
              0);
    ASSERT_NE(statement.value.ifs.falses, 0);
    ASSERT_EQ(statement.value.ifs.falses->type, Cent_Statement_Block);
    ASSERT_EQ(statement.value.ifs.falses->value.block->num_declarations, 0);
    ASSERT_EQ(statement.value.ifs.falses->value.block->num_statements, 1);
    ASSERT_EQ(statement.value.ifs.falses->value.block->statements[0].type,
              Cent_Statement_Return);
    ASSERT_EQ(statement.value.ifs.falses->value.block->statements[0]
                  .value.returns.is_valid,
              0);
    Cent_Statement_destroy(&statement);

    ASSERT_EQ(Cent_Parser_parse_statement(&parser, &statement), 0);

    Cent_Parser_destroy(&parser);
    FINISH();
}
