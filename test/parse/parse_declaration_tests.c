#include "../test.h"
#include "parse.h"

int parser__parse_declaration__no_value() {
    Cent_Parser parser = Cent_Parser_from_string("int i;");
    Cent_Declaration declaration;

    ASSERT_EQ(Cent_Parser_parse_declaration(&parser, &declaration), 1);
    ASSERT_EQ(declaration.type, Cent_Declaration_Variable);
    ASSERT_EQ(declaration.value.variable.type.type, Cent_Type_Base);
    ASSERT_STR_EQ(declaration.value.variable.type.value.base.name, "int");
    ASSERT_STR_EQ(declaration.value.variable.name, "i");
    ASSERT_EQ(declaration.value.variable.value, 0);
    Cent_Declaration_destroy(&declaration);

    ASSERT_EQ(Cent_Parser_parse_declaration(&parser, &declaration), 0);

    Cent_Parser_destroy(&parser);
    FINISH();
}

int parser__parse_declaration__constant_value() {
    Cent_Parser parser = Cent_Parser_from_string("int i = 3;");
    Cent_Declaration declaration;

    ASSERT_EQ(Cent_Parser_parse_declaration(&parser, &declaration), 1);
    ASSERT_EQ(declaration.type, Cent_Declaration_Variable);
    ASSERT_EQ(declaration.value.variable.type.type, Cent_Type_Base);
    ASSERT_STR_EQ(declaration.value.variable.type.value.base.name, "int");
    ASSERT_STR_EQ(declaration.value.variable.name, "i");
    ASSERT_NE(declaration.value.variable.value, 0);
    ASSERT_EQ(declaration.value.variable.value->type, Cent_Expression_Constant);
    ASSERT_EQ(declaration.value.variable.value->value.constant.type,
              Cent_Constant_Integer);
    ASSERT_EQ(declaration.value.variable.value->value.constant.value.integer,
              3);
    Cent_Declaration_destroy(&declaration);

    ASSERT_EQ(Cent_Parser_parse_declaration(&parser, &declaration), 0);

    Cent_Parser_destroy(&parser);
    FINISH();
}
