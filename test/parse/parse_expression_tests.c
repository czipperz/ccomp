#include "parse.h"
#include "test.h"

int parser__parse_expression__function_call_no_args() {
    Cent_Parser parser = Cent_Parser_from_string("f()");
    Cent_Expression expression;

    ASSERT_EQ(Cent_Parser_parse_expression(&parser, &expression), 1);
    ASSERT_EQ(expression.type, Cent_Expression_FunctionCall);
    ASSERT_EQ(expression.value.function_call.function->type,
              Cent_Expression_Variable);
    ASSERT_STR_EQ(expression.value.function_call.function->value.variable, "f");
    ASSERT_EQ(expression.value.function_call.num_arguments, 0);

    Cent_Expression_destroy(&expression);
    Cent_Parser_destroy(&parser);
    FINISH();
}

int parser__parse_expression__function_call() {
    Cent_Parser parser = Cent_Parser_from_string("f(x)");
    Cent_Expression expression;

    ASSERT_EQ(Cent_Parser_parse_expression(&parser, &expression), 1);
    ASSERT_EQ(expression.type, Cent_Expression_FunctionCall);
    ASSERT_EQ(expression.value.function_call.function->type,
              Cent_Expression_Variable);
    ASSERT_STR_EQ(expression.value.function_call.function->value.variable, "f");
    ASSERT_EQ(expression.value.function_call.num_arguments, 1);
    ASSERT_EQ(expression.value.function_call.arguments[0].type,
              Cent_Expression_Variable);
    ASSERT_STR_EQ(expression.value.function_call.arguments[0].value.variable,
                  "x");

    Cent_Expression_destroy(&expression);
    Cent_Parser_destroy(&parser);
    FINISH();
}

int parser__parse_expression__dereference_pointer() {
    Cent_Parser parser = Cent_Parser_from_string("*x");
    Cent_Expression expression;

    ASSERT_EQ(Cent_Parser_parse_expression(&parser, &expression), 1);
    ASSERT_EQ(expression.type, Cent_Expression_Dereference);
    ASSERT_EQ(expression.value.dereference.inner->type,
              Cent_Expression_Variable);
    ASSERT_STR_EQ(expression.value.dereference.inner->value.variable, "x");

    Cent_Expression_destroy(&expression);
    Cent_Parser_destroy(&parser);
    FINISH();
}

int parser__parse_expression__parenthesis_expression() {
    Cent_Parser parser = Cent_Parser_from_string("(*(x))");
    Cent_Expression expression;

    ASSERT_EQ(Cent_Parser_parse_expression(&parser, &expression), 1);
    ASSERT_EQ(expression.type, Cent_Expression_Dereference);
    ASSERT_EQ(expression.value.dereference.inner->type,
              Cent_Expression_Variable);
    ASSERT_STR_EQ(expression.value.dereference.inner->value.variable, "x");

    Cent_Expression_destroy(&expression);
    Cent_Parser_destroy(&parser);
    FINISH();
}

int parser__parse_expression__assign_expression() {
    Cent_Parser parser = Cent_Parser_from_string("x = y");
    Cent_Expression expression;

    ASSERT_EQ(Cent_Parser_parse_expression(&parser, &expression), 1);
    ASSERT_EQ(expression.type, Cent_Expression_Binary);
    ASSERT_EQ(expression.value.binary.operator, Cent_Operator_Assign);
    ASSERT_NE(expression.value.binary.left, 0);
    ASSERT_EQ(expression.value.binary.left->type, Cent_Expression_Variable);
    ASSERT_STR_EQ(expression.value.binary.left->value.variable, "x");
    ASSERT_NE(expression.value.binary.right, 0);
    ASSERT_EQ(expression.value.binary.right->type, Cent_Expression_Variable);
    ASSERT_STR_EQ(expression.value.binary.right->value.variable, "y");
    Cent_Expression_destroy(&expression);

    ASSERT_EQ(Cent_Parser_parse_expression(&parser, &expression), 0);

    Cent_Parser_destroy(&parser);
    FINISH();
}

int parser__parse_expression__assign_pointers_expression() {
    Cent_Parser parser = Cent_Parser_from_string("*x = *y");
    Cent_Expression expression;

    ASSERT_EQ(Cent_Parser_parse_expression(&parser, &expression), 1);
    ASSERT_EQ(expression.type, Cent_Expression_Binary);
    ASSERT_EQ(expression.value.binary.operator, Cent_Operator_Assign);
    ASSERT_NE(expression.value.binary.left, 0);
    ASSERT_EQ(expression.value.binary.left->type, Cent_Expression_Dereference);
    ASSERT_EQ(expression.value.binary.left->value.dereference.inner->type,
              Cent_Expression_Variable);
    ASSERT_STR_EQ(
        expression.value.binary.left->value.dereference.inner->value.variable,
        "x");
    ASSERT_NE(expression.value.binary.right, 0);
    ASSERT_EQ(expression.value.binary.right->type, Cent_Expression_Dereference);
    ASSERT_EQ(expression.value.binary.right->value.dereference.inner->type,
              Cent_Expression_Variable);
    ASSERT_STR_EQ(
        expression.value.binary.right->value.dereference.inner->value.variable,
        "y");
    Cent_Expression_destroy(&expression);

    ASSERT_EQ(Cent_Parser_parse_expression(&parser, &expression), 0);

    Cent_Parser_destroy(&parser);
    FINISH();
}

int parser__parse_expression__dereference_function_call() {
    Cent_Parser parser = Cent_Parser_from_string("*f(x)");
    Cent_Expression expression;

    ASSERT_EQ(Cent_Parser_parse_expression(&parser, &expression), 1);
    ASSERT_EQ(expression.type, Cent_Expression_Dereference);
    ASSERT(expression.value.dereference.inner);
    ASSERT_EQ(expression.value.dereference.inner->type,
              Cent_Expression_FunctionCall);
    ASSERT_EQ(
        expression.value.dereference.inner->value.function_call.function->type,
        Cent_Expression_Variable);
    ASSERT_STR_EQ(expression.value.dereference.inner->value.function_call
                      .function->value.variable,
                  "f");
    ASSERT_EQ(
        expression.value.dereference.inner->value.function_call.num_arguments,
        1);
    ASSERT_EQ(
        expression.value.dereference.inner->value.function_call.arguments[0]
            .type,
        Cent_Expression_Variable);
    ASSERT_STR_EQ(
        expression.value.dereference.inner->value.function_call.arguments[0]
            .value.variable,
        "x");
    Cent_Expression_destroy(&expression);

    ASSERT_EQ(Cent_Parser_parse_expression(&parser, &expression), 0);

    Cent_Parser_destroy(&parser);
    FINISH();
}
