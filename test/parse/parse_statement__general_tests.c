#include "../test.h"
#include "parse.h"

int parser__parse_statement__empty() {
    Cent_Parser parser = Cent_Parser_from_string(";;");
    Cent_Statement statement;

    ASSERT_EQ(Cent_Parser_parse_statement(&parser, &statement), 1);
    ASSERT_EQ(statement.type, Cent_Statement_Empty);
    Cent_Statement_destroy(&statement);

    Cent_Parser_destroy(&parser);
    FINISH();
}

int parser__parse_statement__block() {
    Cent_Parser parser = Cent_Parser_from_string("{ ; }");
    Cent_Statement statement;

    ASSERT_EQ(Cent_Parser_parse_statement(&parser, &statement), 1);
    ASSERT_EQ(statement.type, Cent_Statement_Block);
    ASSERT_EQ(statement.value.block->num_declarations, 0);
    ASSERT_EQ(statement.value.block->num_statements, 1);
    ASSERT_EQ(statement.value.block->statements[0].type, Cent_Statement_Empty);
    Cent_Statement_destroy(&statement);

    Cent_Parser_destroy(&parser);
    FINISH();
}

int parser__parse_statement__continue() {
    Cent_Parser parser = Cent_Parser_from_string("continue;");
    Cent_Statement statement;

    ASSERT_EQ(Cent_Parser_parse_statement(&parser, &statement), 1);
    ASSERT_EQ(statement.type, Cent_Statement_Continue);
    Cent_Statement_destroy(&statement);

    ASSERT_EQ(Cent_Parser_parse_statement(&parser, &statement), 0);

    Cent_Parser_destroy(&parser);
    FINISH();
}

int parser__parse_statement__break() {
    Cent_Parser parser = Cent_Parser_from_string("break;");
    Cent_Statement statement;

    ASSERT_EQ(Cent_Parser_parse_statement(&parser, &statement), 1);
    ASSERT_EQ(statement.type, Cent_Statement_Break);
    Cent_Statement_destroy(&statement);

    ASSERT_EQ(Cent_Parser_parse_statement(&parser, &statement), 0);

    Cent_Parser_destroy(&parser);
    FINISH();
}
