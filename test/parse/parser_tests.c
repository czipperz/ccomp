#include "test.h"

void parser_tests(TestRunner* runner) {
    REGISTER(runner, parser__parse_type__int);
    REGISTER(runner, parser__parse_type__intp);
    REGISTER(runner, parser__parse_type__intpp);
    REGISTER(runner, parser__parse_type__intpp_then_symbol);
    REGISTER(runner, parser__next_top_level__variable);
    REGISTER(runner, parser__next_top_level__function_declaration_no_args);
    REGISTER(
        runner,
        parser__next_top_level__function_declaration_no_args_no_semicolon_fails);
    REGISTER(runner, parser__next_top_level__function_declaration_two_params);
    REGISTER(runner,
             parser__next_top_level__function_declaration_two_params_no_names);
    REGISTER(runner, parser__next_top_level__function_definition__empty_body);
    REGISTER(runner,
             parser__next_top_level__function_definition__return_constant);
    REGISTER(runner,
             parser__next_top_level__function_definition__no_parameters);
    REGISTER(runner,
             parser__next_top_level__function_definition__expression_constant);
    REGISTER(
        runner,
        parser__next_top_level__function_definition__declarare_variable_then_return_it);
    REGISTER(runner, parser__parse_expression__function_call_no_args);
    REGISTER(runner, parser__parse_expression__function_call);
    REGISTER(runner, parser__parse_expression__dereference_pointer);
    REGISTER(runner, parser__parse_expression__parenthesis_expression);
    REGISTER(runner, parser__parse_expression__assign_expression);
    REGISTER(runner, parser__parse_expression__assign_pointers_expression);
    REGISTER(runner, parser__parse_expression__dereference_function_call);
    REGISTER(runner, parser__parse_statement__empty);
    REGISTER(runner, parser__parse_statement__block);
    REGISTER(runner, parser__parse_statement__continue);
    REGISTER(runner, parser__parse_statement__break);
    REGISTER(runner, parser__parse_statement__while_then_statement);
    REGISTER(runner, parser__parse_statement__while_then_semicolon);
    REGISTER(runner, parser__parse_statement__if_then_statement);
    REGISTER(runner, parser__parse_statement__if_then_else);
    REGISTER(runner, parser__parse_statement__if_then_else_returns);
    REGISTER(runner, parser__parse_declaration__no_value);
    REGISTER(runner, parser__parse_declaration__constant_value);
}
