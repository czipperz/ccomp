#include "parse.h"
#include "test.h"

int parser__next_top_level__variable() {
    Cent_Parser parser = Cent_Parser_from_string("int x;");
    Cent_TopLevel top_level;

    ASSERT_EQ(Cent_Parser_next_top_level(&parser, &top_level), 1);
    ASSERT_EQ(top_level.type, Cent_TopLevel_Declaration);
    ASSERT_EQ(top_level.value.declaration.type, Cent_Declaration_Variable);
    ASSERT_EQ(top_level.value.declaration.value.variable.type.type,
              Cent_Type_Base);
    ASSERT_STR_EQ(
        top_level.value.declaration.value.variable.type.value.base.name, "int");
    ASSERT_STR_EQ(top_level.value.declaration.value.variable.name, "x");
    ASSERT_EQ(top_level.value.declaration.value.variable.value, 0);

    Cent_TopLevel_destroy(&top_level);
    Cent_Parser_destroy(&parser);
    FINISH();
}

int parser__next_top_level__function_declaration_no_args() {
    Cent_Parser parser = Cent_Parser_from_string("int x();");
    Cent_TopLevel top_level;

    ASSERT_EQ(Cent_Parser_next_top_level(&parser, &top_level), 1);
    ASSERT_EQ(top_level.type, Cent_TopLevel_Declaration);
    ASSERT_EQ(top_level.value.declaration.type, Cent_Declaration_Function);
    ASSERT_EQ(top_level.value.declaration.value.function.return_type.type,
              Cent_Type_Base);
    ASSERT_STR_EQ(
        top_level.value.declaration.value.function.return_type.value.base.name,
        "int");
    ASSERT_STR_EQ(top_level.value.declaration.value.function.name, "x");
    ASSERT_EQ(top_level.value.declaration.value.function.num_parameters, 0);

    ASSERT_EQ(Cent_Parser_next_top_level(&parser, &top_level), 0);

    Cent_TopLevel_destroy(&top_level);
    Cent_Parser_destroy(&parser);
    FINISH();
}

int parser__next_top_level__function_declaration_no_args_no_semicolon_fails() {
    Cent_Parser parser = Cent_Parser_from_string("int x()");
    Cent_TopLevel top_level;

    ASSERT_EQ(Cent_Parser_next_top_level(&parser, &top_level), -1);

    Cent_Parser_destroy(&parser);
    FINISH();
}

int parser__next_top_level__function_declaration_two_params() {
    Cent_Parser parser = Cent_Parser_from_string(
        "int x(int arg1, void** arg2);");
    Cent_TopLevel top_level;

    ASSERT_EQ(Cent_Parser_next_top_level(&parser, &top_level), 1);
    ASSERT_EQ(top_level.type, Cent_TopLevel_Declaration);
    ASSERT_EQ(top_level.value.declaration.type, Cent_Declaration_Function);
    ASSERT_EQ(top_level.value.declaration.value.function.return_type.type,
              Cent_Type_Base);
    ASSERT_STR_EQ(
        top_level.value.declaration.value.function.return_type.value.base.name,
        "int");
    ASSERT_STR_EQ(top_level.value.declaration.value.function.name, "x");

    ASSERT_EQ(top_level.value.declaration.value.function.num_parameters, 2);

    ASSERT_EQ(
        top_level.value.declaration.value.function.parameters[0].type.type,
        Cent_Type_Base);
    ASSERT_STR_EQ(top_level.value.declaration.value.function.parameters[0]
                      .type.value.base.name,
                  "int");
    ASSERT_STR_EQ(top_level.value.declaration.value.function.parameters[0].name,
                  "arg1");

    ASSERT_EQ(
        top_level.value.declaration.value.function.parameters[1].type.type,
        Cent_Type_Pointer);
    ASSERT_EQ(top_level.value.declaration.value.function.parameters[1]
                  .type.value.pointer.inner->type,
              Cent_Type_Pointer);
    ASSERT_EQ(top_level.value.declaration.value.function.parameters[1]
                  .type.value.pointer.inner->value.pointer.inner->type,
              Cent_Type_Base);
    ASSERT_STR_EQ(
        top_level.value.declaration.value.function.parameters[1]
            .type.value.pointer.inner->value.pointer.inner->value.base.name,
        "void");
    ASSERT_STR_EQ(top_level.value.declaration.value.function.parameters[1].name,
                  "arg2");

    Cent_TopLevel_destroy(&top_level);
    Cent_Parser_destroy(&parser);
    FINISH();
}

int parser__next_top_level__function_declaration_two_params_no_names() {
    Cent_Parser parser = Cent_Parser_from_string("int x(int, void**);");
    Cent_TopLevel top_level;

    ASSERT_EQ(Cent_Parser_next_top_level(&parser, &top_level), 1);
    ASSERT_EQ(top_level.type, Cent_TopLevel_Declaration);
    ASSERT_EQ(top_level.value.declaration.type, Cent_Declaration_Function);
    ASSERT_EQ(top_level.value.declaration.value.function.return_type.type,
              Cent_Type_Base);
    ASSERT_STR_EQ(
        top_level.value.declaration.value.function.return_type.value.base.name,
        "int");
    ASSERT_STR_EQ(top_level.value.declaration.value.function.name, "x");

    ASSERT_EQ(top_level.value.declaration.value.function.num_parameters, 2);

    ASSERT_EQ(
        top_level.value.declaration.value.function.parameters[0].type.type,
        Cent_Type_Base);
    ASSERT_STR_EQ(top_level.value.declaration.value.function.parameters[0]
                      .type.value.base.name,
                  "int");
    ASSERT_EQ(top_level.value.declaration.value.function.parameters[0].name, 0);

    ASSERT_EQ(
        top_level.value.declaration.value.function.parameters[1].type.type,
        Cent_Type_Pointer);
    ASSERT_EQ(top_level.value.declaration.value.function.parameters[1]
                  .type.value.pointer.inner->type,
              Cent_Type_Pointer);
    ASSERT_EQ(top_level.value.declaration.value.function.parameters[1]
                  .type.value.pointer.inner->value.pointer.inner->type,
              Cent_Type_Base);
    ASSERT_STR_EQ(
        top_level.value.declaration.value.function.parameters[1]
            .type.value.pointer.inner->value.pointer.inner->value.base.name,
        "void");
    ASSERT_EQ(top_level.value.declaration.value.function.parameters[1].name, 0);

    Cent_TopLevel_destroy(&top_level);
    Cent_Parser_destroy(&parser);
    FINISH();
}
