#pragma once
#include "bool.h"
#include "deque.h"

struct TestRunnerOptions {
    bool quiet;
    bool fork_tests;
};
typedef struct TestRunnerOptions TestRunnerOptions;

TestRunnerOptions TestRunnerOptions_default(void);
void TestRunnerOptions_parse(TestRunnerOptions*, int argc, char** argv);

struct TestRunner {
    Deque test_names;
    Deque tests;
    TestRunnerOptions options;
};
typedef struct TestRunner TestRunner;

TestRunner TestRunner_new(void);
void TestRunner_destroy(TestRunner*);

void TestRunner_add(TestRunner*, char* name, int test(void));
int TestRunner_run(TestRunner*);

#define REGISTER(runner, test)                                                 \
    do {                                                                       \
        int(test)(void);                                                       \
        TestRunner_add(runner, STRINGIFY(test), (test));                       \
    } while (0)
