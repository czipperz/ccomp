#include "assert.h"
#include "bool.h"
#include "deque_string.h"
#include "test.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

static void read_all(DequeString* string, int fd) {
    char buffer[128];
    ssize_t num;

    while ((num = read(fd, buffer, 128)) > 0) {
        DequeString_push_back_n(string, buffer, num);
    }

    DequeString_push_back(string, '\0');
    close(fd);
}

static int run_fork_test(Deque* errors, char* name, int (*test)(void),
                         TestRunnerOptions* options) {
    int status;
    int out[2];
    pid_t pid;

    pipe(out);
    pid = fork();
    if (pid == 0) {
        int result;
        close(out[0]);
        dup2(out[1], STDOUT_FILENO);
        dup2(out[1], STDERR_FILENO);
        close(out[1]);

        result = test();

        fclose(stdout);
        fclose(stderr);
        exit(result);
    } else {
        close(out[1]);

        waitpid(pid, &status, 0);

        if ((WIFEXITED(status) && WEXITSTATUS(status) != 0)
            || !WIFEXITED(status)) {
            DequeString string = DequeString_new();
            DequeString_push_back_str(&string, "Test case failed: ");
            DequeString_push_back_str(&string, name);
            DequeString_push_back_str(&string, ":\n");
            read_all(&string, out[0]);
            if (options->quiet) {
                fprintf(stderr, "F");
            } else {
                fprintf(stderr, "Failed: %s\n", name);
            }
            {
                char* unwrapped = DequeString_unwrap(&string);
                Deque_push_back(errors, &unwrapped, sizeof(char*));
            }
            return 1;
        }

        if (options->quiet) {
            fprintf(stderr, ".");
        } else {
            fprintf(stderr, "Succeeded: %s\n", name);
        }
        close(out[0]);
    }

    return 0;
}

int TestRunner_run_test(Deque* errors, char* name, int (*test)(void),
                        TestRunnerOptions* options) {
    if (options->fork_tests) {
        return run_fork_test(errors, name, test, options);
    } else {
        int status = test();
        if (status) {
            if (options->quiet) {
                fprintf(stderr, "F");
            } else {
                fprintf(stderr, "Failed: %s\n", name);
            }
        } else {
            if (options->quiet) {
                fprintf(stderr, ".");
            } else {
                fprintf(stderr, "Succeeded: %s\n", name);
            }
        }
        return status;
    }
}
