#include "test.h"
#include <stdlib.h>

static int starts_with(char* string, char* prefix) {
    return strncmp(prefix, string, strlen(prefix)) == 0;
}

TestRunnerOptions TestRunnerOptions_default(void) {
    TestRunnerOptions options;
    options.quiet = 0;
    options.fork_tests = 0;
    return options;
}

void TestRunnerOptions_parse(TestRunnerOptions* options, int argc,
                             char** argv) {
    char* options_key[] = { "quiet", "fork-tests" };
    bool* option_value[] = { &options->quiet, &options->fork_tests };
    int i;
    for (i = 0; i < argc; ++i) {
        char* arg = argv[i];
        if (starts_with(arg, "--")) {
            arg += 2;
            size_t option;
            for (option = 0;
                 option < sizeof(options_key) / sizeof(*options_key);
                 ++option) {
                size_t key_len = strlen(options_key[option]);
                if (starts_with(arg, options_key[option])) {
                    if (arg[key_len] == '=') {
                        arg += strlen(options_key[option]) + 1;
                        if (strcmp(arg, "true") == 0) {
                            *(option_value[option]) = 1;
                        } else if (strcmp(arg, "false") == 0) {
                            *(option_value[option]) = 0;
                        } else {
                            fprintf(stderr, "Error: Unhandled argument %s\n",
                                    argv[i]);
                            exit(1);
                        }
                        break;
                    } else if (arg[key_len] == '\0') {
                        *(option_value[option]) = 1;
                        break;
                    }
                }
            }
            if (option == sizeof(options_key) / sizeof(*options_key)) {
                fprintf(stderr, "Error: Unhandled argument %s\n", argv[i]);
                exit(1);
            }
        } else {
            fprintf(stderr, "Error: Unhandled argument %s\n", argv[i]);
            exit(1);
        }
    }
}

TestRunner TestRunner_new(void) {
    TestRunner runner = { Deque_new(), Deque_new(),
                          TestRunnerOptions_default() };
    return runner;
}

void TestRunner_destroy(TestRunner* test_runner) {
    free(test_runner->test_names.buffer);
    free(test_runner->tests.buffer);
}

void TestRunner_add(TestRunner* test_runner, char* name, int (*test)(void)) {
    Deque_push_back(&test_runner->test_names, &name, sizeof(char*));
    Deque_push_back(&test_runner->tests, &test, sizeof(int (*)(void)));
}

int TestRunner_run_test(Deque* errors, char* name, int (*test)(void),
                        TestRunnerOptions* options);

int TestRunner_run(TestRunner* test_runner) {
    int passed = 0, failed = 0;
    Deque errors = Deque_new();

    size_t i;
    for (i = 0; i < test_runner->tests.length; ++i) {
        char** name = Deque_at(&test_runner->test_names, i, sizeof(char*));
        int (**test)(void) = Deque_at(&test_runner->tests, i,
                                      sizeof(int (*)(void)));
        if (TestRunner_run_test(&errors, *name, *test, &test_runner->options)) {
            failed++;
        } else {
            passed++;
        }
    }
    if (test_runner->options.quiet) {
        printf("\n");
    }
    fflush(stdout);

    for (i = 0; i < errors.length; ++i) {
        char** ptr_error = Deque_at(&errors, i, sizeof(char*));
        fprintf(stderr, "\n%s", *ptr_error);
    }
    Deque_destroy_deref(&errors, free, sizeof(char*));
    fflush(stderr);

    printf("\nRan %d tests: %d passed, %d failed\n", passed + failed, passed,
           failed);

    return failed;
}
