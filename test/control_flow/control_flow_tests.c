#include "test.h"

void control_flow_tests(TestRunner* runner) {
    REGISTER(runner, cfgi__serialize);
    REGISTER(runner, cfgi__return_and_if);
    REGISTER(runner, cfgi__while_continue_and_break);
}
