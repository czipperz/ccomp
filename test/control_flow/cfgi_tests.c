#include "../test.h"
#include "cfgi.h"
#include "parse.h"
#include <stdlib.h>

int cfgi__serialize(void) {
    Cent_Parser parser = Cent_Parser_from_string("void f() { if (1) 2; 3; }");
    Cent_TopLevel top_level;
    CFGI_Graph graph = CFGI_Graph_new();
    char* result;
    ASSERT_EQ(Cent_Parser_next_top_level(&parser, &top_level), 1);

    CFGI_Graph_add(&graph, &top_level);
    result = CFGI_Graph_serialize(&graph, 0);

    ASSERT_STR_EQ(result, "digraph G {\n\
\"f <<start>>\" -> \"1\"\n\
\"1\" -> \"2\" [color=green]\n\
\"1\" -> \"3\" [color=red]\n\
\"2\" -> \"3\"\n\
\"3\" -> \"f <<end>>\"\n\
}\n");

    free(result);
    CFGI_Graph_destroy(&graph);
    Cent_TopLevel_destroy(&top_level);
    Cent_Parser_destroy(&parser);
    FINISH();
}

int cfgi__return_and_if(void) {
    Cent_Parser parser = Cent_Parser_from_string(
        "void f() { if (1) { return 2; } else { 3; } 4; }");
    Cent_TopLevel top_level;
    ASSERT_EQ(Cent_Parser_next_top_level(&parser, &top_level), 1);

    {
        CFGI_Graph graph = CFGI_Graph_new();

        CFGI_Graph_add(&graph, &top_level);

        CFGI_Graph_print(&graph, stdout, 0);
        ASSERT(CFGI_Graph_has_string_edge(&graph, "\"f <<start>>\"",
                                          CFGI_Edge_Sequential, "\"1\""));
        ASSERT(CFGI_Graph_has_string_edge(&graph, "\"1\"", CFGI_Edge_True,
                                          "\"return 2;\""));
        ASSERT(CFGI_Graph_has_string_edge(&graph, "\"1\"", CFGI_Edge_False,
                                          "\"3\""));
        ASSERT(CFGI_Graph_has_string_edge(&graph, "\"3\"", CFGI_Edge_Sequential,
                                          "\"4\""));
        ASSERT(CFGI_Graph_has_string_edge(&graph, "\"4\"", CFGI_Edge_Sequential,
                                          "\"f <<end>>\""));
        ASSERT(CFGI_Graph_has_string_edge(
            &graph, "\"return 2;\"", CFGI_Edge_Sequential, "\"f <<end>>\""));
        ASSERT_EQ(CFGI_Graph_count_edges(&graph), 6);

        CFGI_Graph_destroy(&graph);
    }

    Cent_TopLevel_destroy(&top_level);
    Cent_Parser_destroy(&parser);
    FINISH();
}

int cfgi__while_continue_and_break(void) {
    Cent_Parser parser = Cent_Parser_from_string(
        "void f() { while (1) { if (2) { continue; } "
        "else { 3; } break; } 5; }");
    Cent_TopLevel top_level;
    ASSERT_EQ(Cent_Parser_next_top_level(&parser, &top_level), 1);

    {
        CFGI_Graph graph = CFGI_Graph_new();

        CFGI_Graph_add(&graph, &top_level);

        CFGI_Graph_print(&graph, stdout, 0);
        ASSERT(CFGI_Graph_has_string_edge(&graph, "\"f <<start>>\"",
                                          CFGI_Edge_Sequential, "\"1\""));
        ASSERT(CFGI_Graph_has_string_edge(&graph, "\"1\"", CFGI_Edge_True,
                                          "\"2\""));
        ASSERT(CFGI_Graph_has_string_edge(&graph, "\"2\"", CFGI_Edge_True,
                                          "\"1\""));
        ASSERT(CFGI_Graph_has_string_edge(&graph, "\"2\"", CFGI_Edge_False,
                                          "\"3\""));
        ASSERT(CFGI_Graph_has_string_edge(&graph, "\"3\"", CFGI_Edge_Sequential,
                                          "\"5\""));
        ASSERT(CFGI_Graph_has_string_edge(&graph, "\"1\"", CFGI_Edge_False,
                                          "\"5\""));
        ASSERT(CFGI_Graph_has_string_edge(&graph, "\"5\"", CFGI_Edge_Sequential,
                                          "\"f <<end>>\""));
        ASSERT_EQ(CFGI_Graph_count_edges(&graph), 7);

        CFGI_Graph_destroy(&graph);
    }

    Cent_TopLevel_destroy(&top_level);
    Cent_Parser_destroy(&parser);
    FINISH();
}
