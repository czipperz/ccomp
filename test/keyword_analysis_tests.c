#include "keyword_analysis.h"
#include "parse.h"
#include "test.h"

int analyze_keywords__doesnt_error_normally(void) {
    Cent_TopLevel top_level;
    Cent_Parser parser = Cent_Parser_from_string(
        "int f(double x) { float y; }");

    ASSERT_EQ(Cent_Parser_next_top_level(&parser, &top_level), 1);
    ASSERT_EQ(analyze_keywords(&top_level), 0);
    Cent_TopLevel_destroy(&top_level);

    Cent_Parser_destroy(&parser);
    FINISH();
}

int analyze_keywords__variable_name_int(void) {
    Cent_TopLevel top_level;
    Cent_Parser parser = Cent_Parser_from_string("int int;");

    ASSERT_EQ(Cent_Parser_next_top_level(&parser, &top_level), 1);
    ASSERT_EQ(analyze_keywords(&top_level), 1);
    Cent_TopLevel_destroy(&top_level);

    Cent_Parser_destroy(&parser);
    FINISH();
}

int analyze_keywords__variable_type_return(void) {
    Cent_TopLevel top_level;
    Cent_Parser parser = Cent_Parser_from_string("return a;");

    ASSERT_EQ(Cent_Parser_next_top_level(&parser, &top_level), 1);
    ASSERT_EQ(analyze_keywords(&top_level), 1);
    Cent_TopLevel_destroy(&top_level);

    Cent_Parser_destroy(&parser);
    FINISH();
}

int analyze_keywords__parameter(void) {
    Cent_TopLevel top_level;
    Cent_Parser parser = Cent_Parser_from_string(
        "int f(return a); int g(int return); int h(int int);");

    ASSERT_EQ(Cent_Parser_next_top_level(&parser, &top_level), 1);
    ASSERT_EQ(analyze_keywords(&top_level), 1);
    Cent_TopLevel_destroy(&top_level);

    ASSERT_EQ(Cent_Parser_next_top_level(&parser, &top_level), 1);
    ASSERT_EQ(analyze_keywords(&top_level), 1);
    Cent_TopLevel_destroy(&top_level);

    ASSERT_EQ(Cent_Parser_next_top_level(&parser, &top_level), 1);
    ASSERT_EQ(analyze_keywords(&top_level), 1);
    Cent_TopLevel_destroy(&top_level);

    ASSERT_EQ(Cent_Parser_next_top_level(&parser, &top_level), 0);

    Cent_Parser_destroy(&parser);
    FINISH();
}

void keyword_analysis_tests(TestRunner* runner) {
    REGISTER(runner, analyze_keywords__doesnt_error_normally);
    REGISTER(runner, analyze_keywords__variable_name_int);
    REGISTER(runner, analyze_keywords__variable_type_return);
    REGISTER(runner, analyze_keywords__parameter);
}
