#include "bool.h"
#include "deque.h"
#include <stdio.h>
#include <string.h>

#include "test_runner/test_runner.h"

#define STRINGIFY_(x) #x
#define STRINGIFY(x) STRINGIFY_(x)

#define ASSERT(cond)                                                           \
    do {                                                                       \
        if (!(cond)) {                                                         \
            fprintf(stderr, "%s:%d: %s: Assertion failure: %s\n", __FILE__,    \
                    __LINE__, __func__, STRINGIFY(cond));                      \
            return 1;                                                          \
        }                                                                      \
    } while (0)

#define ASSERT_EQ(a, b)                                                        \
    do {                                                                       \
        if ((a) != (b)) {                                                      \
            fprintf(stderr, "%s:%d: %s: Assertion failure: %s == %s\n",        \
                    __FILE__, __LINE__, __func__, STRINGIFY(a), STRINGIFY(b)); \
            return 1;                                                          \
        }                                                                      \
    } while (0)

#define ASSERT_NE(a, b)                                                        \
    do {                                                                       \
        if ((a) == (b)) {                                                      \
            fprintf(stderr, "%s:%d: %s: Assertion failure: %s != %s\n",        \
                    __FILE__, __LINE__, __func__, STRINGIFY(a), STRINGIFY(b)); \
            return 1;                                                          \
        }                                                                      \
    } while (0)

#define ASSERT_STR_EQ(a, b)                                                    \
    do {                                                                       \
        if (strcmp((a), (b)) != 0) {                                           \
            fprintf(stderr, "%s:%d: %s: Assertion failure: %s == %s\n",        \
                    __FILE__, __LINE__, __func__, STRINGIFY(a), STRINGIFY(b)); \
            return 1;                                                          \
        }                                                                      \
    } while (0)

#define ASSERT_STRNEQ(a, b, n)                                                 \
    do {                                                                       \
        if (strncmp((a), (b), (n)) != 0) {                                     \
            fprintf(stderr,                                                    \
                    "%s:%d: %s: Assertion failure: %s == %s for %s chars\n",   \
                    __FILE__, __LINE__, __func__, STRINGIFY(a), STRINGIFY(b),  \
                    STRINGIFY(n));                                             \
            return 1;                                                          \
        }                                                                      \
    } while (0)

#define FINISH()                                                               \
    do {                                                                       \
        return 0;                                                              \
    } while (0)
